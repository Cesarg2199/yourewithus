var base_url = $("#base_url").val();

function viewMore(id)
{
  prepareModal();

  //load the modal with the new data
  $.ajax({
    url: base_url + '/Reports/inclusionReportFull',
    type: 'POST',
    data: {id:id},
    success: function(full){
      full = JSON.parse(full);

      //pq
      $("#mpq1").html(full[0].pq1);
      $("#mpq2").html(full[0].pq2);
      $("#mpq3").html(full[0].pq3);
      $("#mpq4").html(full[0].pq4);
      $("#mpq5").html(full[0].pq5);

      //sq
      $("#msq1").html(full[0].sq1);
      $("#msq2").html(full[0].sq1);
      $("#msq3").html(full[0].sq1);
      $("#msq4").html(full[0].sq1);

      //aq
      $("#maq1").html(full[0].aq1);
      $("#maq2").html(full[0].aq2);
      $("#maq3").html(full[0].aq3);
      $("#maq4").html(full[0].aq4);
    }
  });

  //show the modal
  $("#viewMoreScores").modal("show");
}

function prepareModal()
{
  $("#mpq1").html('');
  $("#mpq2").html('');
  $("#mpq3").html('');
  $("#mpq4").html('');
  $("#mpq5").html('');

  //sq
  $("#msq1").html('');
  $("#msq2").html('');
  $("#msq3").html('');
  $("#msq4").html('');

  //aq
  $("#maq1").html('');
  $("#maq2").html('');
  $("#maq3").html('');
  $("#maq4").html('');
}
