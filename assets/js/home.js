function filterSchool(school)
{
  $(".thumbnail").show();

  if(school != "All")
  {
    $(".linkedSchool").filter(function(){
      return $(this).text().trim() != school;
    }).closest(".thumbnail").hide();
  }
  else {
    $(".thumbnail").show();
  }

}
