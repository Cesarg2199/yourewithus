var site_url = $("#base_url").val();

function printPED(participantId){
  window.location = site_url+'/Download/ped?participantId='+participantId;
}

function printIntake(participantId){
  window.location = site_url+'/Download/intake?participantId='+participantId;
}

function printScoutingReport(participantId){
  window.location = site_url+'/Download/scoutingReport?participantId='+participantId;
}
