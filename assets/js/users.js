var base_url = $("#base_url").val();
var clone = true;
var schoolCloneCount = 0;

//array of where to pull participants from
var list = []

$( document ).ready(function() {
   $('#phone').mask('(000) 000-0000');
   $("#participantField").hide();
   $("#link").hide();
    roleCheck();
 });

function deactivateUser(id)
{
  ans = confirm("Are you sure you want to deactivate this user?");
  if(ans)
  {
    $.ajax({
      type: 'post',
      url: base_url + '/Users/deactivateUser',
      data: { userId:id },
      success: function () {
        $("#"+id).remove();
      }
    });
  }
  else { return false; }
}

function activateUser(id)
{
  ans = confirm("Are you sure you want to activate this user?");
  if(ans)
  {
    $.ajax({
      type: 'post',
      url: base_url + '/Users/activateUser',
      data: { userId:id },
      success: function () {
        $("#"+id).remove();
      }
    });
  }
  else { return false; }
}

function roleCheck()
{
   if($("#role").val() == "Administrator")
   {
       $("#schoolLinking").hide();
       $("#participantField").hide();
   }
   else if($("#role").val() == "Campus Case Coordinator")
   {
       $("#schoolLinking").show();
       $("#participantField").hide();
   }
   else if($("#role").val() == "Region Administrator")
   {
       $("#schoolLinking").show();
       $("#participantField").hide();
   }
   else if($("#role").val() == "")  {
       $("#schoolLinking").hide();
       $("#participantField").hide();
   }
   else {
       $("#schoolLinking").show();
       $("#participantField").show();
   }
}

function addParticipant()
{
  if(clone)
  {
    $("#clone").show();
    clone = false;
  }
  else {
    $('#participantSet').clone().appendTo('#participantField');
  }
}

function removeParticipant(el)
{
   $("#participant"+el).remove();
}

function quickAddSchool()
{
  var schoolname = $("#schoolNameAdd").val();

  $.ajax({
    type: 'post',
    url: base_url + '/Schools/quickAddSchool',
    data: { name:schoolname },
    success: function (json) {
      json = JSON.parse(json);
      $('#schoolList').empty();
      //rebuild select list here
      $('#schoolList').append($('<option>').text('-'));
      $.each(json, function(i, value) {
          $('#schoolList').append($('<option>').text(json[i].name).attr('value', json[i].name));
        });

      $("#schoolAddModal").modal('hide');
      $("#schoolNameAdd").val("");
    }
  });
}

function addSchool()
{
   $('#schoolSet').clone().attr('id', 'schoolSet'+ schoolCloneCount++).insertAfter($('[id^=schoolSet]:last')).appendTo('#schoolField');
}

function removeSchool(el)
{
   $("#school"+el).remove();
}

/*function loadParticipants()
{
  //First check to see if we need to activate the participants
  role = $("#role").val();

  if((role != "Region Administrator") && (role != "Campus Case Coordinator"))
  {
    if(schoolCloneCount > 0)
    {
      list.push($("#schoolSet"+(schoolCloneCount - 1)+" option:selected").text())
    }
    else
    {
      //add the first item because it wont run in the for loop
      list.push($("#schoolSet option:selected").text())
    }

    //destroy same items in array
    reduced = Object.keys(list.reduce((p,c) => (p[c] = true,p),{}));
  }

  //note you can reset the array when a new school clone is added

  //another note if someone changes the school on the already added select option it keeps it in the array. need a new way to get rid of This

  //possibly run this as a loop instead everytime versus just added every time the button is clicked.

  //ajax here to get list of participants


}*/
