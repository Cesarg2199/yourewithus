var base_url = $("#base_url").val();

function quickAddSchool()
{
  var schoolname = $("#schoolNameAdd").val();

  $.ajax({
    type: 'post',
    url: base_url + '/Schools/quickAddSchool',
    data: { name:schoolname },
    success: function (json) {
      json = JSON.parse(json);
      $('#school').empty();
      //rebuild select list here
      $.each(json, function(i, value) {
            $('#school').append($('<option>').text(json[i].name).attr('value', json[i].name));
        });

      $("#schoolAddModal").modal('hide');
      $("#schoolNameAdd").val("");
    }
  });
}
