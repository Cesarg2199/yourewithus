var base_url = $("#base_url").val();
var totalFiles = parseInt($("#totalFiles").val());
var participantId = parseInt($("#participantId").val());

// Disabling autoDiscover, otherwise Dropzone will try to attach twice.
Dropzone.autoDiscover = false;
// or disable for specific dropzone:
// Dropzone.options.myDropzone = false;

$(function() {
  // Now that the DOM is fully loaded, create the dropzone, and setup the
  // event listeners
  var myDropzone = new Dropzone(".dropzone");

  myDropzone.on("sending", function(file, xhr, formData) {
    formData.append("participantId", participantId);
  });

  myDropzone.on("queuecomplete", function(file, res) {
      if (myDropzone.files[0].status == Dropzone.SUCCESS ) {
          location.reload();
      }
      else {
        alert("File size too big.");
      }
  });

  $('#filter').keyup(function () {
    search();
  });

});


function search()
{
  var rex = new RegExp($("#filter").val(), 'i');
  $('.searchable div').hide();
  $('.searchable div').filter(function () {
      return rex.test($(this).text());
  }).show();
}

function deleteFile(name, count)
{
  var check = confirm("Are you sure you want to delete this file?");
  if(check)
  {
    var remove = "#file"+count;

    $.ajax({
      type: 'post',
      url: base_url+"Participants/deleteFile",
      data: { name:name, participantId:participantId },
  		success: function() {
  			 location.reload();
  		}
  	});
  }

}
