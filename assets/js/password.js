$("#confirmpassword").on('change keydown paste input', function(){
    checkPassword();
});

$("#password").on('change keydown paste input', function(){
    checkPassword();
});

function checkPassword()
{
  if($("#password").val() != "" & $("#confirmpassword").val() != "")
  {
    $('#btnUpdatePassword').prop('disabled', false);
  }
  else {
    $('#btnUpdatePassword').prop('disabled', true);
  }
}
