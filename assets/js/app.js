$(function() {
    $("#participant").select2();
    $("#support").select2();
    $("#user").select2();
    $("table.datatable").DataTable();
});

function loadingScreen() {
    console.log("LST");
    var body = $(document.body);
    if (!body.hasClass('overlay')) {
        loadingScreenOn();
    } else {
        loadingScreenOff();
    }
}

function loadingScreenOn() {
    $(document.body).addClass("overlay");
    $("body").append("<div class='spinner'></div>");
}

function loadingScreenOff() {
    $(".spinner").remove();
    $(document.body).removeClass("overlay");
}