var base_url = $("#base_url").val();

$(document).ready(function(){

  $( "#assignDue" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

});

function viewNotes(taskId) //Opens the notes modal and gets all the notes accociated with a specific task
{
  $("#newNote").val("");
  $("#notesView").empty();
  $("#taskId").val(taskId);

  $.ajax({
    url: base_url + '/Tasks/getNotes',
    type: 'POST',
    data: {
      taskId: taskId
    },
    success: function(notes)
    {
      notes = JSON.parse(notes);
      $.each(notes, function(i, val)
      {
          $("#notesView").append("<tr class='removeableNote-"+notes[i].id+"'><td>"+notes[i].user+"</td><td style='word-wrap: break-word'>"+notes[i].message+"</td><td>"+notes[i].date+"</td><td><button class='btn btn-danger' onmouseup='deleteTaskNote("+notes[i].id+", "+notes[i].taskId+")'>X</button></td></tr>");
      });

      $('#notesModal').modal('show');
    }
  });
}

function addNoteToTask() //Adds new note to a specific task
{
  var message = $("#newNote").val();
  var taskId = $("#taskId").val();

  var eachLine = message.split('\n');
  var messageWithFormating = eachLine[0];

  for(var i = 1, l = eachLine.length; i < l; i++)
  {
     messageWithFormating += '<br>' + eachLine[i];
  }

  if(message != "")
  {
    $.ajax({
      url: base_url + '/Tasks/addNote',
      type: 'POST',
      data: {
        taskId: taskId, message: messageWithFormating
      },
      success: function(notes)
      {
        $('#notesModal').modal('hide');
        viewNotes(taskId);
      }
    });
  }
  else {
    alert("Fill out the message field before adding note.");
  }
}

function deleteTaskNote(noteId, taskId) //Confirms user's choice, hides note, and then deletes it
{
  var verify = confirm("Are you sure you want to delete this note?");
  if(verify)
  {
    $("button").click(function()
    {
      $(".removeableNote-" + noteId + "").hide(500);

      $.ajax({
        url: base_url + '/Tasks/deleteNote',
        type: 'POST',
        data: {
          id: noteId
        }
      });
    });
  }
}

function completedTask(taskId) //Hides the task then deletes it
{
  $("button").click(function()
  {
    $(".removeable-" + taskId + "").hide(500);

    $.ajax({
      url: base_url + '/Tasks/completedTask',
      type: 'POST',
      data: {
        taskId: taskId
      }
    });
  });
}


function uncompleteTask(taskId) //Confirms user's choice, hides task, then marks task as uncomplete
{
  var verify = confirm("Are you sure you want to uncomplete this task?");
    if(verify){
    $("button").click(function()
    {
      $(".removeable-" + taskId + "").hide(500);

      $.ajax({
        url: base_url + '/Tasks/uncompleteTask',
        type: 'POST',
        data: { taskId: taskId }
      });
    });
  }
}

function deleteSentTask(taskId) //Confirms user's choice, hides task, then deletes task
{
  var verify = confirm("Are you sure you want to delete this task?");
  if(verify){
    $("button").click(function()
    {
      $(".removeable-" + taskId + "").hide(500);

      $.ajax({
        url: base_url + '/Tasks/deleteSentTask',
        type: 'POST',
        data: { taskId: taskId }
      });
    });
  }
}
