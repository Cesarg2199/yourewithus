$(function() {
	//$('#findParticipant').select2();

	var currentDate; // Holds the day clicked when adding a new event
	var currentEvent; // Holds the event object when editing an event

	$("#sd").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	$("#ed").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	createCalendar();

	var base_url = $("#siteUrl").val(); // Here i define the base_url

	function removeAll() {
		$("#calendar").fullCalendar('destroy');
	}

	$("#participant").change(function() {
		removeAll();
		createCalendar();
		loadSchedule();
	});

	function loadSchedule() {
		var participant = $("#participant").val();
		var newCal = base_url + 'index.php/Calendar/getEvents?participantId=' + participant;
		$("#calendar").fullCalendar('addEventSource', newCal);
		$('#calendar').fullCalendar('renderEvents');
	}

	function createCalendar() {
		// Fullcalendar
		$('#calendar').fullCalendar({
			height: 900,
			aspectRatio: 2,
			header: {
				left: 'prev, next',
				center: 'title',
				right: 'month, basicWeek, basicDay'
			},



			// Handle Day Click
			dayClick: function(date, event, view) {
				var participant = $("#participant").val();
				//Prevents opening add model without selecting participant first
				if (participant != 0) {
					currentDate = date.format();
					// Open modal to add event
					modal({
						// Available buttons when adding
						buttons: {
							add: {
								id: 'add-event', // Buttons id
								css: 'btn-success', // Buttons class
								label: 'Add' // Buttons label
							}
						},
						title: 'Add Event (' + date.format() + ')', // Modal title
						selectedDate: date.format()
					});
				}

			},

			/* editable: true, // Make the event draggable true
			eventDrop: function(event, delta, revertFunc) {


			    $.post(base_url + 'Calendar/dragUpdateEvent', {
			        id: event.id,
			        date: event.start.format()
			    }, function(result) {
			        if (result) {
			            alert('Updated');
			        } else {
			            alert('Try Again later!')
			        }
			    });

			},*/
			// Event Mouseover
			/*  eventMouseover: function(calEvent, jsEvent, view) {

			      var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
			      $("body").append(tooltip);

			      $(this).mouseover(function(e) {
			          $(this).css('z-index', 10000);
			          $('.event-tooltip').fadeIn('500');
			          $('.event-tooltip').fadeTo('10', 1.9);
			      }).mousemove(function(e) {
			          $('.event-tooltip').css('top', e.pageY + 10);
			          $('.event-tooltip').css('left', e.pageX + 20);
			      });
			  },
			  eventMouseout: function(calEvent, jsEvent) {
			      $(this).css('z-index', 8);
			      $('.event-tooltip').remove();
			  }, */
			// Handle Existing Event Click
			eventClick: function(calEvent, jsEvent, view) {
				// Set currentEvent variable according to the event clicked in the calendar
				currentEvent = calEvent;

				// Open modal to edit or delete event
				modal({
					// Available buttons when editing
					buttons: {
						delete: {
							id: 'delete-event',
							css: 'btn-danger',
							label: 'Delete'
						},
						update: {
							id: 'update-event',
							css: 'btn-success',
							label: 'Update'
						}
					},
					title: 'Event Details',
					event: calEvent
				});
			}

		});
	}

	var removeCheck = 0;

	// Prepares the modal window according to data passed
	function modal(data) {
		// Set modal title
		$('.modal-title').html(data.title);
		// Clear buttons except Cancel
		$('.modal-footer button:not(".btn-default")').remove();
		// Set input values
		$('#title').val(data.event ? data.event.title : '');
		if (!data.event) {
			// When adding set timepicker to current time
			var now = new Date();
		}

		if (data.event) {
			$("#support").prepend("<option value='" + data.event.userid + "' selected>" + data.event.username + "</option>");
			removeCheck = 1;
		} else {
			removeCheck = 0;
		}

		$('#description').val(data.event ? data.event.description : '');

		//Checks to see if add modal was opened
		if (data.buttons.add) {
			//get date selected and fill it in
			$('#sd').val(data.selectedDate);
			$('#ed').val(data.selectedDate);
		} else {
			//If edit modal was opened fill in the information
			$('#sd').val(data.event ? data.event.start._i : '');
			$('#ed').val(data.event ? data.event.end._i : '');
		}

		// Create Butttons
		$.each(data.buttons, function(index, button) {
			$('.modal-footer').prepend('<button type="button" id="' + button.id + '" class="btn ' + button.css + '">' + button.label + '</button>')
		})
		//Show Modal
		$('.modal').modal('show');
	}

	//Handle modal close
	$('.modal').on('hidden.bs.modal', function() {
		if (removeCheck == 1) {
			$("#support").find("option").eq(0).remove();
		}
		removeCheck = 0;
	});

	// Handle Click on Add Button
	$('.modal').on('click', '#add-event', function(e) {
		if (validator(['title', 'description'])) {
			$.post(base_url + 'index.php/Calendar/addEvent', {
				title: $('#title').val(),
				description: $('#description').val(),
				startDate: $("#sd").val(),
				endDate: $("#ed").val(),
				participantid: $("#participant").val(),
				support: $("#support").val()
			}, function(result) {
				$('.modal').modal('hide');
				$("#calendar").fullCalendar('refetchEvents');
				//loadSchedule();
			});
		}
	});

	// Handle click on Update Button
	$('.modal').on('click', '#update-event', function(e) {
		var check = confirm("Are you sure you want to update this event?");
		if (check) {
			if (validator(['title', 'description'])) {
				$.post(base_url + 'index.php/Calendar/updateEvent?id=' + currentEvent._id, {
					title: $('#title').val(),
					description: $('#description').val(),
					startDate: $("#sd").val(),
					endDate: $("#ed").val(),
					participantid: $("#participant").val(),
					support: $("#support").val()
				}, function(result) {
					$('.modal').modal('hide');
					$("#calendar").fullCalendar('refetchEvents');
				});
			}
		}
	});

	// Handle Click on Delete Button
	$('.modal').on('click', '#delete-event', function(e) {
		var check = confirm("Are you sure you want to delete this event?");
		if (check) {
			$.get(base_url + 'index.php/Calendar/deleteEvent?id=' + currentEvent._id, function(result) {
				$('.modal').modal('hide');
				$("#calendar").fullCalendar('refetchEvents');
			});
		}
	});

	// Dead Basic Validation For Inputs
	function validator(elements) {
		var errors = 0;
		$.each(elements, function(index, element) {
			if ($.trim($('#' + element).val()) == '') errors++;
		});
		if (errors) {
			$('.error').html('Please insert title and description');
			return false;
		}
		return true;
	}
});
