$(document).on('click', '[data-toggle="lightbox"]', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});

$("#btnMissionStatement").click(function() {
	console.log("mission statement read more button clicked");
	$("#rmMissionStatement").slideToggle("fast", function() {});
	$("#btnMissionStatement .more").toggle();
	$("#btnMissionStatement .less").toggle();
});

$("#btnHistory").click(function() {
	console.log("mission statement read more button clicked");
	$("#rmHistory").slideToggle("fast", function() {});
	$("#btnHistory .more").toggle();
	$("#btnHistory .less").toggle();
});