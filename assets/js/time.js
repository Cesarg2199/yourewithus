var base_url = $("#base_url").val();

$(document).ready(function(){
  $( "#date" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  var options = {
    title: 'Enter Time'
  };

  $('.time').wickedpicker(options);
});

function timeDiff()
{
  //getting times
  var timeIn = $("#timeIn").wickedpicker('time');
  var timeOut = $("#timeOut").wickedpicker('time');

  //converting time ins into ints
  var timeInHour = timeIn.substring(0, 2).trim();
  timeInHour = parseInt(timeInHour);

  var timeInMins = timeIn.substring(4, 7).trim();
  timeInMins = parseInt(timeInMins);

  //converting time out into ints
  var timeOutHour = timeOut.substring(0, 2).trim();
  timeOutHour = parseInt(timeOutHour);

  var timeOutMins = timeOut.substring(4, 7).trim();
  timeOutMins = parseInt(timeOutMins);

  //creating test case for PM
  var testPm = new RegExp("PM");

  //test PM for time in
  var timeInRes = testPm.test(timeIn);

  if(timeInRes) {
    if(timeInHour != 12)
    {
      timeInHour = timeInHour + 12;
    }
  }

  //test PM for time out
  var timeOutRes = testPm.test(timeOut);

  if (timeOutRes) {
    if(timeOutHour != 12)
    {
      timeOutHour = timeOutHour + 12;
    }
  }

	if(timeInHour == timeOutHour)
  {
  	if((timeOutMins - timeInMins) < 0)
    {
    	alert("Time Range Not Possible.");
			return false;
    }
		else
		{
			return true;
		}
  }
  else
  {
  	if(timeInHour > timeOutHour)
    {
    	alert("Time Range Not Possible.");
			return false;
    }
		else
		{
			return true;
		}
  }
}

function dailyLog() {
  var userId = $("#userId").val();

  $.ajax({
    type: 'post',
    url: base_url+"Users/checkDailyLog",
    data: { userId:userId },
    success: function(data) {
      if(data == "true")
      {
        document.getElementById("timeSheetForm").submit();
      }
      else
      {
        $("#dailyLogMessage").modal();
      }
    }
  });
}

function submitTimeEntry()
{
	if(timeDiff())
	{
    dailyLog();
	}
}
