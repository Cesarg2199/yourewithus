var base_url = $("#base_url").val();

function loadEditModal(id, name)
{
  $("#schoolNameEdit").val(name);
  $("#schoolIdEdit").val(id);
  $("#schoolEditModal").modal()
}

function addSchoolSave()
{
  var name = $("#schoolNameAdd").val();
  $.ajax({
    type: 'post',
    url: base_url + '/Schools/addSchool',
    data: { name:name },
    success: function () {

    }
  });

  location.reload();
}

function editSchoolSave()
{
  var name = $("#schoolNameEdit").val();
  var id = $("#schoolIdEdit").val();

  $.ajax({
    type: 'post',
    url: base_url + '/Schools/editSchool',
    data: { id:id, name:name },
    success: function () {

    }
  });

  location.reload();
}
