$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');


    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-danger').addClass('btn-default');
            $item.addClass('btn-danger');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-danger').trigger('click');
});

function enableNext(cb, btn)
{
  if($('#'+cb).prop('checked'))
  {
    $('#'+btn).removeAttr('disabled');
  }
  else {
    $('#'+btn).attr('disabled', true);
  }
}

function completeOnboarding()
{
  //check if all three checkboxes are not checked
  if(!($('.chkBox').not(':checked').length === 0))
  {
    //if not all checkboxes are checked create alert and do not submit form.
    alert("You must check all the boxes in order to continue");
    preventDefault();
  }
  else {
    //Checks to see if the skip password is checked.
    if($('.skipPassword').not(':checked').length === 0)
    {
      $( "form:first" ).submit();
    }
    else {
      var password = $("#password").val();
      var cpassword = $("#cpassword").val();

      //check to see if both fields are not empty.
      if((password != '') && (cpassword != ''))
      {
        if(password == cpassword)
        {
          $( "form:first" ).submit();
        }
        else {
          alert("The passwords do not match.");
          preventDefault();
        }
      }
      else {
          alert("The password field can not be empty unless you choose to keep the same password.");
          preventDefault();
      }
    }
  }
}
