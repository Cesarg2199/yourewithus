# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 23.229.180.163 (MySQL 5.6.44-cll-lve)
# Database: yourewithus
# Generation Time: 2019-11-09 05:27:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bugs
# ------------------------------------------------------------

CREATE TABLE `bugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `userName` varchar(500) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table dailylog
# ------------------------------------------------------------

CREATE TABLE `dailylog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `participantid` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `activity` varchar(2000) NOT NULL,
  `notes` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table emailListing
# ------------------------------------------------------------

CREATE TABLE `emailListing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `exported` varchar(5) DEFAULT 'n',
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table expenseSheet
# ------------------------------------------------------------

CREATE TABLE `expenseSheet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `item` varchar(300) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `receipt` varchar(3) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table inclusion
# ------------------------------------------------------------

CREATE TABLE `inclusion` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `participantid` int(11) NOT NULL,
  `date` varchar(1000) NOT NULL,
  `pq1` varchar(1000) NOT NULL,
  `pq2` varchar(1000) NOT NULL,
  `pq3` varchar(1000) NOT NULL,
  `pq4` varchar(1000) NOT NULL,
  `pq5` varchar(1000) NOT NULL,
  `sq1` varchar(1000) NOT NULL,
  `sq2` varchar(1000) NOT NULL,
  `sq3` varchar(1000) NOT NULL,
  `sq4` varchar(1000) NOT NULL,
  `aq1` varchar(1000) NOT NULL,
  `aq2` varchar(1000) NOT NULL,
  `aq3` varchar(1000) NOT NULL,
  `aq4` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table intake
# ------------------------------------------------------------

CREATE TABLE `intake` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participantId` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `q1` varchar(500) DEFAULT NULL,
  `q2s1p1` varchar(500) DEFAULT NULL,
  `q2s1p2` varchar(500) DEFAULT NULL,
  `q2s2p1` varchar(500) DEFAULT NULL,
  `q2s2p2` varchar(500) DEFAULT NULL,
  `q2s3p1` varchar(500) DEFAULT NULL,
  `q2s3p2` varchar(500) DEFAULT NULL,
  `q2s4p1` varchar(500) DEFAULT NULL,
  `q2s4p2` varchar(500) DEFAULT NULL,
  `q3s1p1` varchar(500) DEFAULT NULL,
  `q3s1p2` varchar(500) DEFAULT NULL,
  `q3s2p1` varchar(500) DEFAULT NULL,
  `q3s2p2` varchar(500) DEFAULT NULL,
  `q3s3p1` varchar(500) DEFAULT NULL,
  `q3s3p2` varchar(500) DEFAULT NULL,
  `q3s4p1` varchar(500) DEFAULT NULL,
  `q3s4p2` varchar(500) DEFAULT NULL,
  `q4s1p1` varchar(500) DEFAULT NULL,
  `q4s1p2` varchar(500) DEFAULT NULL,
  `q4s2p1` varchar(500) DEFAULT NULL,
  `q4s2p2` varchar(500) DEFAULT NULL,
  `q4s3p1` varchar(500) DEFAULT NULL,
  `q4s3p2` varchar(500) DEFAULT NULL,
  `q4s4p1` varchar(500) DEFAULT NULL,
  `q4s4p2` varchar(500) DEFAULT NULL,
  `q5` varchar(500) DEFAULT NULL,
  `q6p1` varchar(500) DEFAULT NULL,
  `q6p2` varchar(500) DEFAULT NULL,
  `q6p3` varchar(500) DEFAULT NULL,
  `q6p4` varchar(500) DEFAULT NULL,
  `q6p5` varchar(500) DEFAULT NULL,
  `q7p1` varchar(500) DEFAULT NULL,
  `q7p2` varchar(500) DEFAULT NULL,
  `q7p3` varchar(500) DEFAULT NULL,
  `q7p4` varchar(500) DEFAULT NULL,
  `q7p5` varchar(500) DEFAULT NULL,
  `q8p1` varchar(500) DEFAULT NULL,
  `q8p2` varchar(500) DEFAULT NULL,
  `q8p3` varchar(500) DEFAULT NULL,
  `q9p1` varchar(500) DEFAULT NULL,
  `q9p2` varchar(500) DEFAULT NULL,
  `q9p3` varchar(500) DEFAULT NULL,
  `q9p4` varchar(500) DEFAULT NULL,
  `q9p5` varchar(500) DEFAULT NULL,
  `q10` varchar(500) DEFAULT NULL,
  `q11` varchar(500) DEFAULT NULL,
  `q12s1` varchar(500) DEFAULT NULL,
  `q12s2` varchar(500) DEFAULT NULL,
  `q12s3` varchar(500) DEFAULT NULL,
  `q12s4` varchar(500) DEFAULT NULL,
  `q12s5` varchar(500) DEFAULT NULL,
  `q12s6` varchar(500) DEFAULT NULL,
  `q12s7` varchar(500) DEFAULT NULL,
  `q12s8` varchar(500) DEFAULT NULL,
  `q13p1` varchar(500) DEFAULT NULL,
  `q13p2` varchar(500) DEFAULT NULL,
  `q13p3` varchar(500) DEFAULT NULL,
  `q14p1` varchar(500) DEFAULT NULL,
  `q14p2` varchar(500) DEFAULT NULL,
  `q14p3` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table messageboard
# ------------------------------------------------------------

CREATE TABLE `messageboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `participantId` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `message` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table participantaccess
# ------------------------------------------------------------

CREATE TABLE `participantaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participantId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table participantcalendar
# ------------------------------------------------------------

CREATE TABLE `participantcalendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `participantid` int(11) NOT NULL,
  `userid` int(11) DEFAULT '0',
  `username` varchar(200) DEFAULT 'None',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table participants
# ------------------------------------------------------------

CREATE TABLE `participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `school` varchar(100) NOT NULL,
  `group` varchar(200) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table ped
# ------------------------------------------------------------

CREATE TABLE `ped` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `participantId` varchar(500) DEFAULT NULL,
  `age` varchar(500) DEFAULT NULL,
  `ag1` varchar(500) DEFAULT NULL,
  `ag2` varchar(500) DEFAULT NULL,
  `ag3` varchar(500) DEFAULT NULL,
  `en1` varchar(500) DEFAULT NULL,
  `en2` varchar(500) DEFAULT NULL,
  `en3` varchar(500) DEFAULT NULL,
  `enrn1` varchar(500) DEFAULT NULL,
  `enrn2` varchar(500) DEFAULT NULL,
  `enrn3` varchar(500) DEFAULT NULL,
  `sn1` varchar(500) DEFAULT NULL,
  `sn2` varchar(500) DEFAULT NULL,
  `sn3` varchar(500) DEFAULT NULL,
  `snrn1` varchar(500) DEFAULT NULL,
  `snrn2` varchar(500) DEFAULT NULL,
  `snrn3` varchar(500) DEFAULT NULL,
  `sto1` varchar(500) DEFAULT NULL,
  `sto2` varchar(500) DEFAULT NULL,
  `sto3` varchar(500) DEFAULT NULL,
  `lto1` varchar(500) DEFAULT NULL,
  `lto2` varchar(500) DEFAULT NULL,
  `lto3` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table roles
# ------------------------------------------------------------

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table schoolaccess
# ------------------------------------------------------------

CREATE TABLE `schoolaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `schoolName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table schools
# ------------------------------------------------------------

CREATE TABLE `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table scoutingReport
# ------------------------------------------------------------

CREATE TABLE `scoutingReport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participantId` varchar(455) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `contactInfo` varchar(455) DEFAULT NULL,
  `guardian` varchar(455) DEFAULT NULL,
  `personal` varchar(455) DEFAULT NULL,
  `school` varchar(455) DEFAULT NULL,
  `offense` varchar(1500) DEFAULT NULL,
  `defense` varchar(1500) DEFAULT NULL,
  `key1` varchar(455) DEFAULT NULL,
  `key2` varchar(455) DEFAULT NULL,
  `key3` varchar(455) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tasks
# ------------------------------------------------------------

CREATE TABLE `tasks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `assignedFrom` int(11) DEFAULT NULL,
  `assignedTo` int(11) DEFAULT NULL,
  `dateCreated` date DEFAULT NULL,
  `dateDue` date DEFAULT NULL,
  `dateCompleted` date DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT 'Incomplete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tasksNotes
# ------------------------------------------------------------

CREATE TABLE `tasksNotes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `taskId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table timeSheet
# ------------------------------------------------------------

CREATE TABLE `timeSheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(45) DEFAULT NULL,
  `participantId` varchar(45) DEFAULT NULL,
  `activity` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `timeIn` varchar(45) DEFAULT NULL,
  `timeOut` varchar(45) DEFAULT NULL,
  `totalTime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table users
# ------------------------------------------------------------

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `roletitle` varchar(200) NOT NULL,
  `date` datetime DEFAULT NULL,
  `firstLogin` varchar(45) DEFAULT 'y',
  `active` varchar(11) DEFAULT 'y',
  `onboarding` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
