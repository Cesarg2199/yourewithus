<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model
{
	public function userReport()
  {
    $participant = $this->input->post('participant');
		$user = $this->input->post('user');
    $from = $this->input->post('dFrom');
    $to = $this->input->post('dTo');

		return $this->db->query("
			SELECT dailylog.id, dailylog.participantid, dailylog.date, dailylog.activity, dailylog.notes, CONCAT_WS(' ', users.firstname, users.lastname) as user
			FROM yourewithus.dailylog
			JOIN users on users.id = dailylog.userid
			WHERE dailylog.participantid = '".$participant."' and dailylog.date >= '".$from."' and dailylog.date <= '".$to."' + INTERVAL '1' DAY
		")->result_array();
  }

	public function userTimeSheet()
	{
		$user = $this->input->post('user');
    $from = $this->input->post('dFrom');
		$to = $this->input->post('dTo');

		$sql = "
			SELECT timeSheet.id, timeSheet.userId, CONCAT(participants.Firstname, ' ', participants.Lastname) as participant, timeSheet.activity, timeSheet.date, timeSheet.timeIn, timeSheet.timeOut, timeSheet.totalTime
			FROM timeSheet
			JOIN participants on participants.id = timeSheet.participantId
			where timeSheet.userId = '{$user}' and timeSheet.date >= '{$from}' and timeSheet.date <= '{$to}' + INTERVAL '1' DAY
		";

		$query = $this->db->query($sql);

		$data = array();
		if($query !== FALSE && $query->num_rows() > 0) { $data = $query->result_array(); }

		return $data;
	}

	public function userExpenseSheet()
	{
		$user = $this->input->post('user');
		$from = $this->input->post('dFrom');
		$to = $this->input->post('dTo');

		return $this->db->query("
			SELECT expenseSheet.*, CONCAT_WS(' ', users.firstname, users.lastname) as user
			FROM expenseSheet
			JOIN users on users.id = expenseSheet.userId
			WHERE expenseSheet.userId = '{$user}' and expenseSheet.date >= '{$from}' and expenseSheet.date <= '{$to}' + INTERVAL '1' DAY
		")->result_array();
	}

	public function expenseSheetRecent()
	{
		return $this->db->query("
		SELECT expenseSheet.*, CONCAT_WS(' ', users.firstname, users.lastname) as user
		FROM expenseSheet
		JOIN users on users.id = expenseSheet.userId
		LIMIT 10;
		")->result_array();
	}

	public function userTimeSheetHeadings()
	{
		$user = $this->input->post('user');
    $from = $this->input->post('dFrom');
    $to = $this->input->post('dTo');

		//get the subtotal for each hour set and total hour set for interval
		$intervalTotal = $this->db->query("SELECT round(sum(totalTime), 2) as intervalTime FROM timeSheet WHERE userId = '{$user}' and date >= '{$from}' and date <= '{$to}' + INTERVAL '1' DAY")->result_array()[0];
		$adultLivingTotal = $this->db->query("SELECT round(sum(totalTime), 2) as adultLiving FROM timeSheet where activity = 'Adult Living' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}' + INTERVAL '1' DAY")->result_array()[0];
		$employmentTotal = $this->db->query("SELECT round(sum(totalTime), 2) as employment FROM timeSheet where activity = 'employment' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}' + INTERVAL '1' DAY")->result_array()[0];
		$socialInclusionTotal = $this->db->query("SELECT round(sum(totalTime), 2) as socialInclusion FROM timeSheet where activity = 'Social Inclusion' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}' + INTERVAL '1' DAY")->result_array()[0];
		$socialTotal = $this->db->query("SELECT round(sum(totalTime), 2) as social FROM timeSheet where activity = 'Social' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}' + INTERVAL '1' DAY")->result_array()[0];
		$adminTotal = $this->db->query("SELECT round(sum(totalTime), 2) as admin FROM timeSheet where activity = 'Administrative' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}' + INTERVAL '1' DAY")->result_array()[0];

		//construct array of the results
		$results = array(
			'intervalTotal' => $intervalTotal,
			'adultLivingTotal' => $adultLivingTotal,
			'employmentTotal' => $employmentTotal,
			'socialInclusionTotal' => $socialInclusionTotal,
			'socialTotal' => $socialTotal,
			'adminTotal' => $adminTotal
		);

		//return the results
		return $results;
	}

	public function inclusionReportMin()
	{
		$participant = $this->input->post('participant');
    $from = $this->input->post('dFrom');
    $to = $this->input->post('dTo');

		$values = $this->db->query("
		SELECT inclusion.*, CONCAT_WS(' ', users.firstname, users.lastname) as user
		FROM yourewithus.inclusion
		JOIN users on users.id = inclusion.userid
		WHERE inclusion.participantid = '".$participant."' and inclusion.date >= '".$from."' and inclusion.date <= '".$to."' + INTERVAL '1' DAY
		")->result_array();

		//Calculating the sub rows to display as an overall score
		$calculated = array();
		for($i = 0; $i < count($values); $i++)
		{
			$calculated[$i]['id'] = $values[$i]['id'];
			$calculated[$i]['date'] = $values[$i]['date'];
			$calculated[$i]['user'] = $values[$i]['user'];
			$calculated[$i]['pq'] = (int)$values[$i]['pq1'] + (int)$values[$i]['pq2'] + (int)$values[$i]['pq3'] + (int)$values[$i]['pq4'] + (int)$values[$i]['pq5'];
			$calculated[$i]['sq'] = (int)$values[$i]['sq1'] + (int)$values[$i]['sq2'] + (int)$values[$i]['sq3'] + (int)$values[$i]['sq4'];
			$calculated[$i]['aq'] = (int)$values[$i]['aq1'] + (int)$values[$i]['aq2'] + (int)$values[$i]['aq3'] + (int)$values[$i]['aq4'];
		}

		return $calculated;
	}

	public function inclusionReportFull()
 	{
		$id = $this->input->post('id');
		$this->db->select('*');
		$this->db->where('id =', $id);
		return $this->db->get('inclusion')->result_array();
 	}

	public function exportDailyReportFull($participant, $from, $to)
	{
		return $this->db->query("
			SELECT dailylog.id, dailylog.participantid, dailylog.date, dailylog.activity, dailylog.notes, CONCAT_WS(' ', users.firstname, users.lastname) as user
			FROM yourewithus.dailylog
			JOIN users on users.id = dailylog.userid
			WHERE dailylog.participantid = '".$participant."' and dailylog.date >= '".$from."' and dailylog.date <= '".$to."' + INTERVAL '1' DAY
		")->result_array();
	}

	public function exportInclusionReportFull($participant, $from, $to)
	{
		return $this->db->query("
		SELECT inclusion.*, CONCAT_WS(' ', users.firstname, users.lastname) as user
		FROM yourewithus.inclusion
		JOIN users on users.id = inclusion.userid
		WHERE inclusion.participantid = '".$participant."' and inclusion.date >= '".$from."' and inclusion.date <= '".$to."' + INTERVAL '1' DAY
		")->result_array();
	}

	public function exportTimeSheet($user, $from, $to)
	{
		return $this->db->query("
			SELECT timeSheet.id, timeSheet.userId, CONCAT(participants.Firstname, ' ', participants.Lastname) as participant, timeSheet.activity, timeSheet.date, timeSheet.timeIn, timeSheet.timeOut, timeSheet.totalTime
			FROM timeSheet
			JOIN participants on participants.id = timeSheet.participantId
			where timeSheet.userId = '{$user}' and timeSheet.date >= '{$from}' and timeSheet.date <= '{$to}' + INTERVAL '1' DAY
		")->result_array();
	}
}
