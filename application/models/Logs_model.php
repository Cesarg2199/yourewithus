<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/New_York');
class Logs_model extends CI_Model
{
	public function newDailyLog()
	{
		$participantId = $this->input->post('participant');
		$activity = $this->input->post('activity');
		$notes = $this->input->post('notes');
		$date = $this->input->post('date');

		$checkDate = strtotime($date);

		if(!$checkDate) {
			$date = date('Y-m-d');
		}

		if(isset($participantId)){
			$data = array(
		        'userId' => $this->session->userId,
		        'participantId' => $participantId,
		        'date' => $date,
	        	'activity' => $activity,
	        	'notes' => $notes
			);

			$this->db->insert('dailylog', $data);
		}

		return true;
	}

	public function newIA()
	{
		$participantId = $this->input->post('participant');

		$pq1 = $this->input->post('pq1', true);
		$pq2 = $this->input->post('pq2', true);
		$pq3 = $this->input->post('pq3', true);
		$pq4 = $this->input->post('pq4', true);
		$pq5 = $this->input->post('pq5', true);

		$sq1 = $this->input->post('sq1', true);
		$sq2 = $this->input->post('sq2', true);
		$sq3 = $this->input->post('sq3', true);
		$sq4 = $this->input->post('sq4', true);

		$aq1 = $this->input->post('aq1', true);
		$aq2 = $this->input->post('aq2', true);
		$aq3 = $this->input->post('aq3', true);
		$aq4 = $this->input->post('aq4', true);

		if(isset($participantId)){
			$data = array(
	      'userId' => $this->session->userId,
	      'participantId' => $participantId,
	      'date' => date('Y-m-d h:m'),
	    	'pq1' => $pq1,
				'pq2' => $pq2,
				'pq3' => $pq3,
				'pq4' => $pq4,
				'pq5' => $pq5,
				'sq1' => $sq1,
				'sq2' => $sq2,
				'sq3' => $sq3,
				'sq4' => $sq4,
				'aq1' => $aq1,
				'aq2' => $aq2,
				'aq3' => $aq3,
				'aq4' => $aq4
			);

			$this->db->insert('inclusion', $data);
		}

		return true;
	}

	public function saveTimeSheet()
	{
		$timeIn = $this->input->post('timeIn');
		$timeOut = $this->input->post('timeOut');

		$cleanTimeIn = str_replace(" : ", ":", $timeIn);
		$cleanTimeOut = str_replace(" : ", ":", $timeOut);

		$convertTI = strtotime($cleanTimeIn);
		$convertTO = strtotime($cleanTimeOut);

		$totalTime = round((($convertTO - $convertTI) / (3600)), 2);

		$data = array(
			'userId' => $this->session->userId,
			'participantId' => $this->input->post('participantId'),
			'activity' => $this->input->post('activity'),
			'date' => $this->input->post('date'),
			'timeIn' => $cleanTimeIn,
			'timeOut' => $cleanTimeOut,
			'totalTime' => $totalTime
		);

		$this->db->insert('timeSheet', $data);

		return true;
	}

	public function newExpenseSheet()
	{
		 $date = $this->input->post('date');
		 $item = $this->input->post('item');
		 $amount = $this->input->post('amount');
		 $receipt = $this->input->post('receipt');
		 $notes = $this->input->post('notes');

		 $data = array(
 			'userId' => $this->session->userId,
 			'date' => $date,
			'item' => $item,
			'amount' => $amount,
			'receipt' => $receipt,
			'notes' => $notes
 		);

 		$this->db->insert('expenseSheet', $data);

 		return true;


	}
}
