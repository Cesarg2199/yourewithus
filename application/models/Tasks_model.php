<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/New_York');

class Tasks_model extends CI_Model
{
  public function assignTask()
  {
    $assignedTo = $this->input->post('assignTo');
    $dateDue = $this->input->post('assignDue');
    $description = $this->input->post('description');
    $dateCreated = date('Y-m-d');
    $assignedFrom = $this->session->userId;

    $data = array(
      'assignedFrom' => $assignedFrom,
      'assignedTo' => $assignedTo,
      'dateCreated' => $dateCreated,
      'dateDue' => $dateDue,
      'description' => $description
    );

    $this->db->insert('tasks', $data);
  }

  public function reassignTask()
  {
    $assignedTo = $this->input->post('assignTo');
    $dateDue = $this->input->post('assignDue');
    $description = $this->input->post('description');
    $dateCreated = date('Y-m-d');
    $assignedFrom = $this->session->userId;
    $taskId = $this->input->post('taskId');

    $data = array(
      'assignedFrom' => $assignedFrom,
      'assignedTo' => $assignedTo,
      'dateCreated' => $dateCreated,
      'dateDue' => $dateDue,
      'dateCompleted' => NULL,
      'description' => $description,
      'status' => "Incomplete"
    );

    $this->db->where('id', $taskId);
    $this->db->update('tasks', $data);
  }

  public function getTask($taskId)
  {
    return $this->db->query("
    SELECT tasks.id, tasks.assignedTo, DATE_FORMAT(dateDue, '%M %d, %Y') as dateDue, tasks.description, CONCAT(users.Firstname, ' ', users.Lastname) as assignedToName
    FROM tasks
    JOIN users on tasks.assignedTo = users.id
    WHERE tasks.id = {$taskId}
    ")->result_array();
  }

  public function getPendingTasks()
  {
    $assignedTo = $this->session->userId;

    return $this->db->query("
    SELECT tasks.id, tasks.assignedFrom, tasks.assignedTo, DATE_FORMAT(dateDue, '%M %d, %Y') as dateDue, tasks.description, tasks.status, CONCAT(users.roletitle, ': <br>', users.Firstname, ' ', users.Lastname) as assignedBy
    FROM tasks
    JOIN users on tasks.assignedFrom = users.id
    WHERE tasks.status = 'Incomplete' and assignedTo = {$this->session->userId}
    ")->result_array();
  }

  public function getCompletedTasks()
  {
    $assignedTo = $this->session->userId;

    return $this->db->query("
    SELECT tasks.id, tasks.assignedFrom, tasks.assignedTo, DATE_FORMAT(dateDue, '%M %d, %Y') as dateDue, DATE_FORMAT(dateCompleted, '%M %d, %Y') as dateCompleted, tasks.description, tasks.status, CONCAT(users.roletitle, ': <br>', users.Firstname, ' ', users.Lastname) as assignedBy
    FROM tasks
    JOIN users on tasks.assignedFrom = users.id
    WHERE tasks.status = 'Completed' and assignedTo = {$this->session->userId}
    ")->result_array();
  }

  public function getSentTasks()
  {
    $assignedFrom = $this->session->userId;

    return $this->db->query("
    SELECT tasks.id, tasks.assignedFrom, tasks.assignedTo, DATE_FORMAT(dateDue, '%M %d, %Y') as dateDue, DATE_FORMAT(dateCompleted, '%M %d, %Y') as dateCompleted, tasks.description, tasks.status, CONCAT(users.roletitle, ': <br>', users.Firstname, ' ', users.Lastname) as assignedTo
    FROM tasks
    JOIN users on tasks.assignedTo = users.id
    WHERE tasks.assignedFrom = {$assignedFrom} and assignedFrom = {$this->session->userId}
    ")->result_array();
  }

  public function getAllTasks()
  {
    return $this->db->query("
    SELECT tasks.id, tasks.assignedFrom, tasks.dateCompleted, DATE_FORMAT(dateCreated, '%M %d, %Y') as dateCreated, tasks.assignedTo, DATE_FORMAT(dateDue, '%M %d, %Y') as dateDue, DATE_FORMAT(dateCompleted, '%M %d, %Y') as dateCompleted, tasks.description, tasks.status, CONCAT(users.roletitle, ': <br>', users.Firstname, ' ', users.Lastname) as assignedBy, CONCAT(users2.roletitle, ': <br>', users2.Firstname, ' ', users2.Lastname) as assignedTo
    FROM tasks
    JOIN users on tasks.assignedFrom = users.id
    JOIN users users2 on tasks.assignedTo = users2.id 
    ")->result_array();
  }

  public function completedTask() //Marks a task as complete by setting completed to the current date and employee to the current user
	{
		$taskId = $this->input->post('taskId');

		$dateCompleted = date('Y-m-d');
    $status = "Completed";

		$data = array(
			'dateCompleted' => $dateCompleted,
			'status' => $status
		);

		$this->db->where('id', $taskId);
		$this->db->update('tasks', $data);
	}

  public function uncompleteTask() //Marks a task as uncomplete by setting completes to "n" and employee to what it was originally
	{
		$taskId = $this->input->post('taskId');
    $dateCompleted = NULL;
		$status = "Incomplete";

    $data = array(
			'dateCompleted' => $dateCompleted,
			'status' => $status
		);

    $this->db->where('id', $taskId);
		$this->db->update('tasks', $data);
	}

  public function deleteSentTask() //Removes task from tasks table as well as any note accociated with it
	{
		$taskId = $this->input->post('taskId');
		$this->db->delete('tasks', array('id' => $taskId));
		$this->db->delete('tasksNotes', array('taskId' => $taskId));
	}

  public function getNotes() //Returns all notes that are accociated with the specific task
	{
		$taskId = $this->input->post('taskId');

		$result = $this->db->query("
    SELECT tasksNotes.id, tasksNotes.taskId, tasksNotes.message, DATE_FORMAT(tasksNotes.date, '%M %d, %Y') as date, CONCAT(users.roletitle, ': <br>', users.Firstname, ' ', users.Lastname) as user
    FROM tasksNotes
    JOIN users on tasksNotes.userId = users.id
    WHERE taskId = '".$taskId."'
    ")->result_array();

		return $result;
	}

	public function addNote() //Adds note to tasknotes table
	{
		$taskId = $this->input->post('taskId');
		$message = $this->input->post('message');
		$userId = $this->session->userId;
    $date = date('Y-m-d');

		$data = array(
		   'taskId' => $taskId,
			 'userId' => $userId,
       'date' => $date,
			 'message' => $message
		);

		$this->db->insert('tasksNotes', $data);
	}

	public function deleteNote() //Deletes note form tasknotes table
	{
		$id = $this->input->post('id');

		$this->db->where('id', $id);
		$this->db->delete('tasksNotes');
	}

}
