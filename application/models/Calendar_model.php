<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar_model extends CI_Model
{
	/*Read the data from DB */
	Public function getEvents()
	{
		return $this->db->query("SELECT *  from participantcalendar where participantId = '".$this->input->get('participantId')."'")->result_array();
	}

	/*Create new events */
	Public function addEvent($userId, $userName, $title, $description, $start, $end, $participantid)
	{
		$data = array(
			'title' => $title,
			'description' => $description,
			'start' => $start,
			'end' => $end,
			'participantid' => $participantid,
			'userid' => $userId,
			'username' => $userName
		);

		$this->db->insert('participantcalendar', $data);
	}

	/*Update  event */
	Public function updateEvent($userId, $userName, $id, $title, $description, $start, $end, $participantid)
	{

		$data = array(
			'title' => $title,
			'description' => $description,
			'start' => $start,
			'end' => $end,
			'participantid' => $participantid,
			'userid' => $userId,
			'username' => $userName
		);

		$this->db->where('id', $id);
		$this->db->update('participantcalendar', $data);
	}

	/*Delete event */
	Public function deleteEvent()
	{
		$id = $this->input->get('id');
		$this->db->where('id', $id);
		$this->db->delete('participantcalendar');
	}
}
