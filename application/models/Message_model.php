<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/New_York');
class Message_model extends CI_Model
{
  public function loadPosts()
	{
    //Grab the post variable
		$participantId = $this->input->post('id');

    //Grab the posts related to this participant
    $this->db->select('*');
    $this->db->select("DATE_FORMAT(date, '%M %d, %Y') as date");
		$this->db->where('participantId', $participantId);
		$query = $this->db->get('messageboard')->result_array();

    //return all the posts related to this participant
    return $query;
	}

  public function addPost($participantId, $message)
  {
    $data = array(
        'userId' => $this->session->userId,
        'participantId' => $participantId,
        'name' => ucfirst($this->session->firstname).' '.ucfirst($this->session->lastname),
        'date' => date('Y-m-d h:m'),
        'message' => $message
    );

    $this->db->insert('messageboard', $data);
  }

  public function deletePost()
  {
    $postId = $this->input->post('postId');
    $this->db->where('id', $postId);
    $this->db->delete('messageboard');
  }
}
