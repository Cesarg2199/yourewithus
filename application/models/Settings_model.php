<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings_model extends CI_Model
{
  public function changeEmail()
  {
    $email = $this->input->post('email');
    $confirmEmail = $this->input->post('confirmemail');

    if($email == $confirmEmail)
    {
      if(isset($email))
      {
        $data = array(
            'email' => $email
        );
      }
    }

    $this->db->where('id', $this->session->userId);
    $this->db->update('users', $data);
  }

  public function changePassword()
  {
    $password = $this->input->post('password');
    $confirmPassword = $this->input->post('confirmpassword');

    if($password == $confirmPassword)
    {
      if(isset($password))
      {
        $securePassword = password_hash($password, PASSWORD_BCRYPT);

        $data = array(
            'password' => $securePassword
        );
      }
    }

    $this->db->where('id', $this->session->userId);
    $this->db->update('users', $data);
  }
}
