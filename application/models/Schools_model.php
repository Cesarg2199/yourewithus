<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/New_York');

class Schools_model extends CI_Model
{
  public function getAllSchools()
  {
      return $this->db->query("Select * from schools ORDER BY name asc")->result_array();
  }

  public function getAllSchoolsLinkedUser($userId)
  {
    return $this->db->query("Select id, schoolName from schoolaccess where userId = '".$userId."'")->result_array();
  }

  public function getEntireSchoolList()
  {
    $this->db->order_by("name", "asc");
    return $this->db->get('schools')->result_array();
  }

  public function addSchool()
  {
    $name = $this->input->post('name');
    $name = ucwords($name);

    //Check if school exist
    $check = $this->schoolExist($name);

    if(!$check)
    {
      $data = array(
        'name' => $name
      );

      $this->db->insert('schools', $data);
    }
  }

  public function schoolExist($name)
  {
    $name = "%".$name."%";
    $check = $this->db->query("SELECT * FROM schools WHERE name LIKE '".$name."'")->num_rows();

    if($check > 0)
    {
      return true;
    }
    else {
      return false;
    }
  }

  public function editSchool()
  {
    $id = $this->input->post('id');
    $name = $this->input->post('name');
    $name = ucwords($name);

    $data = array(
      'name' => $name
    );

    $this->db->where('id', $id);
    $this->db->update('schools', $data);
  }
}
