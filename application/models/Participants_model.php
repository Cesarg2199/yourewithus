<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants_model extends CI_Model
{
	public function getParticipants()
	{
		if($this->session->role == "1"){
			$participants = $this->db->query("select * from participants")->result_array();
			return $participants;
		}
		elseif ($this->session->role == "7") {
			//list of all the schools the ccc is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$this->session->userId."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			return $this->db->query("select * from participants where school in ({$schoolList})")->result_array();
		}
		elseif ($this->session->role == "4") {
			//list of all the schools the ccc is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$this->session->userId."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			return $this->db->query("select * from participants where school in ({$schoolList})")->result_array();
		}
	}

	public function getParticipant($participantId)
	{
		return $this->db->query("select * from participants where id = '".$participantId."' LIMIT 1")->result_array();
	}

	public function deleteParticipant()
	{
		$participantId = $this->input->post('participantId');

		$this->db->where('id', $participantId);
		$check = $this->db->delete('participants');

		if($check){ return true; }
		else{ return false; }
	}

	public function insertParticipant()
	{
		$firstname = $this->input->post('firstname', true);
		$lastname = $this->input->post('lastname', true);
		$school = $this->input->post('school', true);
		$group = $this->input->post('group', true);

		$data = array(
	        'firstname' => $firstname,
	        'lastname' => $lastname,
	        'school' => $school,
	        'group' => $group,
					'date' => Date('Y-m-d H:i:s')
		);

		$this->db->insert('participants', $data);
	}

	public function updateParticipant()
	{
		$id = $this->input->post('id', true);
		$firstname = $this->input->post('firstname', true);
		$lastname = $this->input->post('lastname', true);
		$school = $this->input->post('school', true);
		$group = $this->input->post('group', true);

			$data = array(
		        'firstname' => $firstname,
		        'lastname' => $lastname,
		        'school' => $school,
		        'group' => $group
				);

			$this->db->where('id', $id);
			$this->db->update('participants', $data);
	}

	public function getUserParticipants()
	{
		if(($this->session->role == "2") or ($this->session->role == "3") or ($this->session->role == "5") or ($this->session->role == "6") or ($this->session->role == "8"))
		{

			$query = $this->db->query("Select participantId from participantaccess where userId = '".$this->session->userId."'");
			$access = array();
			if($query !== FALSE && $query->num_rows() > 0) { $access = $query->result_array(); }
			
			$listOfParticipants = array();

			if($access){
				foreach ($access as $id)
				{
					$listOfParticipants[] = $id["participantId"];
				}

				$list = array();

				//Compiled Query
				$sql = "select * from participants where id in(".$listOfParticipants[0];
				for($i = 1; $i < count($listOfParticipants); $i++)
				{
					$sql .= ", ".$listOfParticipants[$i];
				}
				$sql .= ")";

				$query = $this->db->query($sql);

				$data = array();
				if($query !== FALSE && $query->num_rows() > 0) { $data = $query->result_array(); }

				return $data;
			}
			else
			{
				return false;
			}
		}
		elseif($this->session->role == "1")
		{
			$access = $this->db->query("Select * from participants")->result_array();
			return $access;
		}
		elseif ($this->session->role == "4" or ($this->session->role == "7"))
		{
			//list of all the schools the ccc is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$this->session->userId."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			return $this->db->query("select * from participants where school in ({$schoolList})")->result_array();
		}
	}

	public function linkedUsers($participantId)
	{
		$usersList = $this->db->query("select userId from participantaccess where participantId = {$participantId}")->result_array();
		$userCompiledIds = array();

		foreach ($usersList as $user)
		{
			$userCompiledIds[] = $user['userId'];
		}
		
		//Compiled Query
		$sql = "select id, firstname, lastname, roletitle from users where id in(".$userCompiledIds[0];

		for($i = 1; $i < count($userCompiledIds); $i++)
		{
			$sql .= ", ".$userCompiledIds[$i];
		}

		$sql .= ") and active = 'y'";
		$query = $this->db->query($sql);

		$data = array();
		if($query !== FALSE && $query->num_rows() > 0) { $data = $query->result_array(); }

		return $data;

	}

	public function participantsDailyLog($participantId)
	{
		return $this->db->query("
			SELECT dailylog.id, dailylog.userid, DATE_FORMAT(dailylog.date, '%M %d %Y %r') as date, dailylog.activity, dailylog.notes, CONCAT(users.firstname, ' ', users.lastname) as user
			FROM yourewithus.dailylog
			JOIN users on users.id = dailylog.userid
			WHERE dailylog.participantid = '".$participantId."'
			ORDER BY dailylog.date DESC
			LIMIT 5
		")->result_array();
	}

	public function participantsRecentIA($participantId)
	{
		$values = $this->db->query("
		SELECT inclusion.*, CONCAT(users.firstname, ' ', users.lastname) as user
		FROM inclusion
		JOIN users on users.id = inclusion.userid
		WHERE inclusion.participantid = '".$participantId."'
		ORDER BY inclusion.date DESC
		LIMIT 5
		")->result_array();

		//Calculating the sub rows to display as an overall score
		$calculated = array();
		for($i = 0; $i < count($values); $i++)
		{
			$calculated[$i]['id'] = $values[$i]['id'];
			$calculated[$i]['date'] = $values[$i]['date'];
			$calculated[$i]['user'] = $values[$i]['user'];
			$calculated[$i]['pq'] = (int)$values[$i]['pq1'] + (int)$values[$i]['pq2'] + (int)$values[$i]['pq3'] + (int)$values[$i]['pq4'] + (int)$values[$i]['pq5'];
			$calculated[$i]['sq'] = (int)$values[$i]['sq1'] + (int)$values[$i]['sq2'] + (int)$values[$i]['sq3'] + (int)$values[$i]['sq4'];
			$calculated[$i]['aq'] = (int)$values[$i]['aq1'] + (int)$values[$i]['aq2'] + (int)$values[$i]['aq3'] + (int)$values[$i]['aq4'];
		}

		return $calculated;
	}

	public function getIntakeForm($participantId)
	{
		return $this->db->query("select * from intake where participantId = '{$participantId}'")->result_array();
	}

	public function saveIntakeForm()
	{
		$participantId = $this->input->post('participantId');
		$q1 = $this->input->post('q1');
		$q2s1p1 = $this->input->post('q2s1p1');
		$q2s1p2 = $this->input->post('q2s1p2');
		$q2s2p1 = $this->input->post('q2s2p1');
		$q2s2p2 = $this->input->post('q2s2p2');
		$q2s3p1 = $this->input->post('q2s3p1');
		$q2s3p2 = $this->input->post('q2s3p2');
		$q2s4p1 = $this->input->post('q2s4p1');
		$q2s4p2 = $this->input->post('q2s4p2');
		$q3s1p1 = $this->input->post('q3s1p1');
		$q3s1p2 = $this->input->post('q3s1p2');
		$q3s2p1 = $this->input->post('q3s2p1');
		$q3s2p2 = $this->input->post('q3s2p2');
		$q3s3p1 = $this->input->post('q3s3p1');
		$q3s3p2 = $this->input->post('q3s3p2');
		$q3s4p1 = $this->input->post('q3s4p1');
		$q3s4p2 = $this->input->post('q3s4p2');
		$q4s1p1 = $this->input->post('q4s1p1');
		$q4s1p2 = $this->input->post('q4s1p2');
		$q4s2p1 = $this->input->post('q4s2p1');
		$q4s2p2 = $this->input->post('q4s2p2');
		$q4s3p1 = $this->input->post('q4s3p1');
		$q4s3p2 = $this->input->post('q4s3p2');
		$q4s4p1 = $this->input->post('q4s4p1');
		$q4s4p2 = $this->input->post('q4s4p2');
		$q5 = $this->input->post('q5');
		$q6p1 = $this->input->post('q6p1');
		$q6p2 = $this->input->post('q6p2');
		$q6p3 = $this->input->post('q6p3');
		$q6p4 = $this->input->post('q6p4');
		$q6p5 = $this->input->post('q6p5');
		$q7p1 = $this->input->post('q7p1');
		$q7p2 = $this->input->post('q7p2');
		$q7p3 = $this->input->post('q7p3');
		$q7p4 = $this->input->post('q7p4');
		$q7p5 = $this->input->post('q7p5');
		$q8p1 = $this->input->post('q8p1');
		$q8p2 = $this->input->post('q8p2');
		$q8p3 = $this->input->post('q8p3');
		$q9p1 = $this->input->post('q9p1');
		$q9p2 = $this->input->post('q9p2');
		$q9p3 = $this->input->post('q9p3');
		$q9p4 = $this->input->post('q9p4');
		$q9p5 = $this->input->post('q9p5');
		$q10 = $this->input->post('q10');
		$q11 = $this->input->post('q11');
		$q12s1 = $this->input->post('q12s1');
		$q12s2 = $this->input->post('q12s2');
		$q12s3 = $this->input->post('q12s3');
		$q12s4 = $this->input->post('q12s4');
		$q12s5 = $this->input->post('q12s5');
		$q12s6 = $this->input->post('q12s6');
		$q12s7 = $this->input->post('q12s7');
		$q12s8 = $this->input->post('q12s8');
		$q13p1 = $this->input->post('q13p1');
		$q13p2 = $this->input->post('q13p2');
		$q13p3 = $this->input->post('q13p3');
		$q14p3 = $this->input->post('q14p1');
		$q14p3 = $this->input->post('q14p2');
		$q14p3 = $this->input->post('q14p3');

		//Delete current record if exist and insert new one. delete by participant id
		$this->deleteIntakeForm($participantId);

		$data = array(
	    'participantId' => $participantId,
			'q1' => $q1,
			'q2s1p1' => $q2s1p1,
			'q2s1p2' => $q2s1p2,
			'q2s2p1' => $q2s2p1,
			'q2s2p2' => $q2s2p2,
			'q2s3p1' => $q2s3p1,
			'q2s3p2' => $q2s3p2,
			'q2s4p1' => $q2s4p1,
			'q2s4p2' => $q2s4p2,
			'q3s1p1' => $q3s1p1,
			'q3s1p2' => $q3s1p2,
			'q3s2p1' => $q3s2p1,
			'q3s2p2' => $q3s2p2,
			'q3s3p1' => $q3s3p1,
			'q3s3p2' => $q3s3p2,
			'q3s4p1' => $q3s4p1,
			'q3s4p2' => $q3s4p2,
			'q4s1p1' => $q4s1p1,
			'q4s1p2' => $q4s1p2,
			'q4s2p1' => $q4s2p1,
			'q4s2p2' => $q4s2p2,
			'q4s3p1' => $q4s3p1,
			'q4s3p2' => $q4s3p2,
			'q4s4p1' => $q4s4p1,
			'q4s4p2' => $q4s4p2,
			'q5' => $q5,
			'q6p1' => $q6p1,
			'q6p2' => $q6p2,
			'q6p3' => $q6p3,
			'q6p4' => $q6p4,
			'q6p5' => $q6p5,
			'q7p1' => $q7p1,
			'q7p2' => $q7p2,
			'q7p3' => $q7p3,
			'q7p4' => $q7p4,
			'q7p5' => $q7p5,
			'q8p1' => $q8p1,
			'q8p2' => $q8p2,
			'q8p3' => $q8p3,
			'q9p1' => $q9p1,
			'q9p2' => $q9p2,
			'q9p3' => $q9p3,
			'q9p4' => $q9p4,
			'q9p5' => $q9p5,
			'q10' => $q10,
			'q11' => $q11,
			'q12s1' => $q12s1,
			'q12s2' => $q12s2,
			'q12s3' => $q12s3,
			'q12s4' => $q12s4,
			'q12s5' => $q12s5,
			'q12s6' => $q12s6,
			'q12s7' => $q12s7,
			'q12s8' => $q12s8,
			'q13p1' => $q13p1,
			'q13p2' => $q13p2,
			'q13p3' => $q13p3,
			'q14p1' => $q14p1,
			'q14p2' => $q14p2,
			'q14p3' => $q14p3,
		);

		$this->db->insert('intake', $data);
	}

	public function deleteIntakeForm($participantId)
	{
		$this->db->where('participantId', $participantId);
		$this->db->delete('intake');
	}

	public function getScoutingReport($participantId)
	{
		return $this->db->query("select * from scoutingReport where participantId = '{$participantId}'")->result_array();
	}

	public function saveScoutingReport()
	{
		//Collect Input's
		$participantId = $this->input->post('participantId');

		//Delete existing record if it exist
		$this->deleteScoutingReport($participantId);

		//Save values to db
		$data = array(
      'participantId' => $participantId,
			'contactInfo' => $this->input->post('contactInfo'),
			'guardian' => $this->input->post('guardian'),
			'personal' => $this->input->post('personal'),
			'school' => $this->input->post('school'),
			'offense' => $this->input->post('offense'),
			'defense' => $this->input->post('defense'),
			'key1' => $this->input->post('key1'),
			'key2' => $this->input->post('key2'),
			'key3' => $this->input->post('key3'),
		);

		$this->db->insert('scoutingReport', $data);

	}

	public function deleteScoutingReport($participantId)
	{
		$this->db->where('participantId', $participantId);
		$this->db->delete('scoutingReport');
	}

	public function getPED($participantId)
	{
		return $this->db->query("select * from ped where participantId = '{$participantId}'")->result_array();
	}

	public function savePositiveExperienceDesign()
	{
		//Collect Input's
		$participantId = $this->input->post('participantId');

		//Delete existing record if it exist
		$this->deletePositiveExperienceDesign($participantId);

		//Save values to db
		$data = array(
      'participantId' => $participantId,
      'age' => $this->input->post('age'),
			'ag1' => $this->input->post('ag1'),
			'ag2' => $this->input->post('ag2'),
			'ag3' => $this->input->post('ag3'),
			'en1' => $this->input->post('en1'),
			'en2' => $this->input->post('en2'),
			'en3' => $this->input->post('en3'),
			'enrn1' => $this->input->post('enrn1'),
			'enrn2' => $this->input->post('enrn2'),
			'enrn3' => $this->input->post('enrn3'),
			'sn1' => $this->input->post('sn1'),
			'sn2' => $this->input->post('sn2'),
			'sn3' => $this->input->post('sn3'),
			'snrn1' => $this->input->post('snrn1'),
			'snrn2' => $this->input->post('snrn2'),
			'snrn3' => $this->input->post('snrn3'),
			'sto1' => $this->input->post('sto1'),
			'sto2' => $this->input->post('sto2'),
			'sto3' => $this->input->post('sto3'),
			'lto1' => $this->input->post('lto1'),
			'lto2' => $this->input->post('lto2'),
			'lto3' => $this->input->post('lto3')
		);

		$this->db->insert('ped', $data);
	}

	public function deletePositiveExperienceDesign($participantId)
	{
		$this->db->where('participantId', $participantId);
		$this->db->delete('ped');
	}
}
