<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/New_York');
class Email_model extends CI_Model
{
  public function emailGroup($participantName, $message, $listOfRelatedUsers)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/lib/sendmail';
		$config['charset']  = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";
		$this->email->initialize($config);

    //grab the first and last name from who posted the message
    $name = ucfirst($this->session->firstname).' '.ucfirst($this->session->lastname);

    //get the name of the participant from the array
    $participant = ucfirst($participantName['firstname']).' '.ucfirst($participantName['lastname']);

    //construct the new message
    $message = "<b>New message posted on ".$participant."'s board from ".$name."</b>.<br><br> $message";

    //grab everyone related to the message and email them
    for($i = 0; $i < count($listOfRelatedUsers); $i++)
    {
      $this->email->from('donotreply@yourewithus.org', 'Youre With Us!');
  		$this->email->to($listOfRelatedUsers[$i][0]['email']); //rework this to follow ci model online
  		$this->email->subject('Message Board Update');
  		$this->email->message($message);
  		$this->email->send();
    }

    return true;
	}

  public function emailSupport($userName, $userEmail, $participantName, $title, $description, $start, $end, $type)
  {
    $this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/lib/sendmail';
		$config['charset']  = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";
		$this->email->initialize($config);

    //construct the new message
    $message = "<b>".$type." schedule event for ".$participantName."</b>.
    <br><br>
    ".$start." - ".$end."
    ".$title."
    <br><br>
    ".$description."";

    $this->email->from('donotreply@yourewithus.org', 'Youre With Us!');
    $this->email->to($userEmail);
    $this->email->subject('Calendar Event');
    $this->email->message($message);
    $this->email->send();
  }

  public function emailBug($description)
  {
    $this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/lib/sendmail';
		$config['charset']  = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";
		$this->email->initialize($config);

    //grab the user id of who sent it
    $userid = $this->session->userId;

    //grab the first and last name from who submitted the bug
    $name = ucfirst($this->session->firstname).' '.ucfirst($this->session->lastname);

    //grab the date they submitted it
    $date = Date('Y-m-d H:i:s');

    //construct the new message
    $message = "<b>New bug reported User Id: ".$userid." User Name: ".$name." posted on ".$date."</b>.<br><br>$description";

    $this->email->from('donotreply@yourewithus.org', 'Youre With Us!');
    $this->email->to("cesarguerrero2111@gmail.com"); //rework this to follow ci model online
    $this->email->subject('New Bug Reported');
    $this->email->message($message);
    $this->email->send();

    return true;

  }
}
