<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe_model extends CI_Model
{
  public function addEmail()
  {
    $email = $this->input->post('email', true);
    $firstName = $this->input->post('firstName', true);
    $lastName = $this->input->post('lastName', true);
    $check = $this->alreadySubscribed($email);

    if($check)
    {
      $data = array(
        'email' => $email,
        'firstname' => $firstName,
        'lastname' => $lastName
      );

      $this->db->insert('emailListing', $data);

      return true;
    }
    else { return false; }

  }

  public function alreadySubscribed($email)
  {
    $this->db->where('email', $email);
    $check = $this->db->get('emailListing')->num_rows();

    //Email does not exist in system
    if($check < 1)  {  return true; }

    //Email does exist in system
    else {  return false; }
  }

  function getSubs()
  {
    return $this->db->query("select * from emailListing where exported = 'n'")->result_array();
  }

  public function getAllSubs()
  {
    return $this->db->query("select * from emailListing")->result_array();
  }

  public function updateExportList()
  {
    $this->db->query("UPDATE emailListing set exported = 'y' where exported = 'n'");
    return true;
  }
}
