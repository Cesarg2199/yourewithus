<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
	public function getUsers()
	{
		if($this->session->role == "1"){
			$users = $this->db->query("select * from users where active = 'y'")->result_array();
			return $users;
		}
		elseif ($this->session->role == "7") {
			//list of all the schools the region admin is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$this->session->userId."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			//taking that list the region admin is connected to and getting all the unique id's
			$userList = $this->db->query("SELECT distinct(userId) FROM schoolaccess where schoolName in ({$schoolList})")->result_array();
			foreach($userList as $user)
			{
				@$newUserList[0] .= "'".$user['userId']."',";
			}
			$userList = rtrim($newUserList[0],",");

			$users = $this->db->query("select * from users where id in ({$userList}) and roletitle != 'Administrator' and active = 'y' ")->result_array();

			return $users;

			//Figure out new way to only get users from the schools list
		}
		elseif ($this->session->role == "4") {
			//list of all the schools the ccc is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$this->session->userId."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			//taking that list the ccc is connected to and getting all the unique id's
			$userList = $this->db->query("SELECT distinct(userId) FROM schoolaccess where schoolName in ({$schoolList})")->result_array();
			foreach($userList as $user)
			{
				@$newUserList[0] .= "'".$user['userId']."',";
			}
			$userList = rtrim($newUserList[0],",");

			$users = $this->db->query("select * from users where id in ({$userList}) and roletitle != 'Administrator' and active = 'y' ")->result_array();

			return $users;

			//Figure out new way to only get users from the schools list
		}
		else {
			$users = $this->db->query("select * from users where id = {$this->session->userId} and active = 'y'")->result_array();
			return $users;
		}
	}

	public function getDeactiveUsers()
	{
		$users = $this->db->query("select * from users where active = 'n'")->result_array();
		return $users;
	}

	public function getUser($userId)
	{
		$user = $this->db->query("select * from users where id = '".$userId."' and active = 'y' LIMIT 1")->result_array();
		return $user;
	}

	public function getParticipant($userId)
	{
		return $this->db->query("select participantId from participantaccess where userId = '".$userId."' LIMIT 1000")->result_array();
	}

	public function deactivateUser()
	{
		$userId = $this->input->post('userId');

		$check = $this->db->query("UPDATE users SET active = 'n' where id = '".$userId."'");

		if($check){ return true; }
		else{ return false; }
	}

	public function activateUser()
	{
		$userId = $this->input->post('userId');

		$check = $this->db->query("UPDATE users SET active = 'y' where id = '".$userId."'");

		if($check){ return true; }
		else{ return false; }
	}

	public function getRoleId($title)
	{
		//This needs to be done through the database
		switch ($title)
		{
		    case "Administrator":
		        return "1";
		        break;
		    case "Mentor":
		        return "2";
		        break;
	        case "Adult Companion":
	        	return "3";
		        break;
		    case "Campus Case Coordinator":
	        	return "4";
		        break;
		    case "Group":
	        	return "5";
		        break;
		    case "Parent":
	        	return "6";
		        break;
				case "Region Administrator":
	        	return "7";
		        break;
				case "Licensed Clinician":
	        	return "8";
		        break;
		}
	}

	public function insertUser()
	{
		$firstname = $this->input->post('firstname', true);
		$lastname = $this->input->post('lastname', true);
		$email = $this->input->post('email', true);
		$phone = $this->input->post('phone', true);
		$password = $this->input->post('password', true);
		$roletitle = $this->input->post('role', true);
		$participants = $this->input->post('participant');
		$schoolList = $this->input->post('schoolList');

		$securePassword = password_hash($password, PASSWORD_BCRYPT);

		$data = array(
	        'firstname' => $firstname,
	        'lastname' => $lastname,
	        'email' => $email,
					'phone' => $phone,
	        'password' => $securePassword,
	        'role' => $this->getRoleId($roletitle),
	        'roletitle' => $roletitle,
					'date' => Date('Y-m-d H:i:s'),
		);

		$this->db->insert('users', $data);
		$insertId = $this->db->insert_id();

		//Generating list of participants and adding them to the table
		foreach($participants as $participant)
		{
			$data = array(
				'participantId' => $participant,
		    'userId' => $insertId
			);

			$this->db->insert('participantaccess', $data);
		}

		//Delete all current school access
		$this->deleteAllCurrentAccess($insertId);

		//Generating list of schools and adding them to the table
		foreach($schoolList as $school)
		{
			$data = array(
				'schoolName' => $school,
		    'userId' => $insertId
			);

			$this->db->insert('schoolaccess', $data);
		}

		//Itterate back and delete any blank ones that may have gone through.
		$this->deleteBlanks();
	}

	public function updateUser()
	{
		$id = $this->input->post('id', true);
		$firstname = $this->input->post('firstname', true);
		$lastname = $this->input->post('lastname', true);
		$email = $this->input->post('email', true);
		$phone = $this->input->post('phone', true);
		$password = $this->input->post('password', true);
		$roletitle = $this->input->post('role', true);
		$participants = $this->input->post('participant');
		$schoolList = $this->input->post('schoolList');

		//check to see whether the password was changed or not.
		if($password == "")
		{
			$data = array(
	      'firstname' => $firstname,
	      'lastname' => $lastname,
	      'email' => $email,
				'phone' => $phone,
	      'role' => $this->getRoleId($roletitle),
	      'roletitle' => $roletitle,
			);

			$this->db->where('id', $id);
			$this->db->update('users', $data);

			//clearing list of current participants.
			$this->deleteAccess($id);

			//reinserting the participants along with any other new ones.
			foreach($participants as $participant)
			{
				$data = array(
					'participantId' => $participant,
			    'userId' => $id
				);

				$this->db->insert('participantaccess', $data);
			}

			//Deleting all school access
			$this->deleteAllCurrentAccess($id);

			//check to see if the school list is empty
			if($schoolList != "")
			{
				foreach($schoolList as $school)
				{
					$data = array(
						'schoolName' => $school,
				    'userId' => $id
					);

					$this->db->insert('schoolaccess', $data);
				}
			}
		}

		//Password did change
		else
		{
			$securePassword = password_hash($password, PASSWORD_BCRYPT);

			$data = array(
		        'firstname' => $firstname,
		        'lastname' => $lastname,
		        'email' => $email,
						'phone' => $phone,
		        'password' => $securePassword,
		        'role' => $this->getRoleId($roletitle),
		        'roletitle' => $roletitle
			);

			$this->db->where('id', $id);
			$this->db->update('users', $data);

			$this->deleteAccess($id);

			foreach($participants as $participant)
			{
				$data = array(
					'participantId' => $participant,
			    'userId' => $id
				);

				$this->db->insert('participantaccess', $data);
			}

			$this->deleteAllCurrentAccess($id);
			if($schoolList != "")
			{
				foreach($schoolList as $school)
				{
					$data = array(
						'schoolName' => $school,
				    'userId' => $id
					);

					$this->db->insert('schoolaccess', $data);
				}
			}
		}

		if(($roletitle == "Administrator") or ($roletitle == "Campus Case Coordinator") or ($roletitle == "Region Administrator"))
		{
			$this->deleteAccess($id);
		}

		$this->deleteBlanks();
	}

	public function deleteAllCurrentAccess($id)
	{
		//Delete all current access to schools
		$this->db->query("DELETE FROM schoolaccess where userId = {$id}");
	}

	public function deleteAccess($id)
	{
		$this->db->query("DELETE FROM participantaccess where userId = {$id}");
	}

	public function deleteBlanks()
	{
		$this->db->where('participantId', 0);
		$this->db->delete('participantaccess');

		$this->db->where('schoolName', '');
		$this->db->delete('schoolaccess');
	}

	public function listOfRelatedUsers($participantId)
	{
		$userList = $this->db->query("select userId from participantaccess where participantid = '".$participantId."'")->result_array();

		$loopCount = count($userList);

		for($i = 0; $i < $loopCount; $i++)
		{
			$list[$i] = (int)$userList[$i]['userId'];
		}

		for($u = 0; $u < $loopCount; $u++)
		{
			$users[$u] = $this->db->query("select email from users where id = '".$list[$u]."' and active = 'y' ")->result_array();
		}

		return $users;
	}

	public function userTime($user)
	{
		//Get the current week
		$day = date('w');

		if($day == "Sunday"){ $from = Date('Y-m-d'); }
		else{ $from = date("Y-m-d", strtotime('sunday last week')); }

		if($day == "Saturday"){ $to = Date('Y-m-d'); }
		else{ $to = date("Y-m-d", strtotime('saturday this week')); }

		//get the subtotal for each hour set and total hour set for interval
		$intervalTotal = $this->db->query("SELECT round(sum(totalTime), 2) as intervalTime FROM timeSheet WHERE userId = '{$user}' and date >= '{$from}' and date <= '{$to}'")->result_array()[0];
		$adultLivingTotal = $this->db->query("SELECT round(sum(totalTime), 2) as adultLiving FROM timeSheet where activity = 'Adult Living' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}'")->result_array()[0];
		$employmentTotal = $this->db->query("SELECT round(sum(totalTime), 2) as employment FROM timeSheet where activity = 'employment' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}'")->result_array()[0];
		$socialInclusionTotal = $this->db->query("SELECT round(sum(totalTime), 2) as socialInclusion FROM timeSheet where activity = 'Social Inclusion' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}'")->result_array()[0];
		$socialTotal = $this->db->query("SELECT round(sum(totalTime), 2) as social FROM timeSheet where activity = 'Social' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}'")->result_array()[0];
		$adminTotal = $this->db->query("SELECT round(sum(totalTime), 2) as admin FROM timeSheet where activity = 'Administrative' and userId = '{$user}' and date >= '{$from}' and date <= '{$to}'")->result_array()[0];

		//construct array of the results
		$results = array(
			'intervalTotal' => $intervalTotal,
			'adultLivingTotal' => $adultLivingTotal,
			'employmentTotal' => $employmentTotal,
			'socialInclusionTotal' => $socialInclusionTotal,
			'socialTotal' => $socialTotal,
			'adminTotal' => $adminTotal
		);

		//return the results
		return $results;
	}

	public function getListOfParticipantsForUser($user, $userTitle)
	{
		//if ccc grab all the participants of that school
		if($userTitle == "Campus Case Coordinator")
		{
			//list of all the schools the ccc is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$user."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			return $this->db->query("select * from participants where school in ({$schoolList})")->result_array();
		}
		elseif($userTitle == "Region Administrator")
		{
			//list of all the schools the ccc is connected to
			$listOfSchools = $this->db->query("select schoolName from schoolaccess where userId = '".$user."'")->result_array();
			foreach($listOfSchools as $school)
			{
				@$newSchoolList[0] .= "'".$school['schoolName']."',";
			}

			$schoolList = rtrim($newSchoolList[0],",");

			return $this->db->query("select * from participants where school in ({$schoolList})")->result_array();
		}
		else
		{
			$access = $this->db->query("Select participantId from participantaccess where userId = '".$user."'")->result_array();
			$listOfParticipants = array();

			if($access)
			{
				foreach ($access as $id)
				{
					$listOfParticipants[] = $id["participantId"];
				}

				$list = array();

				//Compiled Query
				$sql = "select * from participants where id in(".$listOfParticipants[0];
				for($i = 1; $i < count($listOfParticipants); $i++)
				{
					$sql .= ", ".$listOfParticipants[$i];
				}
				$sql .= ")";

				$query = $this->db->query($sql);

				$data = array();
				if($query !== FALSE && $query->num_rows() > 0) { $data = $query->result_array(); }

				return $data;
			}
		}
	}

	public function getListOfSchoolsForUser($user)
	{
		return $this->db->query("select schoolName from schoolaccess where userId = '".$user."'")->result_array();
	}

	public function getRecentDailyLogs($user)
	{
		return $this->db->query("
			SELECT dailylog.id, dailylog.participantid, DATE_FORMAT(dailylog.date, '%M %d %Y %r') as date, dailylog.activity, dailylog.notes, CONCAT(participants.Firstname, ' ', participants.Lastname) as participant
			FROM yourewithus.dailylog
			JOIN participants on participants.id = dailylog.participantid
			WHERE dailylog.userid = '".$user."'
			ORDER BY dailylog.date DESC
			LIMIT 5
		")->result_array();
	}

	public function getRecentIA($user)
	{
		$values = $this->db->query("
		SELECT inclusion.*, CONCAT(participants.Firstname, ' ', participants.Lastname) as participant
		FROM inclusion
		JOIN participants on participants.id = inclusion.participantid
		WHERE inclusion.userid = '".$user."'
		ORDER BY inclusion.date DESC
		LIMIT 5
		")->result_array();

		//Calculating the sub rows to display as an overall score
		$calculated = array();
		for($i = 0; $i < count($values); $i++)
		{
			$calculated[$i]['id'] = $values[$i]['id'];
			$calculated[$i]['date'] = $values[$i]['date'];
			$calculated[$i]['participant'] = $values[$i]['participant'];
			$calculated[$i]['pq'] = (int)$values[$i]['pq1'] + (int)$values[$i]['pq2'] + (int)$values[$i]['pq3'] + (int)$values[$i]['pq4'] + (int)$values[$i]['pq5'];
			$calculated[$i]['sq'] = (int)$values[$i]['sq1'] + (int)$values[$i]['sq2'] + (int)$values[$i]['sq3'] + (int)$values[$i]['sq4'];
			$calculated[$i]['aq'] = (int)$values[$i]['aq1'] + (int)$values[$i]['aq2'] + (int)$values[$i]['aq3'] + (int)$values[$i]['aq4'];
		}

		return $calculated;
	}

	public function completeOnboarding()
	{

		$password = $this->input->post('password');
		$cpassword = $this->input->post('cpassword');
		$skip = $this->input->post('skipPassword');

		if(!$skip)
		{
			//change password
			if($password == $cpassword)
	    {
	      if(isset($password))
	      {
	        $securePassword = password_hash($password, PASSWORD_BCRYPT);

	        $data = array(
	            'password' => $securePassword
	        );
	      }
	    }

	   	$this->db->where('id', $this->session->userId);
	    $this->db->update('users', $data);
		}

		$data = array(
			'firstLogin' => 'n',
      'onboarding' => Date('Y-m-d H:i:s')
		);

		$this->db->where('id', $this->session->userId);
		$this->db->update('users', $data);
	}

	public function checkDailyLog()
	{
		$userId = $this->input->post('userId');
		$today = date('Y-m-d', strtotime("-1 day"));

		$check = $this->db->query("SELECT * from dailylog where userid = '{$userId}' and created_date >= '{$today}'")->num_rows();
	
		if($check == 0)
		{
			return false;
		}
		else {
			return true;
		}
	}
}
