<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
	public function login()
	{
		$email = $this->input->post('email', true);
		$password = $this->input->post('password', true);

		if(empty($email) OR empty($password)){ return false; }

		$this->db->select('*');
		$this->db->where('email', $email);
		$this->db->where('active', 'y');
		$query = $this->db->get('users')->row();

		if($query === NULL){ return false; }

		//Test password against hash in db
		$testPassword = password_verify($password, $query->password);

		if($testPassword)
		{
			$check = $this->db->query("select * from users where email = '".$email."' and active = 'y'");
			$results = $check->result_array()[0];

			$this->session->set_userdata('loggedin', "true");
			$this->session->set_userdata('firstname', $results['firstname']);
			$this->session->set_userdata('lastname', $results['lastname']);
			$this->session->set_userdata('userId', $results['id']);
			$this->session->set_userdata('role', $results['role']);
			$this->session->set_userdata('roletitle', $results['roletitle']);
			$this->session->set_userdata('firstLogin', $results['firstLogin']);
			return true;
		}
		else
		{
			return false;
		}
	}
}
