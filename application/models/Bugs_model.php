<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/New_York');
class Bugs_model extends CI_Model
{
  public function reportBug($description)
  {
    $data = array(
        'userId' => $this->session->userId,
        'userName' => ucfirst($this->session->firstname).' '.ucfirst($this->session->lastname),
        'date' => date('Y-m-d H:i:s'),
        'description' => $description
    );

    $this->db->insert('bugs', $data);
  }
}
