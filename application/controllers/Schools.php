<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends CI_Controller
{
	function __construct()
    {
        // Call the Model constructor
      parent::__construct();
      if(!$this->session->loggedin){ redirect('Login/index'); }
      $this->load->model('Schools_model');
    }

	public function index()
	{
        $data['schools'] = $this->Schools_model->getAllSchools();
		$this->load->view('includes/header');
		$this->load->view('webapp/Schools/schoolsList', $data);
		$this->load->view('includes/footer');
	}

  public function addSchool()
  {
      $this->Schools_model->addSchool();
  }

  public function editSchool()
  {
    $this->Schools_model->editSchool();
  }

	public function quickAddSchool()
	{
		$this->Schools_model->addSchool();

		echo json_encode($this->Schools_model->getAllSchools());
	}
}
