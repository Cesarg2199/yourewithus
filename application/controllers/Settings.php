<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{
	function __construct()
  {
      // Call the Model constructor
      parent::__construct();
			if(!$this->session->loggedin){ redirect('Login/index'); }
      $this->load->model('Settings_model');
  }

  public function index()
  {
    //$this->load->view('includes/header'."{$this->session->role}".'');  proof of concept
		$this->load->view('includes/header');
  	$this->load->view('webapp/Settings/settings');
    $this->load->view('includes/footer');
  }

	public function password()
	{
		$this->load->view('includes/header');
  	$this->load->view('webapp/Settings/password');
    $this->load->view('includes/footer');
	}

	public function email()
	{
		$this->load->view('includes/header');
  	$this->load->view('webapp/Settings/email');
    $this->load->view('includes/footer');
	}

  public function changeEmail()
  {
    $this->Settings_model->changeEmail();
    $this->index();
  }

  public function changePassword()
  {
    $this->Settings_model->changePassword();
    $this->index();
  }
}
