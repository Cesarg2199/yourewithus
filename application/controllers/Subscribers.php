<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribers extends CI_Controller
{
	function __construct()
  {
      // Call the Model constructor
      parent::__construct();
			if(!$this->session->loggedin){ redirect('Login/index'); }
      $this->load->model('Subscribe_model');
  }

  function index()
  {
		$newSubs = $this->Subscribe_model->getSubs();
		$subs = $this->Subscribe_model->getAllSubs();
    $data['newSubs'] = $newSubs;
		$data['subs'] = $subs;
		$data['subsList'] = count($subs);
		$data['newSubsList'] = count($newSubs);
		$this->load->view('includes/header');
		$this->load->view('webapp/Subscribers/subscribers', $data);
		$this->load->view('includes/footer');
  }

	function exportSubs()
	{
		//load our new PHPExcel library
		$this->load->library('excel');

		//activate worksheet number 1»
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Email Subscribers');

		foreach(range('A','C') as $columnID)
		{
		    $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'First Name');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Last Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Email');

		//Get list of emails
		$subs = $this->Subscribe_model->getSubs();
		print_r($subs);

		//Write emails to excel sheet
		$cellCount = 2;
		foreach($subs as $sub)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$cellCount.'', $sub['firstname']);
			$this->excel->getActiveSheet()->setCellValue('B'.$cellCount.'', $sub['lastname']);
			$this->excel->getActiveSheet()->setCellValue('C'.$cellCount.'', $sub['email']);
			$cellCount++;
		}

		//After data has been exported set db values to exported
		$this->Subscribe_model->updateExportList();

		$filename='email_subs.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function exportAllSubs()
	{
		//load our new PHPExcel library
		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Email Subscribers');

		foreach(range('A','C') as $columnID)
		{
		    $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'First Name');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Last Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Email');

		//Get list of emails
		$subs = $this->Subscribe_model->getAllSubs();

		//Write emails to excel sheet
		$cellCount = 2;
		foreach($subs as $sub)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$cellCount.'', $sub['firstname']);
			$this->excel->getActiveSheet()->setCellValue('B'.$cellCount.'', $sub['lastname']);
			$this->excel->getActiveSheet()->setCellValue('C'.$cellCount.'', $sub['email']);
			$cellCount++;
		}

		//After data has been exported set db values to exported
		$this->Subscribe_model->updateExportList();

		$filename='all_email_subs.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}
