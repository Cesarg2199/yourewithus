<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onboarding extends CI_Controller
{
	function __construct()
  {
      // Call the Model constructor
      parent::__construct();
			if(!$this->session->loggedin){ redirect('Login/index'); }
      $this->load->model('Users_model');
  }

  public function index()
  {
		$this->load->view('webapp/Onboarding/onboarding');
  }

  public function completeOnboarding()
  {
    $this->Users_model->completeOnboarding();
    Redirect('Home');
  }
}
