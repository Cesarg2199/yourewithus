<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller
{
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		if(!$this->session->loggedin){ redirect('Login/index'); }
		$this->load->helper('directory');
	}

	public function index()
	{
		$files = directory_map('assets/documents/best', 1);

		$list = array();
		foreach($files as $file)
		{
			$list[] = pathinfo($file)["basename"];
		}

		$data['amount'] = $files;
		$data['file'] = $list;
		$this->load->view('includes/header');
		$this->load->view('webapp/Support/individualSupport', $data);
		$this->load->view('includes/footer');
	}

	public function transitions()
	{
		$files = directory_map('assets/documents/transition', 1);

		$list = array();
		foreach($files as $file)
		{
			$list[] = pathinfo($file)["basename"];
		}

		$data['amount'] = $files;
		$data['file'] = $list;
		$this->load->view('includes/header');
		$this->load->view('webapp/Support/transitions', $data);
		$this->load->view('includes/footer');
	}

	public function resources()
	{
		$files = directory_map('assets/documents/resources', 1);

		$list = array();
		foreach($files as $file)
		{
			$list[] = pathinfo($file)["basename"];
		}

		$data['amount'] = $files;
		$data['file'] = $list;
		$this->load->view('includes/header');
		$this->load->view('webapp/Support/employeeResources', $data);
		$this->load->view('includes/footer');
	}

	public function mentorThoughts()
	{
		$this->load->view('includes/header');
		$this->load->view('webapp/Support/mentorThoughts');
		$this->load->view('includes/footer');
	}

	public function orientation()
	{
		$this->load->view('includes/header');
		$this->load->view('webapp/Support/orientation');
		$this->load->view('includes/footer');
	}

	public function theWhy()
	{
		$this->load->view('includes/header');
		$this->load->view('webapp/Support/theWhy');
		$this->load->view('includes/footer');
	}
}
