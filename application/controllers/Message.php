<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller
{
	function __construct()
    {
			// Call the Model constructor
      parent::__construct();
			if(!$this->session->loggedin){ redirect('Login/index'); }
			$this->load->model('Participants_model');
			$this->load->model('Users_model');
			$this->load->model('Message_model');
			$this->load->model('Email_model');
    }

	public function index()
	{
		@$data->participants = $this->Participants_model->getUserParticipants();
		$this->load->view('includes/header');
		$this->load->view('webapp/Message/messageBoard', $data);
		$this->load->view('includes/footer');
	}

	public function loadPosts()
	{
		$data = $this->Message_model->loadPosts();
		echo json_encode($data);
		return true;
	}

	public function addPost()
	{
		//Grab all the post variables
    $participantId = $this->input->post('participantId');
    $message = $this->input->post("message");

		//Grab the related values
		$listOfRelatedUsers = $this->Users_model->listOfRelatedUsers($participantId);
		$participantName = $this->Participants_model->getParticipant($participantId);

		//Add post to the message board
		$this->Message_model->addPost($participantId, $message);

		//Experiment with sending emails at this point here from an email modal
		$this->Email_model->emailGroup($participantName, $message, $listOfRelatedUsers);

		return true;
	}

	public function deletePost()
	{
		$this->Message_model->deletePost();
		return true;
	}
}
