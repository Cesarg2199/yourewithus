<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
	function __construct()
  {
		// Call the Model constructor
    parent::__construct();
		if(!$this->session->loggedin){ redirect('Login/index'); }

		$this->load->model('Users_model');
		$this->load->model('Participants_model');
		$this->load->model('Schools_model');
  }

	public function index()
	{
		@$data['users'] = $this->Users_model->getUsers();
		$this->load->view('includes/header');
		$this->load->view('webapp/Users/users', $data);
		$this->load->view('includes/footer');
	}

	public function deactiveUsers()
	{
		@$data['users'] = $this->Users_model->getDeactiveUsers();
		$this->load->view('includes/header');
		$this->load->view('webapp/Users/deactiveUsers', $data);
		$this->load->view('includes/footer');
	}

	public function addUser()
	{
		@$data['participants'] = $this->Participants_model->getParticipants();
		@$data['entireSchoolList'] = $this->Schools_model->getAllSchools();
		$this->load->view('includes/header');
		$this->load->view('webapp/Users/addUser', $data);
		$this->load->view('includes/footer');
	}

	public function editUser()
	{
		$user = $this->input->get('userId');
		$participantId = $this->Users_model->getParticipant($user);

		$listOfAccess = array();

		foreach($participantId as $participants)
		{
			foreach($participants as $participant)
			{
				$listOfAccess[] = $this->Participants_model->getParticipant($participant);
			}
		}

		@$data['list'] = $listOfAccess;
		@$data['user'] = $this->Users_model->getUser($user);
		@$data['participants'] = $this->Participants_model->getParticipants();
		$data['linkedSchools'] = $this->Schools_model->getAllSchoolsLinkedUser($user);
		$data['entireSchoolList'] = $this->Schools_model->getAllSchools();
		$this->load->view('includes/header');
		$this->load->view('webapp/Users/editUser', $data);
		$this->load->view('includes/footer');
	}

	public function userProfile()
	{
		$user = $this->input->get('userId');
		$userInfo = $this->Users_model->getUser($user)[0];

		@$data['schools'] = $this->Users_model->getListOfSchoolsForUser($user);
		@$data['user'] = $userInfo;
		@$data['currentWeek'] = $this->Users_model->userTime($user);
		@$data['participants'] = $this->Users_model->getListOfParticipantsForUser($user, $userInfo[0]->roletitle);
		@$data['dailyLogs'] = $this->Users_model->getRecentDailyLogs($user);
		@$data['IA'] = $this->Users_model->getRecentIA($user);
		$this->load->view('includes/header');
		$this->load->view('webapp/Users/userProfile', $data);
		$this->load->view('includes/footer');
	}

	public function deactivateUser()
	{
		$check = $this->Users_model->deactivateUser();
		if($check){ return true; }
	}

	public function activateUser()
	{
		$check = $this->Users_model->activateUser();
		if($check){ return true; }
	}

	public function insertUser()
	{
		$this->Users_model->insertUser();
		Redirect('Users/index');
	}

	public function updateUser()
	{
		$this->Users_model->updateUser();
		Redirect('Users/index');
	}

	public function checkDailyLog()
	{
		$data = $this->Users_model->checkDailyLog();
		echo json_encode($data);
	}
}
