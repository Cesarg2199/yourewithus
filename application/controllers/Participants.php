<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants extends CI_Controller
{
	function __construct()
  {
		// Call the Model constructor
    parent::__construct();
		if(!$this->session->loggedin){ redirect('Login/index'); }

    $this->load->model('Participants_model');
		$this->load->model('Schools_model');
  }

	public function index()
	{
		@$data['participants'] = $this->Participants_model->getUserParticipants();
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/participants', $data);
		$this->load->view('includes/footer');
	}

	public function addParticipant()
	{
		@$data['schools'] = $this->Schools_model->getAllSchools();
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/addParticipant', $data);
		$this->load->view('includes/footer');
	}

	public function editParticipant()
	{
		$participant = $this->input->get('participantId');
		@$data['participant'] = $this->Participants_model->getParticipant($participant);
		@$data['schools'] = $this->Schools_model->getAllSchools();
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/editParticipant', $data);
		$this->load->view('includes/footer');
	}

	public function intakeForm()
	{
		$participantId = $this->input->get('participantId');
		@$data['intake'] = $this->Participants_model->getIntakeForm($participantId)[0];
		$data['participantId'] = $participantId;
		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId)[0];
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/intakeForm', $data);
		$this->load->view('includes/footer');
	}

	public function scoutingReport()
	{
		$participantId = $this->input->get('participantId');
		@$data['sr'] = $this->Participants_model->getScoutingReport($participantId)[0];
		$data['participantId'] = $participantId;
		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId)[0];
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/scoutingReport', $data);
		$this->load->view('includes/footer');
	}

	public function positiveExperienceDesign()
	{
		$participantId = $this->input->get('participantId');
		@$data['ped'] = $this->Participants_model->getPED($participantId)[0];
		$data['participantId'] = $participantId;
		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId)[0];
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/positiveExperienceDesign', $data);
		$this->load->view('includes/footer');
	}

	public function participantsProfile()
	{
		$participantId = $this->input->get('participantId');

		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId);
		$data['linkedUsers'] = $this->Participants_model->linkedUsers($participantId);
		$data['dailyLog'] = $this->Participants_model->participantsDailyLog($participantId);
		$data['IA'] = $this->Participants_model->participantsRecentIA($participantId);
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/participantsProfile', $data);
		$this->load->view('includes/footer');
	}

	//File Storage functions
	function fileStore()
	{
		$participantId = $this->input->get('participantId');

		//file path for participant
		$path = "participantFiles/".$participantId;

		//Check to see if a folder for them exist and if not create one first.
		$this->checkFolder($path);

		$files = directory_map($path, 1); //Gets array of files in the filestorage folder
		$allowed = array(".pdf", ".png", ".jpg", "jpeg", ".css", ".PNG"); //List of file types that are allowed to be previewed
		$count = 1;

		//Get Participant Info
		$participant = $this->Participants_model->getParticipant($participantId)[0];

		$data['allowed'] = $allowed;
		$data['count'] = $count;
		$data['files'] = $files;
		$data['amount'] = count($files);
		$data['participant'] = $participant;
		$this->load->view('includes/header');
		$this->load->view('webapp/Participants/fileStore', $data);
		$this->load->view('includes/footer');
	}

	public function saveFile()
	{
		$participantId = $this->input->post('participantId');

		$config['upload_path'] = './participantFiles/'.$participantId.'/';
		$config['allowed_types'] = '*';
		$config['remove_spaces'] = false;


    $this->load->library('upload', $config);


    if ( ! htmlentities($this->upload->do_upload('file')))
    {
      $error = array('error' => $this->upload->display_errors());
			print_r($error);
    }
    else
    {
      $data = array('upload_data' => $this->upload->data());
    }
	}

	public function deleteFile()
	{
		$name = $this->input->post('name');
		$participantId = $this->input->post('participantId');
		$file = "participantFiles/".$participantId."/".$name;

		if(unlink($file)){ return true; }
		else{ return false; }
	}

	public function deleteParticipant()
	{
		//$check = $this->Participants_model->deleteParticipant();
		return true; 
	}

	public function insertParticipant()
	{
		$this->Participants_model->insertParticipant();
		Redirect('Participants/index');
	}

	public function updateParticipant()
	{
		$this->Participants_model->updateParticipant();
		Redirect('Participants/index');
	}

	public function saveIntakeForm()
	{
		$this->Participants_model->saveIntakeForm();
		Redirect('Participants/index');
	}

	public function saveScoutingReport()
	{
		$this->Participants_model->saveScoutingReport();
		Redirect('Participants/index');
	}

	public function savePositiveExperienceDesign()
	{
		$this->Participants_model->savePositiveExperienceDesign();
		Redirect('Participants/index');
	}

	//double check this works as a private function 
	private function checkFolder($path)
	{
		return is_dir($path) || mkdir($path);
	}

}
