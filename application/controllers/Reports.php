<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller
{
	function __construct()
  {
    // Call the Model constructor
    parent::__construct();
		if(!$this->session->loggedin){ redirect('Login/index'); }

    $this->load->model('Users_model');
		$this->load->model('Participants_model');
		$this->load->model('Logs_model');
		$this->load->model('Reports_model');
  }

	public function dailyLogEntreies()
	{
		//Get the current day
		$today = Date('Y-m-d');

		//Get the current week
		$day = date('w');

		if($day == "Sunday"){ $thisWeekFrom = Date('Y-m-d'); }
		else{ $thisWeekFrom = date("Y-m-d", strtotime('sunday last week')); }

		if($day == "Saturday"){ $thisWeekTo = Date('Y-m-d'); }
		else{ $thisWeekTo = date("Y-m-d", strtotime('saturday this week')); }

		//Get the dates of the month
		$thisMonthFrom = date("Y-m-d", strtotime('first day of this month'));
		$thisMonthTo = date("Y-m-d", strtotime('last day of this month'));

		$data['today'] = $today;
		$data['thisWeekFrom'] = $thisWeekFrom;
		$data['thisWeekTo'] = $thisWeekTo;
		$data['thisMonthFrom'] = $thisMonthFrom;
		$data['thisMonthTo'] = $thisMonthTo;
		$data['participants'] = $this->Participants_model->getUserParticipants();
		$data['users'] = $this->Users_model->getUsers();
		$this->load->view('includes/header');
		$this->load->view('webapp/Reports/dailyLogEntry', $data);
		$this->load->view('includes/footer');
	}

	public function inclusionAssessmentReport()
	{
		//Get the current day
		$today = Date('Y-m-d');

		//Get the current week
		$day = date('w');

		if($day == "Sunday"){ $thisWeekFrom = Date('Y-m-d'); }
		else{ $thisWeekFrom = date("Y-m-d", strtotime('sunday last week')); }

		if($day == "Saturday"){ $thisWeekTo = Date('Y-m-d'); }
		else{ $thisWeekTo = date("Y-m-d", strtotime('saturday this week')); }

		//Get the dates of the month
		$thisMonthFrom = date("Y-m-d", strtotime('first day of this month'));
		$thisMonthTo = date("Y-m-d", strtotime('last day of this month'));

		$data['today'] = $today;
		$data['thisWeekFrom'] = $thisWeekFrom;
		$data['thisWeekTo'] = $thisWeekTo;
		$data['thisMonthFrom'] = $thisMonthFrom;
		$data['thisMonthTo'] = $thisMonthTo;
		$data['participants'] = $this->Participants_model->getUserParticipants();
		$this->load->view('includes/header');
		$this->load->view('webapp/Reports/inclusionAssesmentEntry', $data);
		$this->load->view('includes/footer');
	}

	public function timeSheetReport()
	{
		//Get the current day
		$today = Date('Y-m-d');

		//Get the current week
		$day = date('w');

		if($day == "Sunday"){ $thisWeekFrom = Date('Y-m-d'); }
		else{ $thisWeekFrom = date("Y-m-d", strtotime('sunday last week')); }

		if($day == "Saturday"){ $thisWeekTo = Date('Y-m-d'); }
		else{ $thisWeekTo = date("Y-m-d", strtotime('saturday this week')); }

		//Get the dates of the month
		$thisMonthFrom = date("Y-m-d", strtotime('first day of this month'));
		$thisMonthTo = date("Y-m-d", strtotime('last day of this month'));

		$data['today'] = $today;
		$data['thisWeekFrom'] = $thisWeekFrom;
		$data['thisWeekTo'] = $thisWeekTo;
		$data['thisMonthFrom'] = $thisMonthFrom;
		$data['thisMonthTo'] = $thisMonthTo;
		$data['users'] = $this->Users_model->getUsers();
		$this->load->view('includes/header');
		$this->load->view('webapp/Reports/timeSheetEntry', $data);
		$this->load->view('includes/footer');
	}

	public function expenseSheetReport()
	{
		//Get the current day
		$today = Date('Y-m-d');

		//Get the current week
		$day = date('w');

		if($day == "Sunday"){ $thisWeekFrom = Date('Y-m-d'); }
		else{ $thisWeekFrom = date("Y-m-d", strtotime('sunday last week')); }

		if($day == "Saturday"){ $thisWeekTo = Date('Y-m-d'); }
		else{ $thisWeekTo = date("Y-m-d", strtotime('saturday this week')); }

		//Get the dates of the month
		$thisMonthFrom = date("Y-m-d", strtotime('first day of this month'));
		$thisMonthTo = date("Y-m-d", strtotime('last day of this month'));

		$data['today'] = $today;
		$data['thisWeekFrom'] = $thisWeekFrom;
		$data['thisWeekTo'] = $thisWeekTo;
		$data['thisMonthFrom'] = $thisMonthFrom;
		$data['thisMonthTo'] = $thisMonthTo;
		$data['users'] = $this->Users_model->getUsers();
		if($this->session->role == "1"){ $data['recent'] = $this->Reports_model->expenseSheetRecent(); }
		$this->load->view('includes/header');
		$this->load->view('webapp/Reports/expenseSheetEntry', $data);
		$this->load->view('includes/footer');
	}

	public function userTimeSheet()
	{
		$data = $this->Reports_model->userTimeSheet();
		echo json_encode($data);
		return true;
	}

	public function userTimeSheetHeadings()
	{
		$data = $this->Reports_model->userTimeSheetHeadings();
		echo json_encode($data);
		return true;
	}

	public function userReport()
	{
		$data = $this->Reports_model->userReport();
		echo json_encode($data);
		return true;
	}

	public function inclusionReport()
	{
		$data = $this->Reports_model->inclusionReportMin();
		echo json_encode($data);
		return true;
	}

	public function inclusionReportFull()
	{
		$data = $this->Reports_model->inclusionReportFull();
		echo json_encode($data);
		return true;
	}

	public function userExpenseSheet()
	{
		$data = $this->Reports_model->userExpenseSheet();
		echo json_encode($data);
		return true;
	}

	function exportDailyReport()
	{
		$participant = $this->input->get('participant');
    $from = $this->input->get('dFrom');
    $to = $this->input->get('dTo');

		//Get list
		$list = $this->Reports_model->exportDailyReportFull($participant, $from, $to);

		//load our new PHPExcel library
		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Daily Log');

		foreach(range('A','E') as $columnID)
		{
		    $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//set cells content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'ID');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Date');
		$this->excel->getActiveSheet()->setCellValue('C1', 'User');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Activity');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Notes');

		//external count
		$count = 2;

		//Write data to excel sheet
		for($i = 0; $i < count($list); $i++)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$count.'', "".$list[$i]['id']."");
			$this->excel->getActiveSheet()->setCellValue('B'.$count.'', "".$list[$i]['date']."");
			$this->excel->getActiveSheet()->setCellValue('C'.$count.'', "".$list[$i]['user']."");
			$this->excel->getActiveSheet()->setCellValue('D'.$count.'', "".$list[$i]['activity']."");
			$this->excel->getActiveSheet()->setCellValue('E'.$count.'', "".$list[$i]['notes']."");
			$count++;
		}

		$date = date('Y-m-d');
		$nameid = $this->Participants_model->getParticipant($participant)[0];
		$name = $nameid['firstname'].' '.$nameid['lastname'];

		$filename='dailylog_'.$date.'_'.$name.'.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function exportInclusionReport()
	{
		$participant = $this->input->get('participant');
    $from = $this->input->get('dFrom');
    $to = $this->input->get('dTo');

		//Get list
		$list = $this->Reports_model->exportInclusionReportFull($participant, $from, $to);

		//load our new PHPExcel library
		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Inclusion Assessment');

		foreach(range('A','P') as $columnID)
		{
		    $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//set cells content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'ID');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Date');
		$this->excel->getActiveSheet()->setCellValue('C1', 'User');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Physical Presence 1');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Physical Presence 2');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Physical Presence 3');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Physical Presence 4');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Physical Presence 5');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Social & Community 1');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Social & Community 2');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Social & Community 3');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Social & Community 4');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Administrative 1');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Administrative 2');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Administrative 3');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Administrative 4');

		//external count
		$count = 2;

		//Write data to excel sheet
		for($i = 0; $i < count($list); $i++)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$count.'', "".$list[$i]['id']."");
			$this->excel->getActiveSheet()->setCellValue('B'.$count.'', "".$list[$i]['date']."");
			$this->excel->getActiveSheet()->setCellValue('C'.$count.'', "".$list[$i]['user']."");
			$this->excel->getActiveSheet()->setCellValue('D'.$count.'', "".$list[$i]['pq1']."");
			$this->excel->getActiveSheet()->setCellValue('E'.$count.'', "".$list[$i]['pq2']."");
			$this->excel->getActiveSheet()->setCellValue('F'.$count.'', "".$list[$i]['pq3']."");
			$this->excel->getActiveSheet()->setCellValue('G'.$count.'', "".$list[$i]['pq4']."");
			$this->excel->getActiveSheet()->setCellValue('H'.$count.'', "".$list[$i]['pq5']."");
			$this->excel->getActiveSheet()->setCellValue('I'.$count.'', "".$list[$i]['sq1']."");
			$this->excel->getActiveSheet()->setCellValue('J'.$count.'', "".$list[$i]['sq2']."");
			$this->excel->getActiveSheet()->setCellValue('K'.$count.'', "".$list[$i]['sq3']."");
			$this->excel->getActiveSheet()->setCellValue('L'.$count.'', "".$list[$i]['sq4']."");
			$this->excel->getActiveSheet()->setCellValue('M'.$count.'', "".$list[$i]['aq1']."");
			$this->excel->getActiveSheet()->setCellValue('N'.$count.'', "".$list[$i]['aq2']."");
			$this->excel->getActiveSheet()->setCellValue('O'.$count.'', "".$list[$i]['aq3']."");
			$this->excel->getActiveSheet()->setCellValue('P'.$count.'', "".$list[$i]['aq4']."");
			$count++;
		}

		$date = date('Y-m-d');
		$nameid = $this->Participants_model->getParticipant($participant)[0];
		$name = $nameid['firstname'].' '.$nameid['lastname'];

		$filename='inclusion_assessment_'.$date.'_'.$name.'.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function exportTimeSheet()
	{
		$user = $this->input->get('user');
    $from = $this->input->get('dFrom');
    $to = $this->input->get('dTo');

		$nameid = $this->Users_model->getUser($user);
		$name = $nameid[0]['firstname'].' '.$nameid[0]['lastname'];

		//Get list
		$list = $this->Reports_model->exportTimeSheet($user, $from, $to);

		//load our new PHPExcel library
		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Time Sheet');

		foreach(range('A','F') as $columnID)
		{
		  $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//set first cell to show user name
		$this->excel->getActiveSheet()->setCellValue('A1', 'User:');
		$this->excel->getActiveSheet()->setCellValue('B1', $name);

		//set cells content with some text
		$this->excel->getActiveSheet()->setCellValue('A3', 'Date');
		$this->excel->getActiveSheet()->setCellValue('B3', 'Participant');
		$this->excel->getActiveSheet()->setCellValue('C3', 'Activity');
		$this->excel->getActiveSheet()->setCellValue('D3', 'Time In');
		$this->excel->getActiveSheet()->setCellValue('E3', 'Time Out');
		$this->excel->getActiveSheet()->setCellValue('F3', 'Total Time (Hours)');

		//external count
		$count = 4;

		//Write data to excel sheet
		for($i = 0; $i < count($list); $i++)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$count.'', "".$list[$i]['date']."");
			$this->excel->getActiveSheet()->setCellValue('B'.$count.'', "".$list[$i]['participant']."");
			$this->excel->getActiveSheet()->setCellValue('C'.$count.'', "".$list[$i]['activity']."");
			$this->excel->getActiveSheet()->setCellValue('D'.$count.'', "".$list[$i]['timeIn']."");
			$this->excel->getActiveSheet()->setCellValue('E'.$count.'', "".$list[$i]['timeOut']."");
			$this->excel->getActiveSheet()->setCellValue('F'.$count.'', "".$list[$i]['totalTime']."");
			$count++;
		}

		//Current date of export for title
		$date = date('Y-m-d');

		$filename='timesheet_'.$date.'_'.$name.'.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function exportExpenseSheet()
	{
		$user = $this->input->get('user');
    $from = $this->input->get('dFrom');
    $to = $this->input->get('dTo');

		$nameid = $this->Users_model->getUser($user);
		$name = $nameid[0]['firstname'].' '.$nameid[0]['lastname'];

		//Get list
		$list = $this->Reports_model->exportExpenseSheet($user, $from, $to);

		//load our new PHPExcel library
		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Expense Sheet');

		foreach(range('A','F') as $columnID)
		{
		  $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//set first cell to show user name
		$this->excel->getActiveSheet()->setCellValue('A1', 'User:');
		$this->excel->getActiveSheet()->setCellValue('B1', $name);

		//set cells content with some text
		$this->excel->getActiveSheet()->setCellValue('A3', 'Date');
		$this->excel->getActiveSheet()->setCellValue('B3', 'Item');
		$this->excel->getActiveSheet()->setCellValue('C3', 'Amount');
		$this->excel->getActiveSheet()->setCellValue('D3', 'Receipt');
		$this->excel->getActiveSheet()->setCellValue('E3', 'Notes');

		//external count
		$count = 4;

		//Write data to excel sheet
		for($i = 0; $i < count($list); $i++)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$count.'', "".$list[$i]['date']."");
			$this->excel->getActiveSheet()->setCellValue('B'.$count.'', "".$list[$i]['item']."");
			$this->excel->getActiveSheet()->setCellValue('C'.$count.'', "".$list[$i]['amount']."");
			$this->excel->getActiveSheet()->setCellValue('D'.$count.'', "".$list[$i]['receipt']."");
			$this->excel->getActiveSheet()->setCellValue('E'.$count.'', "".$list[$i]['notes']."");
			$count++;
		}

		//Current date of export for title
		$date = date('Y-m-d');

		$filename='timesheet_'.$date.'_'.$name.'.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}
