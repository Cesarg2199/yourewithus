<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bugs extends CI_Controller
{
	function __construct()
  {
      // Call the Model constructor
      parent::__construct();
			if(!$this->session->loggedin){ redirect('Login/index'); }
      $this->load->model('Bugs_model');
			$this->load->model('Email_model');
  }

  public function index()
  {
  	$this->load->view('includes/header');
		$this->load->view('webapp/Bugs/bugs');
		$this->load->view('includes/footer');
  }

	public function bugs()
  {
		$description = $this->input->post('description');
    $this->Bugs_model->reportBug($description);
		$this->Email_model->emailBug($description);
    $this->index();
  }
}
