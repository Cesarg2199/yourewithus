<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

	function __construct()
    {
			// Call the Model constructor
      parent::__construct();
			if(!$this->session->loggedin){ redirect('Login/index'); }

      $this->load->model('Calendar_model');
      $this->load->model('Users_model');
			$this->load->model('Participants_model');
			$this->load->model('Email_model');
    }

	/*Home page Calendar view  */
	Public function home()
	{
		$data['participants'] = $this->Participants_model->getUserParticipants();
		$data['users'] = $this->Users_model->getUsers();
		$this->load->view('includes/header');
		$this->load->view('webapp/Calendar/schedule', $data);
		$this->load->view('includes/footer');
	}

	/*Get all Events */
	Public function getEvents()
	{
		$result = $this->Calendar_model->getEvents();
		echo json_encode($result);
	}
	/*Add new event */
	Public function addEvent()
	{
		$userId = $this->input->post('support');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$start = $this->input->post('startDate');
		$end = $this->input->post('endDate');
		$participantid = $this->input->post('participantid');

		//Check to see if there was a support person.
		if($userId != 0)
		{
			//Getting support person information
			$userInfo = $this->Users_model->getUser($userId)[0];
			$userName = $userInfo['firstname'].' '.$userInfo['lastname'];
			$userEmail = $userInfo['email'];

			//Getting participant information for email
			$participantInfo = $this->Participants_model->getParticipant($participantid);
			$participantName = $participantInfo['firstname'].' '.$participantInfo['lastname'];

			//emailing support person
			$this->Email_model->emailSupport($userName, $userEmail, $participantName, $title, $description, $start, $end, "New");
		}

		//Adding event to the calendar
		$this->Calendar_model->addEvent($userId, $userName, $title, $description, $start, $end, $participantid);
	}

	/*Update Event */
	Public function updateEvent()
	{
		//Getting support person information
		$userId = $this->input->post('support');

		//Collecting info to update the event
		$id = $this->input->get('id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$start = $this->input->post('startDate');
		$end = $this->input->post('endDate');
		$participantid = $this->input->post('participantid');

		//Check to see if there was a support person.
		if($userId != 0)
		{
			//Getting support person information
			$userInfo = $this->Users_model->getUser($userId)[0];
			$userName = $userInfo['firstname'].' '.$userInfo['lastname'];
			$userEmail = $userInfo['email'];

			//Getting participant information for email
			$participantInfo = $this->Participants_model->getParticipant($participantid)[0];
			$participantName = $participantInfo['firstname'].' '.$participantInfo['lastname'];

			//emailing support person
			$this->Email_model->emailSupport($userName, $userEmail, $participantName, $title, $description, $start, $end, "Updated");
		}

		//Updating the event in the database
		$this->Calendar_model->updateEvent($userId, $userName, $id, $title, $description, $start, $end, $participantid);
	}

	/*Delete Event*/
	Public function deleteEvent()
	{
		$result=$this->Calendar_model->deleteEvent();
	}
}
