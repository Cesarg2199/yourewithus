<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	function __construct()
  {
      // Call the Model constructor
      parent::__construct();
      $this->load->model('Login_model');
  }

  public function index()
  {
  	$this->load->view('webapp/Login/login');
  }

	public function login()
	{
		$check = $this->Login_model->login();
		if($check)
		{
			if($this->session->firstLogin == 'n'){ Redirect('Home');	}
			else{
				Redirect('Onboarding');
			}
		}
		else {
				Redirect('Login/index?attempt=fail');
		}

	}

	public function logout()
	{
		session_destroy();
		Redirect('Login/index');
	}

	public function releaseNotes()
	{
		$this->load->view('webapp/Login/release');
	}
}
