<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
  {
      // Call the Model constructor
      parent::__construct();
			$this->load->model('Users_model');
  }

  public function index()
  {
		@$data->currentWeek = $this->Users_model->userTime($this->session->userId);
		if($this->session->roletitle == "Campus Case Coordinator"){ @$data->schools =  $this->Users_model->getListOfSchoolsForUser($this->session->userId); }
		if($this->session->roletitle == "Region Administrator"){ @$data->schools =  $this->Users_model->getListOfSchoolsForUser($this->session->userId); }
		@$data->participants = $this->Users_model->getListOfParticipantsForUser($this->session->userId, $this->session->roletitle);
  		$this->load->view('includes/header');
		$this->load->view('webapp/Home/home', $data);
		$this->load->view('includes/footer');
  }
}
