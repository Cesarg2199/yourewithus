<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller
{
	function __construct()
  {
		// Call the Model constructor
    parent::__construct();
		if(!$this->session->loggedin){ redirect('Login/index'); }

    $this->load->model('Users_model');
    $this->load->model('Tasks_model');
  }

	public function index()
	{
    $data['users'] = $this->Users_model->getUsers();
		$this->load->view('includes/header');
		$this->load->view('webapp/Tasks/newTask', $data);
		$this->load->view('includes/footer');
	}

  public function newTask()
  {
    $data['users'] = $this->Users_model->getUsers();
    $this->load->view('includes/header');
		$this->load->view('webapp/Tasks/newTask', $data);
		$this->load->view('includes/footer');
  }

  public function editTask()
  {
		$taskId = $this->input->get('taskId');

		$data['getTask'] = $this->Tasks_model->getTask($taskId);
		$data['users'] = $this->Users_model->getUsers();
    $this->load->view('includes/header');
		$this->load->view('webapp/Tasks/editTask', $data);
		$this->load->view('includes/footer');
  }

  public function pendingTasks()
  {
    $data['pendingTasks'] = $this->Tasks_model->getPendingTasks();
    $this->load->view('includes/header');
		$this->load->view('webapp/Tasks/pendingTasks', $data);
		$this->load->view('includes/footer');
  }

  public function sentTasks()
  {
		$data['sentTasks'] = $this->Tasks_model->getSentTasks();
    $this->load->view('includes/header');
		$this->load->view('webapp/Tasks/sentTasks', $data);
		$this->load->view('includes/footer');
  }

  public function completedTasks()
  {
		$data['completedTasks'] = $this->Tasks_model->getCompletedTasks();
    $this->load->view('includes/header');
		$this->load->view('webapp/Tasks/completedTasks', $data);
		$this->load->view('includes/footer');
  }

  public function allTasks()
  {
		if($this->session->role == "1") {
			$data['allTasks'] = $this->Tasks_model->getAllTasks();
	    $this->load->view('includes/header');
			$this->load->view('webapp/Tasks/allTasks', $data);
			$this->load->view('includes/footer');
		}
		else {
			redirect('Tasks/newTask');
		}
  }

  public function assignTask()
  {
    $this->Tasks_model->assignTask();
    redirect('Tasks/newTask');
  }

	public function reassignTask()
	{
		$this->Tasks_model->reassignTask();
    redirect('Tasks/sentTasks');
	}

	public function completedTask()
	{
		$this->Tasks_model->completedTask(); //Set a task's completed field to todays date
		return true;
	}

	public function uncompleteTask()
	{
		$this->Tasks_model->uncompleteTask(); //Set a task's completed field to n
		return true;
	}

	public function deleteSentTask()
	{
		$this->Tasks_model->deleteSentTask(); //Delete a task from tasks table and notes accociated with that task from tasknotes table
		return true;
	}

	public function getNotes()
	{
		$allNotes = $this->Tasks_model->getNotes(); //Returns array of task notes accociated with a task from tasknotes table
		echo json_encode($allNotes); //Echo the array in json format so that the javascript can read it
	}

	public function addNote()
	{
		$this->Tasks_model->addNote(); //Creates new entry in tasknotes table
		return true;
	}

	public function deleteNote()
	{
		$this->Tasks_model->deleteNote(); //Delete a note from the tasknotes table
		return true;
	}
}
