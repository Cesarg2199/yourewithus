<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller
{
	function __construct()
  {
    parent::__construct();
		if(!$this->session->loggedin){ redirect('Login/index'); }

		$this->load->model('Participants_model');
		$this->load->model('Schools_model');
  }

	public function createPED($participantId)
  {
		$data['ped'] = $this->Participants_model->getPED($participantId)[0];
		$data['participantId'] = $participantId;
		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId)[0];
		return $this->load->view('webapp/Participants/Prints/pedPrint', $data, TRUE);
  }

  public function ped()
  {
		$participantId = $this->input->get('participantId');

		//ob_clean();
		include('libraries/MPDF/mpdf.php');
    $mpdf= new mPDF('A4');
    $mpdf->WriteHTML($this->createPED($participantId));
    $mpdf->SetHTMLFooter('<p>www.yourewithus.org
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													978-587-6663
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													michael@yourewithus.org
													</p>');
    $file = $mpdf->Output("ped.pdf", 'D');
    return true;
  }

	public function createIntake($participantId)
  {
		$data['intake'] = $this->Participants_model->getIntakeForm($participantId)[0];
		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId)[0];
		return $this->load->view('webapp/Participants/Prints/intakePrint', $data, TRUE);
  }

  public function intake()
  {
		$participantId = $this->input->get('participantId');

		include('libraries/MPDF/mpdf.php');
    $mpdf= new mPDF('A4');
    $mpdf->WriteHTML($this->createIntake($participantId));
		$mpdf->SetHTMLFooter('<p>www.yourewithus.org
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													978-587-6663
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													michael@yourewithus.org
													</p>');
    $file = $mpdf->Output("intake.pdf", 'D');
    return true;
  }

	public function createScoutingReport($participantId)
  {
		$data['sr'] = $this->Participants_model->getScoutingReport($participantId)[0];
		$data['participantInfo'] = $this->Participants_model->getParticipant($participantId)[0];
		return $this->load->view('webapp/Participants/Prints/scoutingReportPrint', $data, TRUE);
  }

  public function scoutingReport()
  {
		$participantId = $this->input->get('participantId');

		include('libraries/MPDF/mpdf.php');
    $mpdf= new mPDF('A4');
    $mpdf->WriteHTML($this->createScoutingReport($participantId));
		$mpdf->SetHTMLFooter('<p>www.yourewithus.org
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													978-587-6663
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													michael@yourewithus.org
													</p>');
    $file = $mpdf->Output("scoutingReport.pdf", 'D');
    return true;
  }
}
