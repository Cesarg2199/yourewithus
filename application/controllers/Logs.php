<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller
{
	function __construct()
  	{
		// Call the Model constructor
		parent::__construct();
		if(!$this->session->loggedin) { redirect('Login/index'); }

		$this->load->model('Users_model');
		$this->load->model('Participants_model');
		$this->load->model('Logs_model');
  	}

	public function index()
	{
		$data['participants'] = $this->Participants_model->getUserParticipants();
		$this->load->view('includes/header');
		$this->load->view('webapp/Logs/dailylog', $data);
		$this->load->view('includes/footer');
	}

	public function newDailyLog()
	{
		$this->Logs_model->newDailyLog();
		redirect('Logs');
	}

	public function expenseSheet()
	{
		$this->load->view('includes/header');
		$this->load->view('webapp/Logs/expenseSheet');
		$this->load->view('includes/footer');
	}

	public function newExpenseSheet()
	{
		$this->Logs_model->newExpenseSheet();
		redirect('Logs/expenseSheet');
	}

	public function inclusionAssessment()
	{
		$data['participants'] = $this->Participants_model->getUserParticipants();
		$this->load->view('includes/header');
		$this->load->view('webapp/Logs/inclusion', $data);
		$this->load->view('includes/footer');
	}

	public function newIA()
	{
		$this->Logs_model->newIA();
		redirect('Logs/inclusionAssessment');
	}

	public function timeSheet()
	{
		$data['participants'] = $this->Participants_model->getUserParticipants();
		$this->load->view('includes/header');
		$this->load->view('webapp/Logs/timeSheet', $data);
		$this->load->view('includes/footer');
	}

	public function saveTimeSheet()
	{
		$this->Logs_model->saveTimeSheet();
		redirect('Logs/timeSheet');
	}
}
