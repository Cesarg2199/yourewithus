<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
				$this->load->model('Subscribe_model');
    }

    public function index()
    {
    	$this->load->view('website/home');
    }

		public function subscribe()
		{
			$this->Subscribe_model->addEmail();
			return true;
		}
}
