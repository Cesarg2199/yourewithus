<div class="container">
  <h1 class="text-center">Edit User</h1>
  <div class="row">
    <?php echo form_open('Users/updateUser', 'data-toggle="validator" '); ?>
    <?php foreach($user as $info) ?>
    <input type="hidden" name="id" value="<?php echo $info['id']; ?>" />
    <div class="form-group ">
      <label class="control-label requiredField" for="firstname">
       First Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="firstname" name="firstname" placeholder="First name" type="text" value="<?php echo $info['firstname']; ?>" required="requried" />
    </div>
    <div class="form-group ">
      <label class="control-label requiredField" for="lastname">
       Last Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="lastname" name="lastname" placeholder="Last name" type="text" value="<?php echo $info['lastname']; ?>" required="requried" />
    </div>
    <div class="form-group ">
      <label class="control-label requiredField" for="email">
       Email
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="email" name="email" placeholder="Email" type="email" data-error="The email entered is invalid" value="<?php echo $info['email']; ?>" required="requried" />
      <div class="help-block with-errors"></div>
    </div>

    <div class="form-group ">
     <label class="control-label requiredField" for="phone">
      Phone
     </label>
     <input class="form-control" id="phone" name="phone" placeholder="Phone" type="text" value="<?php echo $info['phone']; ?>"/>
    </div>

    <div class="form-group ">
      <label class="control-label requiredField" for="password">
       Save New Password
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="password" name="password" placeholder="Password" type="password" />
    </div>
    <div class="form-group ">
      <label class="control-label requiredField" for="cpassword">
       Confirm New Password
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" type="password" data-match="#password" data-match-error="Whoops, these don't match" />
      <div class="help-block with-errors"></div>
    </div>

    <div id="schoolLinking">
        <div class="form-group" id="schoolField">
        <label class="control-label requiredField" for="role">Link School<span class="asteriskField">*</span></label>
          <?php foreach ($linkedSchools as $link) {?>
            <div id="school<?php echo $link['id'];?>">
              <div class="input-group">
                <span class="input-group-btn">
                <button class="btn btn-danger" onclick="removeSchool(<?php echo $link['id'];?>)" type="button">x</button>
                </span>
                <select class="select form-control participant" name="schoolList[]" readonly>
                  <option class="schoolPicked" value="<?php echo $link['schoolName']; ?>"><?php echo $link['schoolName']; ?></option>
                </select>
                  </div>
            </div>
            <div style="margin-top:15px"></div>
          <?php } ?>
          <div id="schoolSet">
          <select class="select form-control" id="schoolList" name="schoolList[]" onchange="loadParticipants()">
           <option value="">-</option>
            <?php foreach($entireSchoolList as $schoolList) { ?>
              <option class="schoolPicked" value="<?php echo $schoolList['name'] ?>"><?php echo $schoolList['name']; ?></option>
            <?php } ?>
          </select>
          <div style="margin-top:15px"></div>
        </div>
      </div>
      <button type="button" id="linkSchool" onclick="addSchool()" class="btn btn-warning">Link School</button>
    </div>

    <br>
    <div class="form-group ">
      <label class="control-label requiredField" for="role">
       Choose a Role
       <span class="asteriskField">
        *
       </span>
      </label>
      <select class="select form-control" onchange="roleCheck()" id="role" name="role">
        <option value="<?php echo $info['roletitle'];?>"><?php echo $info['roletitle'];?></option>
        <option value="">---</option>
        <?php if($this->session->roletitle == "Administrator") { ?>
          <option value="Administrator">Administrator</option>
          <option value="Region Administrator">Region Administrator</option>
        <?php } ?>
        <option value="Licensed Clinician">Licensed Clinician</option>
        <option value="Mentor">Mentor </option>
        <option value="Adult Companion">Adult Companion</option>
        <option value="Campus Case Coordinator">Campus Case Coordinator</option>
        <option value="Group">Group</option>
        <option value="Parent">Parent</option>
      </select>
    </div>

    <!-- LOOK AT ADD USERS TO ADJUST THIS. -->
    <!-- Start of participants -->
    <div class="form-group" id="participantField">
    <label class="control-label requiredField" for="role">Link Participant<span class="asteriskField">*</span></label>
      <?php if(isset($list)) {
        foreach ($list as $persons) {
        foreach($persons as $person) {
      ?>
        <div id="participant<?php echo $person['id'];?>">
          <div class="input-group">
            <span class="input-group-btn">
            <button class="btn btn-danger" onclick="removeParticipant(<?php echo $person['id'];?>)" type="button">x</button>
            </span>
            <select class="select form-control participant" name="participant[]">
              <option value="<?php echo $person['id']; ?>"><?php echo $person['firstname'].' '.$person['lastname']; ?></option>
              <option value="">-</option>
              <?php foreach($participants as $participant) { ?>
              <option value="<?php echo $participant['id'] ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
              <?php } ?>
            </select>
              </div>
        </div>
        <div style="margin-top:15px"></div>
      <?php } } } ?>

      <div style="margin-top:15px"></div>
      <div id="participantSet">
      <select class="select form-control participant" id="clone" name="participant[]">
       <option value="">-</option>
        <?php foreach($participants as $participant) { ?>
          <option value="<?php echo $participant['id'] ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
        <?php } ?>
      </select>
      <div style="margin-top:15px"></div>
    </div>
  </div>
  <!-- end of participants -->

      <input class="btn btn-warning pull-right" name="submit" type="submit" value="Update User" style="margin-left:5px;">

  <?php if($this->session->role == 1) { ?>
    <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#schoolAddModal">
     Quick Add School
    </button>
  <?php } ?>

  </form>
  <button onclick="addParticipant()" id="link" class="btn btn-warning" type="button">Link Another</button>

  <div class="modal fade" id="schoolAddModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header" style="background-color:#D23C3C !important; color:white">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title text-center">Add School</h4>
              </div>
              <div class="modal-body">
                  <form role="form">
                      <div class="form-group">
                          <label class="control-label" for="schoolName">Name
                              <br>
                          </label>
                          <input type="text" class="form-control" id="schoolNameAdd" placeholder="Enter School Name">
                      </div>

                  </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-success" onclick="quickAddSchool()">Save</button>
              </div>
          </div>
      </div>
  </div>

</div>
<br><br>
<script src="<?=base_url('assets/js/mask.js');?>"></script>
<script src="<?=base_url('assets/js/users.js');?>"></script>
