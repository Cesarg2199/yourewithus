<div class="container">
<h1 class="text-center">Add User</h1>
  <div class="row">
    <?php echo form_open('Users/insertUser', 'data-toggle="validator" '); ?>
     <div class="form-group ">
      <label class="control-label requiredField" for="firstname">
       First Name
       <span class="asteriskField">*</span>
      </label>
      <input class="form-control" id="firstname" name="firstname" placeholder="First name" type="text" required="requried"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="lastname">
       Last Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="lastname" name="lastname" placeholder="Last name" type="text" required="requried"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="email">
       Email
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="email" name="email" placeholder="Email" type="email"  data-error="The email entered is invalid" required="requried"/>
      <div class="help-block with-errors"></div>
     </div>

     <div class="form-group ">
      <label class="control-label requiredField" for="phone">
       Phone
      </label>
      <input class="form-control" id="phone" name="phone" placeholder="Phone" type="text"/>
     </div>

     <div class="form-group ">
      <label class="control-label requiredField" for="password">
       Password
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="password" name="password" placeholder="Password" type="password" required="requried"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="cpassword">
       Confirm Password
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password" type="password" data-match="#password" data-match-error="Whoops, these don't match" required="requried"/>
      <div class="help-block with-errors"></div>
     </div>

     <!-- Role  -->
     <div class="form-group ">
      <label class="control-label requiredField" for="role">
       Choose a Role
       <span class="asteriskField">  * </span>
      </label>
      <select class="select form-control" id="role" name="role" onchange="roleCheck()">
      <option value="">-</option>

      <?php if($this->session->roletitle == "Administrator") { ?>
       <option value="Administrator">Administrator</option>
       <option value="Region Administrator">Region Administrator</option>
      <?php } ?>
       <option value="Licensed Clinician">Licensed Clinician</option>
       <option value="Mentor">Mentor</option>
       <option value="Adult Companion">Adult Companion</option>
       <option value="Campus Case Coordinator">Campus Case Coordinator </option>
       <option value="Group">Group</option>
       <option value="Parent">Parent</option>
      </select>
     </div>

     <!-- School -->
     <div id="schoolLinking">
          <div class="form-group" id="schoolField">
          <label class="control-label requiredField" for="role">Link School</label>
            <?php if($this->session->role == 1){ ?>
            <div id="schoolSet">
            <select class="select form-control" name="schoolList[]">
             <option value="">-</option>
              <?php foreach($entireSchoolList as $schoolList) { ?>
                <option  value="<?php echo $schoolList['name'] ?>"><?php echo $schoolList['name']; ?></option>
              <?php } ?>
            </select>
            <div style="margin-top:15px"></div>
          </div>
        <?php } elseif(($this->session->role == 4) or ($this->session->role == 7)) { ?>
            <div id="schoolSet">
            <select class="select form-control" name="schoolList[]">
             <option value="">-</option>
              <?php foreach($entireSchoolList as $school) { ?>
                <option value="<?php echo $school['schoolName'] ?>"><?php echo $school['schoolName']; ?></option>
              <?php } ?>
            </select>
            <div style="margin-top:15px"></div>
          </div>
          <?php } ?>
          </div>
          <button type="button" id="linkSchool" onclick="addSchool()" class="btn btn-warning">Link Another School</button>
     </div>
     <br>


    <!-- Start of participants -->
     <div class="form-group" id="participantField">
      <label class="control-label requiredField" for="role">Link Participant<span class="asteriskField">*</span>
      </label>

      <!-- Change this to ajax model to dynamically load what participants are avalible by school. -->
      <div id="participantSet">
      <select class="select form-control" id="clone" name="participant[]">
       <option value="">-</option>
        <?php foreach($participants as $participant) { ?>
          <option value="<?php echo $participant['id'] ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
        <?php } ?>
      </select>
      <div style="margin-top: 10px;"></div>
      </div>

     </div>
     <button onclick="addParticipant()" id="link" class="btn btn-warning" type="button">Link</button>
     <!-- end of participants -->

     <!-- End of the form buttons -->
     <div class="form-group">
      <div>
        <button class="btn btn-success pull-right" name="submit" type="submit" style="margin-left:5px;">
         Add User
        </button>
        <?php if($this->session->role == 1) { ?>
        <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#schoolAddModal">
         Quick Add School
        </button>
        <?php } ?>
      </div>
     </div>

    </form>
  </div>

  <div class="modal fade" id="schoolAddModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header" style="background-color:#D23C3C !important; color:white">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title text-center">Add School</h4>
              </div>
              <div class="modal-body">
                  <form role="form">
                      <div class="form-group">
                          <label class="control-label" for="schoolName">Name
                              <br>
                          </label>
                          <input type="text" class="form-control" id="schoolNameAdd" placeholder="Enter School Name">
                      </div>

                  </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-success" onclick="quickAddSchool()">Save</button>
              </div>
          </div>
      </div>
  </div>
  <br><br>

 </div>
<script src="<?=base_url('assets/js/mask.js');?>"></script>
<script src="<?=base_url('assets/js/users.js');?>"></script>
