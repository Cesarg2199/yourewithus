<div class="container-fluid">
    <div class="page-header">
      <h1 class="text-center">User Profile</h1>
    </div>
            <h1>User Info</h1>
            <div class="row">
                <div class="col-md-4">
                    <h4>Name: <?php echo $user['firstname']." ".$user['lastname']; ?></h4>
                </div>
                <div class="col-md-4">
                    <h4>Role: <?php echo $user['roletitle']; ?></h4>
                </div>
                <div class="col-md-4">
                    <h4>Date Joined: <?php if($user['date']) { echo $user['date']; } else { echo "N/A"; }  ?></h4>
                </div>
            </div>

            <h1>Current Week Time</h1>
            <div class="row">
                <table class="table table-striped table-bordered" id="weekTotals">
                    <thead class="ywuHeader">
                        <tr>
                            <th>Living Skills (Hours)</th>
                            <th>Employment Skills (Hours)</th>
                            <th>Social Inclusion/Leisure (Hours)</th>
                            <th>Social Skills (Hours)</th>
                            <th>Administrative (Hours)</th>
                            <th>Interval Total (Hours)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php if($currentWeek['adultLivingTotal']['adultLiving']){ echo $currentWeek['adultLivingTotal']['adultLiving']; } else { echo "0"; } ; ?></td>
                            <td><?php if($currentWeek['employmentTotal']['employment']){ echo $currentWeek['employmentTotal']['employment']; } else { echo "0"; } ; ?></td>
                            <td><?php if($currentWeek['socialInclusionTotal']['socialInclusion']){ echo $currentWeek['socialInclusionTotal']['socialInclusion']; } else { echo "0"; } ; ?></td>
                            <td><?php if($currentWeek['socialTotal']['social']){ echo $currentWeek['socialTotal']['social']; } else { echo "0"; } ; ?></td>
                            <td><?php if($currentWeek['adminTotal']['admin']){ echo $currentWeek['adminTotal']['admin']; } else { echo "0"; } ; ?></td>
                            <td><?php if($currentWeek['intervalTotal']['intervalTime']){ echo $currentWeek['intervalTotal']['intervalTime']; } else { echo "0"; } ; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <h1>Connected Schools</h1>
            <div class="row">
                <div style="height:100px; width:100%; border: 1px solid #D23C3C">
                  <?php
                    if($schools) {
                    foreach($schools as $school)  {
                  ?>
                    <div class="thumbnail" style="width:250px; display:inline-block; margin:15px; border-color:#D23C3C">
                        <div class="caption">
                            <h4><?php echo $school['schoolName']; ?></h4>
                        </div>
                    </div>
                    <?php } } ?>
                </div>
            </div>

            <?php if($user['roletitle'] == "Administrator") { ?>
            <h1>Participants</h1>
            <div class="row">
                <div style="height:425px; width:100%; border: 1px solid #D23C3C; overflow:auto">
                    <?php
                      if($participants) {
                      foreach($participants as $participant) {
                    ?>
                    <div class="thumbnail" style="width:300px; display:inline-block; margin:15px; border-color:#D23C3C">
                        <div class="caption">
                            <h4><?php echo $participant['firstname']." ".$participant['lastname']; ?></h4>
                            <p> </p>
                            <div class="dropdown" style="display:inline;">
                                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Info
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo site_url("Participants/intakeForm");?>?participantId=<?php echo $participant['id']; ?>">Intake Form</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url("Participants/positiveExperienceDesign");?>?participantId=<?php echo $participant['id']; ?>">Positive Experince Design</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url("Participants/scoutingReport");?>?participantId=<?php echo $participant['id']; ?>">Scouting Report</a>
                                    </li>
                                </ul>
                            </div>
                            <a href="<?php echo site_url('Participants/participantsProfile');?>?participantId=<?php echo $participant['id'];?>" class="btn btn-primary" role="button">Profile</a>
                            <p></p>
                        </div>
                    </div>
                  <?php } }?>
                </div>
            </div>
            <?php }?>
            <h1>Recent Daily Logs</h1>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="participantsTable">
                        <thead class="ywuHeader">
                            <tr>
                                <th class="col-xs-2">Date</th>
                                <th class="col-xs-2">Partcipant</th>
                                <th class="col-xs-2">Activity</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($dailyLogs as $dailylog) { ?>
                          <tr>
                            <td><?=$dailylog['date'];?></td>
                            <td><?=$dailylog['participant'];?></td>
                            <td><?=$dailylog['activity'];?></td>
                            <td><?=$dailylog['notes'];?></td>
                          </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <h1>Recent Incluision Assessments</h1>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="participantsTable2">
                        <thead class="ywuHeader">
                            <tr>
                                <th>Date</th>
                                <th>Participant</th>
                                <th>Physical Composite</th>
                                <th>Social and Community Composite</th>
                                <th>Administrative Composite</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach($IA as $A) { ?>
                            <tr>
                              <td><?=$A['date'];?></td>
                              <td><?=$A['participant'];?></td>
                              <td><?=$A['pq'];?></td>
                              <td><?=$A['sq'];?></td>
                              <td><?=$A['aq'];?></td>
                              <td><button class="btn btn-info" onclick="viewMore(<?=$A['id'];?>)">View More+</button></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- view more  -->
        <div class="modal fade" id="viewMoreScores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color:#D23C3C !important; color:white">
                    <h4 class="modal-title" id="myModalLabel">Inclussion Assesment</h4>
                </div>
                <div class="modal-body">
                  <h2 class="text-center">Physical Presence</h2>
                  <div class="row">
                    <table class="table table-striped table-bordered">
                      <tbody>
                          <tr>
                              <td>Participant has plan for travel and arrives on time</td>
                              <td class="col-xs-3 text-center" id="mpq1"></td>
                          </tr>
                          <tr>
                              <td>Participant is actively engaging with team/group</td>
                              <td class="col-xs-3 text-center" id="mpq2"></td>
                          </tr>
                          <tr>
                              <td>All of participants needs are met during daily activities</td>
                              <td class="col-xs-3 text-center" id="mpq3"></td>
                          </tr>
                          <tr>
                              <td>Participant is comfortable to participate for full weekly schedule</td>
                              <td class="col-xs-3 text-center" id="mpq4"></td>
                          </tr>
                          <tr>
                              <td>Participant is comfortable in buildings and meeting rooms</td>
                              <td class="col-xs-3 text-center" id="mpq5"></td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                  <h2 class="text-center">Social and Community</h2>
                  <div class="row">
                    <table class="table table-striped table-bordered table-condensed">
                      <tbody>
                          <tr>
                              <td>Participant feels welcomed and greeted upon arrival</td>
                              <td class="col-xs-3 text-center" id="msq1"></td>
                          </tr>
                          <tr>
                              <td>Team/group build rapport directly with participant</td>
                              <td class="col-xs-3 text-center" id="msq2"></td>
                          </tr>
                          <tr>
                              <td>Participant is confident to vocalize needs to team/group or campus coordinator</td>
                              <td class="col-xs-3 text-center" id="msq3"></td>
                          </tr>
                          <tr>
                              <td>Team/group understand how participant best communicates and practice this understanding</td>
                              <td class="col-xs-3 text-center" id="msq4"></td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                  <h2 class="text-center">Administrative</h2>
                  <div class="row">
                    <table class="table table-striped table-bordered table-condensed">
                      <tbody>
                          <tr>
                              <td>Participant feels welcomed and greeted upon arrival</td>
                              <td class="col-xs-3 text-center" id="maq1"></td>
                          </tr>
                          <tr>
                              <td>Team/group build rapport directly with participant</td>
                              <td class="col-xs-3 text-center" id="maq2"></td>
                          </tr>
                          <tr>
                              <td>Participant is confident to vocalize needs to team/group or campus coordinator</td>
                              <td class="col-xs-3 text-center" id="maq3"></td>
                          </tr>
                          <tr>
                              <td>Program uses confidentiality and protection when dealing with participant profiles</td>
                              <td class="col-xs-3 text-center" id="maq4"></td>
                          </tr>
                      </tbody>

                  </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="modalClose" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        </div>
        <!-- End of view more -->
        <script src="<?=base_url('assets/js/userProfile.js');?>"></script>
