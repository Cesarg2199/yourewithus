<div class="container-fluid">
    <?php if(($this->session->role == "1") or ($this->session->role == "4") or ($this->session->role == "7")) { ?>
    <a class="btn btn-success pull-right" style="margin-top: 15px;" href="<?php echo site_url('Users/addUser'); ?>">+ Add User</a>
  <?php } ?>
    <div class="page-header">
        <h1 class="text-center" style="margin-top: 10px;">Users</h1>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover datatable">
            <thead class="ywuHeader">
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th class="col-md-2">Phone</th>
                    <th>Role</th>
                    <th>Options<br></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($users as $user) { ?>
                <tr id="<?php echo $user['id']; ?>">
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo $user['firstname']; ?></td>
                    <td><?php echo $user['lastname']; ?></td>
                    <td><?php echo $user['email']; ?></td>
                    <td><?php echo $user['phone']; ?><br></td>
                    <td><?php echo $user['roletitle']; ?></td>
                    <td>
                      <a type="button" href="<?php echo site_url('Users/userProfile');?>?userId=<?php echo $user['id']; ?>" class="btn btn-primary" style="margin-left: 10px">Profile</a>
                      <div class="dropdown" style="display:inline;">
                        <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Options
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url('Users/editUser');?>?userId=<?php echo $user['id']; ?>">Contact Info</a></li>
                          <?php if($this->session->roletitle == "Administrator") { ?>
                          <li><a href="#" onclick="deactivateUser(<?php echo $user['id']; ?>);return false;">Deactivate</a></li>
                          <?php } ?>
                        </ul>
                      </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
        <script src="<?=base_url('assets/js/users.js');?>"></script>
