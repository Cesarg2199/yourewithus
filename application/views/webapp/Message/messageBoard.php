<div class="container">
    <div class="page-header">
        <h1 class="text-center">Message Board</h1>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label" for="">Participant</label>
                <select id="participant" name="participant" class="form-control" onchange="addPostUnlock()">
                <option value="0">None</option>
                <?php foreach($participants as $participant) { ?>
                <option value="<?php echo $participant['id'];?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <button id="addPostBtn" onclick="addPostModal()" type="button" style="margin-top:18px" class="btn btn-block btn-success" disabled="true">+ Add Post</button>
            </div>
        </div>
    </div>
    <div id="board" class="row">
        <!-- Block to iterate and make more -->

        <!--end of blocks -->
    </div>
</div>

<!-- Add post modal -->
<div class="modal fade" id="addPostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header text-center" style="background-color:#D23C3C !important; color:white">
            <h4 class="modal-title" id="myModalLabel">Add Post</h4>
        </div>
        <div class="modal-body">
          <h2 class="text-center">Write Message</h2>
          <div class="row">
          <div class="form-horizontal ">

            <!-- Textarea -->
            <div class="col-xs-1"></div>
            <div class="form-group center-block">
              <div class="col-xs-10">
                <textarea class="form-control" rows="10" id="message" name="message"></textarea>
              </div>
            </div>
            <div class="col-xs-1"></div>

            <!-- Button -->
            <div class="form-group">

              <div class="col-xs-10">
                <button onclick="addPost()" name="btnPost" class="btn btn-success pull-right" style="margin-left:10px">+ Post</button>
                <button type="button" class="btn btn-danger pull-right" class="modalClose" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
          </div>
    </div>
</div>
</div>
<!-- end modal -->

<script type="text/javascript">
  $(document).ready(function(){

  });

  function addPostUnlock()
  {
    var id = $("#participant").val();
    if(id != 0)
    {
      $("#addPostBtn").attr("disabled", false);
      loadPosts();
    }
    else {
      $("#addPostBtn").attr("disabled", true);
      $("#board").empty();
    }
  }

  function loadPosts()
  {
    //Have a way to delete the posts on the page
    $("#board").empty();

    //Grab the participant id to load posts
    var id = $("#participant").val();

      $.ajax({
        url: "<?php echo site_url('Message/loadPosts');?>",
        type: 'POST',
        data: {id:id},
        success: function(results){
          results = JSON.parse(results);
          $.each(results, function(i, val){
            if(results[i].date != null){
              $("#board").append("<div id='"+results[i].id+"' class='col-sm-6 col-md-4'><div class='thumbnail' style='min-height:300px;'><div class='caption'><h3>"+results[i].name+"&nbsp;&nbsp;&nbsp;"+results[i].date+"</h3><p>"+results[i].message+"</p><p><button onclick='deletePost("+results[i].id+")' value='' type='button' class='btn btn-danger'>Remove Post</button></p></div></div></div>").addClass('animated zoomIn');
            }
          });
        }
      });
  }

  function deletePost(postId)
  {

    //ask the user before delete
    var remove = confirm("Are you sure you want to delete this post?");

    if(remove){
        $.ajax({
          url: "<?php echo site_url('Message/deletePost');?>",
          type: 'POST',
          data: {postId:postId},
          success: function(){
            //remove the post from view with animation
            $("#"+postId).addClass('animated zoomOut');

            //after one second remove the view from the DOM
            setTimeout(function(){
              $("#"+postId).remove();
            }, 700);
          }
        });
      }
  }

  function addPost()
  {
    //grab the text from the message box and participant id
    var message = $("#message").val();
    var participantId = $("#participant").val();

    //close the modal
    $("#addPostModal").modal("hide");

    //check if messaqge is not blank
    if(message != "")
    {
      //post the message to the database
      $.ajax({
          url: "<?php echo site_url('Message/addPost');?>",
          type: 'POST',
          data: {participantId:participantId, message:message},
          success: function(){
            //clear the text in the message box
            $("#message").empty();

            //reload the post on the screen
            loadPosts();
          }
        });
    }
    //if message is blank alert user
    else
    {
      alert("There is no message to post to the message board.");
    }
  }

  function addPostModal()
  {
    $("#addPostModal").modal("show");
  }
</script>
