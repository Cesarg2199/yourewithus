<div class="container">
      <div class="page-header text-center">
          <h1>Mentor Thoughts</h1>
      </div>
  </div>
  <div class="container">
      <div id="thoughts" class="carousel slide center-block" data-ride="carousel" data-interval="false">
          <!-- Indicators -->
          <ol class="carousel-indicators">
              <li data-target="#carousel1" data-slide-to="0" class="active"></li>
              <li data-target="#carousel1" data-slide-to="1"></li>
              <li data-target="#carousel1" data-slide-to="2"></li>
              <li data-target="#carousel1" data-slide-to="3"></li>
              <li data-target="#carousel1" data-slide-to="4"></li>
              <li data-target="#carousel1" data-slide-to="5"></li>
              <li data-target="#carousel1" data-slide-to="6"></li>
              <li data-target="#carousel1" data-slide-to="7"></li>
              <li data-target="#carousel1" data-slide-to="8"></li>
              <li data-target="#carousel1" data-slide-to="9"></li>
              <li data-target="#carousel1" data-slide-to="10"></li>
              <li data-target="#carousel1" data-slide-to="11"></li>
              <li data-target="#carousel1" data-slide-to="12"></li>
              <li data-target="#carousel1" data-slide-to="13"></li>
              <li data-target="#carousel1" data-slide-to="14"></li>
              <li data-target="#carousel1" data-slide-to="15"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
              <div class="item active">
                  <img src="<?php echo base_url('assets/documents/mentor/1.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/2.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/3.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/4.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/5.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/6.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/7.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/8.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/9.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/10.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/11.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/12.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/13.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/14.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/15.jpg') ?>" alt="">
              </div>
              <div class="item">
                  <img src="<?php echo base_url('assets/documents/mentor/16.jpg') ?>" alt="">
              </div>

          </div>
          <!-- Controls -->
          <a class="left carousel-control" id="left" href="#carousel1" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" id="right" href="#carousel1" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
      </div>
      <div style="margin-bottom:50px;">
      </div>
  </div>
<script>
  $('#left').click(function() {
    $('#thoughts').carousel('prev');
  });

  $('#right').click(function() {
    $('#thoughts').carousel('next');
  });
</script>
