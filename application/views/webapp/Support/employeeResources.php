<div class="container-fluid">
          <div class="page-header text-center" data-pg-collapsed>
              <h1>Employee Resources</h1>
          </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover datatable">
                <thead class="ywuHeader">
                    <tr>
                        <th class="col-xs-8">Document</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach($file as $name) { ?>
                    <tr>
                        <td><?php echo $name; ?></td>
                        <td>
                            <a href="<?php echo base_url('assets/documents/resources');?><?php echo "/".$name; ?>" class="btn btn-danger btn-block" role="button" target="_blank">View</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
