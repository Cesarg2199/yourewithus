<div class="container">
    <div class="page-header text-center">
        <h1>The Why</h1>
    </div>
    <div class="center-block embed-responsive embed-responsive-16by9">
        <video controls preload="none">
            <source src="<?php echo base_url('assets/documents/thewhy/the_why.mp4') ?>" type="video/mp4">
                Your browser does not support HTML5 video.
        </video>
    </div>
</div>

