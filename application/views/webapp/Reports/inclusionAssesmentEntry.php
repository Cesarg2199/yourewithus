<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 text-center page-header">
            <h1>Inclusion Assesment Entries</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">Participant</label>
                <select id="participant" name="participant" class="form-control" onchange="unlock()">
                <option>None</option>
                <?php foreach($participants as $participant) { ?>
                <option value="<?php echo $participant['id']; ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>
       <div class="col-md-3">
          <label class="control-label" for="">&nbsp;</label>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle col-xs-12" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="options" disabled>
              Options
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li class="dropdown-header">Date Ranges</li>
              <li><a onclick="loadDate('<?=$today;?>', '<?=$today;?>')">Today</a></li>
              <li><a onclick="loadDate('<?=$thisWeekFrom;?>', '<?=$thisWeekTo;?>')">This Week</a></li>
              <li><a onclick="loadDate('<?=$thisMonthFrom;?>', '<?=$thisMonthTo;?>')">This Month</a></li>
              <li class="dropdown-header">Export Options</li>
              <li><a onclick="exportReport();">Excel</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">From</label>
                <input type="text" class="form-control" id="from" placeholder="From" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">To</label>
                <input type="text" class="form-control" id="to" placeholder="To" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" id="participantsTable">
                <thead class="ywuHeader">
                    <tr>
                        <th>Date</th>
                        <th>User</th>
                        <th>Physical Composite</th>
                        <th>Social and Community Composite</th>
                        <th>Administrative Composite</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody id="tdata">
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- view more  -->
<div class="modal fade" id="viewMoreScores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header text-center" style="background-color:#D23C3C !important; color:white">
            <h4 class="modal-title" id="myModalLabel">Inclussion Assesment</h4>
        </div>
        <div class="modal-body">
          <h2 class="text-center">Physical Presence</h2>
          <div class="row">
            <table class="table table-striped table-bordered">
              <tbody>
                  <tr>
                      <td>Participant has plan for travel and arrives on time</td>
                      <td class="col-xs-3 text-center" id="mpq1"></td>
                  </tr>
                  <tr>
                      <td>Participant is actively engaging with team/group</td>
                      <td class="col-xs-3 text-center" id="mpq2"></td>
                  </tr>
                  <tr>
                      <td>All of participants needs are met during daily activities</td>
                      <td class="col-xs-3 text-center" id="mpq3"></td>
                  </tr>
                  <tr>
                      <td>Participant is comfortable to participate for full weekly schedule</td>
                      <td class="col-xs-3 text-center" id="mpq4"></td>
                  </tr>
                  <tr>
                      <td>Participant is comfortable in buildings and meeting rooms</td>
                      <td class="col-xs-3 text-center" id="mpq5"></td>
                  </tr>
              </tbody>
            </table>
          </div>
          <h2 class="text-center">Social and Community</h2>
          <div class="row">
            <table class="table table-striped table-bordered table-condensed">
              <tbody>
                  <tr>
                      <td>Participant feels welcomed and greeted upon arrival</td>
                      <td class="col-xs-3 text-center" id="msq1"></td>
                  </tr>
                  <tr>
                      <td>Team/group build rapport directly with participant</td>
                      <td class="col-xs-3 text-center" id="msq2"></td>
                  </tr>
                  <tr>
                      <td>Participant is confident to vocalize needs to team/group or campus coordinator</td>
                      <td class="col-xs-3 text-center" id="msq3"></td>
                  </tr>
                  <tr>
                      <td>Team/group understand how participant best communicates and practice this understanding</td>
                      <td class="col-xs-3 text-center" id="msq4"></td>
                  </tr>
              </tbody>
            </table>
          </div>
          <h2 class="text-center">Administrative</h2>
          <div class="row">
            <table class="table table-striped table-bordered table-condensed">
              <tbody>
                  <tr>
                      <td>Participant feels welcomed and greeted upon arrival</td>
                      <td class="col-xs-3 text-center" id="maq1"></td>
                  </tr>
                  <tr>
                      <td>Team/group build rapport directly with participant</td>
                      <td class="col-xs-3 text-center" id="maq2"></td>
                  </tr>
                  <tr>
                      <td>Participant is confident to vocalize needs to team/group or campus coordinator</td>
                      <td class="col-xs-3 text-center" id="maq3"></td>
                  </tr>
                  <tr>
                      <td>Program uses confidentiality and protection when dealing with participant profiles</td>
                      <td class="col-xs-3 text-center" id="maq4"></td>
                  </tr>
              </tbody>

          </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" class="modalClose" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
<!-- End of view more -->

<script type="text/javascript">
  $(document).ready(function(){

    $( "#from" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd'
    });

    $( "#to" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd'
    });
  });

function unlock()
{
  $("#options").attr("disabled", false);
  $("#from").attr("disabled", false);
  $("#to").attr("disabled", false);
  loadDataTable();
}

function loadDate(from, to)
{
  $("#from").val(from);
  $("#to").val(to);
  loadDataTable();
}

  function loadDataTable()
  {
    $("#tdata").empty();

    var participant = $("#participant").val();
    var dFrom = $("#from").val();
    var dTo = $("#to").val();

    if(dTo != ''){
      $.ajax({
        url: "<?php echo site_url('Reports/inclusionReport');?>",
        type: 'POST',
        data: {participant:participant, dFrom:dFrom, dTo:dTo},
        success: function(results){
          results = JSON.parse(results);
          $.each(results, function(i, val){
            if(results[i].date != null){
              $("#tdata").append("<tr><td>"+results[i].date+"</td><td>"+results[i].user+"</td><td>"+results[i].pq+"</td><td>"+results[i].sq+"</td><td>"+results[i].aq+"</td><td><button class='btn btn-info' onclick='viewMore("+results[i].id+")'>View More +</button</td></tr>");
            }
          });

          if(!$("#to").val() == "")
          {
            $("#participantsTable").DataTable();
          }
        }
      });
    }
  }

  function viewMore(id)
  {
    prepareModal();

    //load the modal with the new data
    $.ajax({
      url: "<?php echo site_url('Reports/inclusionReportFull');?>",
      type: 'POST',
      data: {id:id},
      success: function(full){
        full = JSON.parse(full);

        //pq
        $("#mpq1").html(full[0].pq1);
        $("#mpq2").html(full[0].pq2);
        $("#mpq3").html(full[0].pq3);
        $("#mpq4").html(full[0].pq4);
        $("#mpq5").html(full[0].pq5);

        //sq
        $("#msq1").html(full[0].sq1);
        $("#msq2").html(full[0].sq1);
        $("#msq3").html(full[0].sq1);
        $("#msq4").html(full[0].sq1);

        //aq
        $("#maq1").html(full[0].aq1);
        $("#maq2").html(full[0].aq2);
        $("#maq3").html(full[0].aq3);
        $("#maq4").html(full[0].aq4);
      }
    });

    //show the modal
    $("#viewMoreScores").modal("show");
  }

  function prepareModal()
  {
    $("#mpq1").html('');
    $("#mpq2").html('');
    $("#mpq3").html('');
    $("#mpq4").html('');
    $("#mpq5").html('');

    //sq
    $("#msq1").html('');
    $("#msq2").html('');
    $("#msq3").html('');
    $("#msq4").html('');

    //aq
    $("#maq1").html('');
    $("#maq2").html('');
    $("#maq3").html('');
    $("#maq4").html('');
  }

  function exportReport()
  {
    var participant = $("#participant").val();
    var dFrom = $("#from").val();
    var dTo = $("#to").val();
    window.location.href = '<?php echo site_url('Reports/exportInclusionReport'); ?>?participant='+participant+'&dFrom='+dFrom+'&dTo='+dTo+'';
  }
</script>
