<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 text-center page-header">
            <h1>Expense Sheet Entries</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">Users</label>
                <select id="user" name="user" class="form-control" onchange="unlock()">
                <option>None</option>
                <?php foreach($users as $user) { ?>
                <option value="<?php echo $user['id']; ?>"><?php echo $user['firstname']." ".$user['lastname']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-3">
          <label class="control-label" for="">&nbsp;</label>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle col-xs-12" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="options" disabled>
              Options
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li class="dropdown-header">Date Ranges</li>
              <li><a onclick="loadDate('<?=$today;?>', '<?=$today;?>')">Today</a></li>
              <li><a onclick="loadDate('<?=$thisWeekFrom;?>', '<?=$thisWeekTo;?>')">This Week</a></li>
              <li><a onclick="loadDate('<?=$thisMonthFrom;?>', '<?=$thisMonthTo;?>')">This Month</a></li>
              <li class="dropdown-header">Export Options</li>
              <li><a onclick="exportTimeSheet();">Excel</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">From</label>
                <input type="text" class="form-control" id="from" placeholder="From" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">To</label>
                <input type="text" class="form-control" id="to" placeholder="To" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>

        </div>
    </div>
    <br />
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered datatable" id="expenseTable">
                <thead class="ywuHeader">
                    <tr>
                        <th class="col-xs-2">Date</th>
                        <th class="col-xs-2">User</th>
                        <th class="col-xs-2">Item</th>
                        <th>Amount</th>
                        <th>Receipt</th>
                        <th class="col-xs-4">Notes</th>
                    </tr>
                </thead>
                <tbody id="tdata">
                <?php if(isset($recent)) { foreach($recent as $entry) { ?>
                  <tr>
                      <td><?=$entry['date'];?></td>
                      <td><?=$entry['user'];?></td>
                      <td><?=$entry['item'];?></td>
                      <td>$<?=$entry['amount'];?></td>
                      <td><?=$entry['receipt'];?></td>
                      <td><?=$entry['notes'];?></td>
                  </tr>
                <?php } } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

  $( "#from" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  $( "#to" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });
});

function unlock()
{
  $("#options").attr("disabled", false);
  $("#from").attr("disabled", false);
  $("#to").attr("disabled", false);
  loadDataTable();
}

function loadDate(from, to)
{
  $("#from").val(from);
  $("#to").val(to);
  loadDataTable();
}

  function loadDataTable()
  {
    $("#tdata").empty();

    var user = $("#user").val();
    var dFrom = $("#from").val();
    var dTo = $("#to").val();

    $('#expenseTable').dataTable().fnClearTable();
    $('#expenseTable').dataTable().fnDestroy();

    $.ajax({
      url: "<?php echo site_url('Reports/userExpenseSheet');?>",
      type: 'POST',
      data: {user:user, dFrom:dFrom, dTo:dTo},
      success: function(results){
        results = JSON.parse(results);
        $.each(results, function(i, val){
          $("#tdata").append("<tr><td>"+results[i].date+"</td><td>"+results[i].user+"</td><td>"+results[i].item+"</td><td>"+results[i].amount+"</td><td>"+results[i].receipt+"</td><td>"+results[i].notes+"</td></tr>");
        });

        if(!$("#to").val() == "")
        {
          $("#expenseTable").DataTable();
        }

      }
    });
  }

  function exportTimeSheet()
  {
    var user = $("#user").val();
    var dFrom = $("#from").val();
    var dTo = $("#to").val();
    window.location.href = '<?php echo site_url('Reports/exportTimeSheet'); ?>?user='+user+'&dFrom='+dFrom+'&dTo='+dTo+'';
  }
</script>
