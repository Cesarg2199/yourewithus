<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 text-center page-header">
            <h1>Time Sheet Entries</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">Users</label>
                <select id="user" name="user" class="form-control" onchange="changeOrUnlock()">
                <option>None</option>
                <?php foreach($users as $user) { ?>
                <option value="<?php echo $user['id']; ?>"><?php echo $user['firstname']." ".$user['lastname']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-3">
          <label class="control-label" for="">&nbsp;</label>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle col-xs-12" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="options" disabled>
              Options
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li class="dropdown-header">Date Ranges</li>
              <li><a onclick="loadDate('<?=$today;?>', '<?=$today;?>')">Today</a></li>
              <li><a onclick="loadDate('<?=$thisWeekFrom;?>', '<?=$thisWeekTo;?>')">This Week</a></li>
              <li><a onclick="loadDate('<?=$thisMonthFrom;?>', '<?=$thisMonthTo;?>')">This Month</a></li>
              <li class="dropdown-header">Export Options</li>
              <li><a onclick="exportTimeSheet();">Excel</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">From</label>
                <input type="text" class="form-control" id="from" placeholder="From" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">To</label>
                <input type="text" class="form-control" id="to" placeholder="To" onchange="loadDataTable()" autocomplete="off"  disabled>
            </div>

        </div>
    </div>
    <br>
    <h3>Totals</h3>
    <table class="table table-striped table-hover table-bordered">
        <thead class="ywuHeader">
            <tr>
                <th>Living Skills (Hours)</th>
                <th>Employment Skills (Hours)</th>
                <th>Social Inclusion/Leisure (Hours)</th>
                <th>Social Skills (Hours)</th>
                <th>Administrative (Hours)</th>
                <th>Interval Total (Hours)</th>
            </tr>
        </thead>
        <tbody id="tdatahead">
            <tr>
                <td><span id="adultLivingTotal"></td>
                <td><span id="employmentTotal"></td>
                <td><span id="socialInclusionTotal"></span></td>
                <td><span id="socialTotal"></span></td>
                <td><span id="adminTotal"></span></td>
                <td><span id="intervalTotal"></span></td>
            </tr>
        </tbody>
    </table>
    <br />
    <div class="row">
      <h3>Records</h3>
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" id="timeTable">
                <thead class="ywuHeader">
                    <tr>
                        <th class="col-xs-2">Date</th>
                        <th class="col-xs-2">Participant</th>
                        <th class="col-xs-2">Activity</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Total Time (Hours)</th>
                    </tr>
                </thead>
                <tbody id="tdata">
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

  $( "#from" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  $( "#to" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });
});

function changeOrUnlock()
{
  if($("#from").val() == "" && $("#to").val() == "") {
    unlock();
  }
  else {
    loadDataTable();
  }
}

function unlock()
{
  $("#options").attr("disabled", false);
  $("#from").attr("disabled", false);
  $("#to").attr("disabled", false);
}

function loadDate(from, to)
{
  $("#from").val(from);
  $("#to").val(to);
  loadDataTable();
}

function clearHeader() {
  $("#intervalTotal").html('');
  $("#adultLivingTotal").html('');
  $("#employmentTotal").html('');
  $("#socialInclusionTotal").html('');
  $("#socialTotal").html('');
  $("#adminTotal").html('');
}

function loadDataTable() {
  // trigger loading screen here
  loadingScreen();
  $('#timeTable').DataTable().clear().destroy();
  clearHeader();

  var user = $("#user").val();
  var dFrom = $("#from").val();
  var dTo = $("#to").val();

  $.ajax({
    url: "<?php echo site_url('Reports/userTimeSheet');?>",
    type: 'POST',
    data: {user:user, dFrom:dFrom, dTo:dTo},
    success: function(results){
      results = JSON.parse(results);
      $.each(results, function(i, val){
        $("#tdata").append("<tr><td>"+results[i].date.substring(0, results[i].date.length - 8)+"</td><td>"+results[i].participant+"</td><td>"+results[i].activity+"</td><td>"+results[i].timeIn+"</td><td>"+results[i].timeOut+"</td><td>"+results[i].totalTime+"</td></tr>");
      });

      if(!$("#to").val() == "") {
        $("#timeTable").DataTable();
        loadHeadings(user, dFrom, dTo);
      }
    }
  });
  //end loading screen 
  loadingScreen();
}

  function loadHeadings(user, dFrom, dTo)
  {
    $.ajax({
      url: "<?php echo site_url('Reports/userTimeSheetHeadings');?>",
      type: 'POST',
      data: {user:user, dFrom:dFrom, dTo:dTo},
      success: function(results){
        results = JSON.parse(results);
        console.log(results);

        //If results are null, display as zero
        var interval = results.intervalTotal.intervalTime
        var adultLiving = results.adultLivingTotal.adultLiving;
        var employment = results.employmentTotal.employment;
        var socialInclusion = results.socialInclusionTotal.socialInclusion;
        var social = results.socialTotal.social;
        var admin = results.adminTotal.admin;

        if(interval == null){ interval = '0'; }
        if(adultLiving == null){ adultLiving = '0'; }
        if(employment == null){ employment = '0'; }
        if(socialInclusion == null){ socialInclusion = '0'; }
        if(social == null){ social = '0'; }
        if(admin == null){ admin = '0'; }

        $("#intervalTotal").html(interval);
        $("#adultLivingTotal").html(adultLiving);
        $("#employmentTotal").html(employment);
        $("#socialInclusionTotal").html(socialInclusion);
        $("#socialTotal").html(social);
        $("#adminTotal").html(admin);
      }
    });
  }

  function exportTimeSheet()
  {
    var user = $("#user").val();
    var dFrom = $("#from").val();
    var dTo = $("#to").val();
    window.location.href = '<?php echo site_url('Reports/exportTimeSheet'); ?>?user='+user+'&dFrom='+dFrom+'&dTo='+dTo+'';
  }
</script>
