<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 text-center page-header">
            <h1>Daily Log Entries</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">Participant</label>
                <select id="participant" name="participant" class="form-control" onchange="unlock()">
                <option>None</option>
                <?php foreach($participants as $participant) { ?>
                <option value="<?php echo $participant['id']; ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
                <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-3">
          <label class="control-label" for="">&nbsp;</label>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle col-xs-12" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="options" disabled>
              Options
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li class="dropdown-header">Date Ranges</li>
              <li><a onclick="loadDate('<?=$today;?>', '<?=$today;?>')">Today</a></li>
              <li><a onclick="loadDate('<?=$thisWeekFrom;?>', '<?=$thisWeekTo;?>')">This Week</a></li>
              <li><a onclick="loadDate('<?=$thisMonthFrom;?>', '<?=$thisMonthTo;?>')">This Month</a></li>
              <li class="dropdown-header">Export Options</li>
              <li><a onclick="exportDailyReport();">Excel</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">From</label>
                <input type="text" class="form-control" id="from" placeholder="From" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="">To</label>
                <input type="text" class="form-control" id="to" placeholder="To" onchange="loadDataTable()" autocomplete="off" disabled>
            </div>

        </div>
    </div>
    <br />
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" id="participantsTable">
                <thead class="ywuHeader">
                    <tr>
                        <th class="col-xs-2">Date</th>
                        <th class="col-xs-2">User</th>
                        <th class="col-xs-2">Activity</th>
                        <th>Notes</th>
                    </tr>
                </thead>
                <tbody id="tdata">
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

  $( "#from" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  $( "#to" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });
});

function unlock()
{
  $("#options").attr("disabled", false);
  $("#from").attr("disabled", false);
  $("#to").attr("disabled", false);
  loadDataTable();
}

function loadDate(from, to)
{
  $("#from").val(from);
  $("#to").val(to);
  loadDataTable();
}

function loadDataTable() {
  $('#participantsTable').DataTable().clear().destroy();
  var participant = $("#participant").val();
  var dFrom = $("#from").val();
  var dTo = $("#to").val();

  if(dFrom != ""){
    $.ajax({
      url: "<?php echo site_url('Reports/userReport');?>",
      type: 'POST',
      data: {participant:participant, dFrom:dFrom, dTo:dTo},
      success: function(results){
        $("#tdata").empty();

        results = JSON.parse(results);
        $.each(results, function(i, val){
          $("#tdata").append("<tr><td>"+results[i].date.substring(0, results[i].date.length - 8)+"</td><td>"+results[i].user+"</td><td>"+results[i].activity+"</td><td>"+results[i].notes+"</td></tr>");
        });

        if($("#to").val() != "") {
          $("#participantsTable").DataTable();
        }

      }
    });
  }

}

  function exportDailyReport()
  {
    var participant = $("#participant").val();
    var dFrom = $("#from").val();
    var dTo = $("#to").val();
    window.location.href = '<?php echo site_url('Reports/exportDailyReport'); ?>?participant='+participant+'&dFrom='+dFrom+'&dTo='+dTo+'';
  }
</script>
