<div class="container">
    <div class="page-header text-center">
        <h1>Report Bug</h1>
    </div>
    <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
          <?php echo form_open('Bugs/bugs'); ?>
            <div class="form-group">
                <label class="control-label" for="date">Date</label>
                <input type="text" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo date('Y-m-d H:i:s'); ?>" disabled="true">
            </div>
            <div class="form-group">
                <label class="control-label" for="description">Description</label>
                <textarea class="form-control" name="description" id="description" rows="8"></textarea>
            </div>
            <button type="submit" class="btn btn-default btn-danger pull-right">Report</button>
          </form>
        </div>
        <div class="col-xs-3"></div>
    </div>
</div>
