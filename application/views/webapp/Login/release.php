<html lang="en" data-pgc-set-master>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>YWU | Release Notes</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        @import 'https://fonts.googleapis.com/css?family=Open+Sans';
          * {
                  font-family: 'Open Sans', sans-serif;
          }
    </style>

    </head>
    <body>
      <nav class="navbar-fixed-top navbar navbar-inverse">
              <div class="navbar-header">
                  <a class="navbar-brand" href="<?php echo site_url('Login');?>"><span aria-hidden="true">&larr;</span>Back to Login</a>
              </div>
      </nav>

      <div class="container">
    <div class="page-header">
        <h1 class="text-center">Release Notes</h1>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h3>February 10th, 2017</h3>
        </div>
        <div class="col-md-8">
            <h4>Miscellaneous</h4>
              <p>- Improvments to the website performance.</p>
              <p>- Condensed links for better mobile use.</p>
            <h4>Schedule</h4>
              <p>- Fixed calendar bug that mixed participants calendars together.</p>
              <p>- Made calendar view larger.</p>
            <h4>Reports</h4>
              <p>- Moved subscribers to reports.</p>
              <p>- Added multple export options and filters to subscribers page</p>
              <p>- Fixed bug in the inclussion assessment report.</p>
              <p>- Added export to excel option in inclusion assessment report.</p>
              <p>- Added export to excel option in daily log report.</p>
            <h4>Users</h4>
              <p>- Added search filters for the users and participants page.</p>
            <h4>Logs</h4>
              <p>- Moved Message Board to Logs.</p>
              <p>- Can now manually change the date and time for logs.</p>
            <h4>Support</h4>
              <p>- Fixed glitch in the mentor thoughts.</p>
            <h4>Logout</h4>
              <p>- Created an account settings page to change password and email.</p>
              <p>- Created a report bug page to report any errors.</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <h3>January 3rd, 2017</h3>
        </div>
        <div class="col-md-8">
            <h4>Schedule</h4>
            <p>- Ability to add support person for event.</p>
            <p>- Email sent to support person giving them event details.</p>
            <p>- Confirm any delete or updates on event before it happens.</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <h3>January 1st, 2017</h3>
        </div>
        <div class="col-md-8">
            <h4>Logs - Daily Log</h4>
            <p>- Increased the size of the notes area.</p>
            <h4>Users - Users</h4>
            <p>- Fixed bug with edit user.</p>
            <h4>Reports - Inclusion Assesment Entries</h4>
            <p>- Finished the report.</p>
            <p>- Included View More option to show sub totals of each section.</p>
            <h4>Surveys - All</h4>
            <p>- Surveys link to website with surveys created.</p>
            <h4>Schedule</h4>
            <p>- Calendar feature where you can create and share events related to participant.</p>
            <h4>Message Board</h4>
            <p>- Share a message to the people that are also linked to your participant.</p>
            <p>- When message is shared, an email is sent to all the linked people of the participant.</p>
            <h4>Subscribers</h4>
            <p>- View list of people who signed up for the email listing</p>
            <p>- Export list of emails to excel and marks them as exported.</p>
            <h4>Miscellaneous</h4>
            <p>- Improvments to the website performance.</p>
        </div>
    </div>
</div>

    </div>
  </body>
</html>
