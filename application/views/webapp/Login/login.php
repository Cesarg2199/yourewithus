<html lang="en" data-pgc-set-master>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>You're With Us</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href='//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.min.css' rel='stylesheet' />
    <link href='//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />


    <script src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.min.js'></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <style>
        @import 'https://fonts.googleapis.com/css?family=Open+Sans';
          * {
                  font-family: 'Open Sans', sans-serif;
          }
    </style>

    </head>
    <body>
      <nav class="navbar-fixed-top navbar navbar-inverse">
              <div class="navbar-header">
                  <a class="navbar-brand" href="<?php echo site_url('Website');?>"><span aria-hidden="true">&larr;</span>Back to Website</a>
              </div>
      </nav>

    <div class="container">
      <div class="" style="margin-top:14%;">
      <h3 class="text-center" style="color:#D23C3C">You're With Us</h3>
      <?php echo form_open('Login/login'); ?>
        <div class="col-md-4 col-md-offset-4">
            <div id="failedLogin" class="alert alert-danger" style="display: none">
        		    <strong>The Username and Password combination failed. Try again.</strong>
      		  </div>
        </div>
        <div class="row"></div>
        <div class="input-group col-md-4 col-md-offset-4">

          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
          <input type="text" class="form-control" placeholder="Email" name="email" aria-describedby="basic-addon1">
        </div>
        <div style="margin-top:15px"></div>
        <div class="input-group col-md-4 col-md-offset-4">
          <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
          <input type="password" class="form-control" placeholder="Password" name="password" aria-describedby="basic-addon1">
        </div>
        <div style="margin-top:15px"></div>
        <div class="input-group col-md-4 col-xs-8 col-xs-offset-4">
          <input type="submit" value="Login" class="btn btn-danger pull-right">
        </div>
        <div style="margin-top:30px"></div>
        <!-- <a href="<?php echo site_url('Login/releaseNotes'); ?>" style="color:#D23C3C" class="input-group col-xs-12 text-center"><span class="badge" style="background-color:#D23C3C; color:white">New!</span>  Release Notes</a> -->
            </div>
          </form>
        </form>
      </div>
    </div>
  </body>
  <input type="hidden" id="base_urls" value="<?=base_url();?>">
  <script src="<?=base_url();?>assets/js/login.js"></script>
</html>
