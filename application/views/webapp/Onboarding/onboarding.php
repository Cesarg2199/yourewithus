<!DOCTYPE html>
<html lang="en" data-pgc-set-master>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>You're With Us</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/external/css/animate.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/external/css/stepWizard.css');?>" rel="stylesheet">
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"> -->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

  <body>
    <nav class="navbar-fixed-top navbar navbar-inverse">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand">You're With Us!</a>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo site_url('Login/logout');?>">Logout</a>
            </li>
          </ul>
      </div>
      <!--/.nav-collapse -->
    </nav>
  <div class="container">
    <div class="text-center">
      <h1 class="animated fadeInDown">You're With Us! Onboarding</h1>
    </div>
    <br>
    <div class="stepwizard animated fadeInDown">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3">
                <a href="#step-1" type="button" class="btn btn-danger btn-circle">1</a>
                <p><small>CORI</small></p>
            </div>
            <div class="stepwizard-step col-xs-3">
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Family Resources</small></p>
            </div>
            <div class="stepwizard-step col-xs-3">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>DDS Incident Reporting</small></p>
            </div>
            <div class="stepwizard-step col-xs-3">
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Change Password</small></p>
            </div>
        </div>
    </div>

    <?php echo form_open('Onboarding/completeOnboarding'); ?>
        <div class="setup-content animated fadeInUp" id="step-1">
          <h3 style="display:inline-block;">CORI Form</h3>
          <a href="<?php echo base_url('assets/documents/onboarding/Cori.pdf');?>" class="btn btn-danger" style="display:inline-block; margin-left:5px;" role="button" target="_blank">Download</a>
          <div class="checkbox">
            <label>
              <input id="cbCori" class="chkBox" onclick="enableNext('cbCori', 'btnCori')" type="checkbox"> I acknowledge that I have read this entire document.
            </label>
          </div>
          <button class="btn btn-primary nextBtn pull-right" id="btnCori" type="button" disabled>Next</button>
        </div>

        <div class="setup-content animated fadeInUp" id="step-2">
          <h3 style="display:inline-block;">Family Resources</h3>
          <a href="<?php echo base_url('assets/documents/onboarding/Resources.pdf');?>" class="btn btn-danger" style="display:inline-block; margin-left:5px;" role="button" target="_blank">Download</a>
          <div class="checkbox">
            <label>
              <input id="cbResources" class="chkBox"  onclick="enableNext('cbResources', 'btnResources')" type="checkbox"> I acknowledge that I have read this entire document.
            </label>
          </div>
          <button class="btn btn-primary nextBtn pull-right" id="btnResources" type="button" disabled>Next</button>
        </div>

        <div class="setup-content animated fadeInUp" id="step-3">
          <h3 style="display:inline-block;">DDS Incident Reporting</h3>
          <a href="<?php echo base_url('assets/documents/onboarding/DDS.pdf');?>" class="btn btn-danger" style="display:inline-block; margin-left:5px;" role="button" target="_blank">Download</a>
          <div class="checkbox">
            <label>
              <input id="cbDDS" class="chkBox" onclick="enableNext('cbDDS', 'btnDDS')" type="checkbox"> I acknowledge that I have read this entire document.
            </label>
          </div>
          <button class="btn btn-primary nextBtn pull-right" id="btnDDS" type="button" disabled>Next</button>
          </div>

            <div class="panel panel-default setup-content animated fadeInUp" id="step-4">
              <div class="panel-heading" style="background-color:#D23C3C; color:white;">
                 <h3 class="panel-title">Change Password</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">New Password</label>
                    <input maxlength="100" type="password" required="required" id="password" name="password" class="form-control" placeholder="New Password" />
                </div>
                <div class="form-group">
                    <label class="control-label">Confirm New Password</label>
                    <input maxlength="100" type="password" required="required" id="cpassword" name="cpassword" class="form-control" placeholder="Confirm New Password" />
                </div>
                <br>
                <div class="checkbox">
                  <label>
                    <input name="skipPassword" class="skipPassword" type="checkbox"> Click here to keep same password.
                  </label>
                </div>
              <button class="btn btn-success pull-right" onclick="completeOnboarding()" type="button">Get Started</button>
            </div>
    </form>
    <br><br><br>
    <h5 class="text-center animated fadeInUp"><i>*All these documents can be found under the employees tab.</i></h5>
  </div>
    <script src='<?php echo base_url();?>assets/external/js/jquery.min.js'></script>
    <script src='<?php echo base_url();?>assets/external/js/bootstrap.min.js'></script>
    <script src='<?php echo base_url();?>assets/external/js/stepWizard.js'></script>
  </body>
</html>
