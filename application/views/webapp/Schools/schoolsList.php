<div class="container-fluid">
    <button class="btn btn-success pull-right" data-toggle="modal" data-target="#schoolAddModal" style="margin-top: 15px;">+ Add School</button>

    <div class="page-header">
        <h1 class="text-center" style="margin-top: 10px;">Schools</h1>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover datatable">
            <thead class="ywuHeader">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Options<br></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($schools as $school) { ?>
                <tr>
                    <td><?php echo $school['id']; ?></td>
                    <td><?php echo $school['name']; ?></td>
                    <td>
                      <button class="btn btn-warning" style="margin-right:5px;" onclick="loadEditModal('<?php echo $school['id']; ?>', '<?php echo $school['name']; ?>')">Edit</button>
                      <!-- Future update -->
                      <!-- <button class="btn btn-danger" onclick="loadEditModal('<?php echo $school['id']; ?>', '<?php echo $school['name']; ?>')">Related Participants</a>-->
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- Add Modal -->
    <div class="modal fade" id="schoolAddModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#D23C3C !important; color:white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title text-center">Add School</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label class="control-label" for="schoolName">Name
                                <br>
                            </label>
                            <input type="text" class="form-control" id="schoolNameAdd" placeholder="Enter School Name">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" onclick="addSchoolSave()">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="schoolEditModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#D23C3C !important; color:white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title text-center">Edit School</h4>
                </div>
                <div class="modal-body">
                    <div role="form">
                        <div class="form-group">
                            <label class="control-label" for="schoolName">Name
                                <br>
                            </label>
                            <input type="text" class="form-control" id="schoolNameEdit" placeholder="Enter School Name">
                        </div>
                    </div>
                    <input type="hidden" id="schoolIdEdit" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" onclick="editSchoolSave()">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>


        <script src="<?=base_url('assets/js/schools.js');?>"></script>
