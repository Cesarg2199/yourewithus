
        <style>


            .fc th {
              padding: 10px 0px;
              vertical-align: middle;
              background-color: #D23C3C;
              color: white;
            }
            .fc-day-grid-event>.fc-content {
                padding: 4px;
            }
            #calendar {
                max-width: 100%;
                margin: 0 auto;
            }
            .error {
                color: #ac2925;
                margin-bottom: 15px;
            }
            .event-tooltip {
                width:150px;
                background: rgba(0, 0, 0, 0.85);
                color:#FFF;
                padding:10px;
                position:absolute;
                z-index:10001;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                cursor: pointer;
                font-size: 11px;

            }
            .modal-header
            {
                background-color: #3A87AD;
                color: #fff;
            }

            a.fc-day-grid-event {
              background-color: #D23C3C;
              border-color: #D23C3C;
            }
        </style>
    <body>
    <div class="container-fluid">
        <div class="row" style="margin-top:15px;">
                <div class="form-group">
                    <select id="participant" name="participant" style="width:50%;"  class="form-control">
                      <option value="0">Select Participant</option>
                    <?php foreach($participants as $participant) { ?>
                    <option value="<?php echo $participant['id']; ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
                    <?php } ?>
                    </select>
                </div>

        </div>
    </div>

      <div class="container-fluid">
        <div id='calendar'></div>
      </div>


        <div class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #D23C3C">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title text-center"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="error"></div>
                        <form class="form-horizontal" id="crud-form">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="title">Title</label>
                                <div class="col-md-4">
                                    <input id="title" name="title" type="text" class="form-control input-md" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="description">Description</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" rows="8" id="description" name="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="description">Support</label>
                                <div class="col-md-4">
                                  <select id="support" name="support" class="form-control" style="width:100%;">
                                    <option value="0">None</option>
                                  <?php foreach($users as $user) { ?>
                                  <option value="<?php echo $user['id']; ?>"><?php echo $user['firstname']." ".$user['lastname']; ?></option>
                                  <?php } ?>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="title">Start Date</label>
                                <div class="col-md-4">
                                    <input id="sd" name="sd" type="text" class="form-control input-md" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="title">End Date</label>
                                <div class="col-md-4">
                                    <input id="ed" name="ed" type="text" class="form-control input-md" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
            <input type="hidden" id="siteUrl" value="<?php echo base_url();?>"
