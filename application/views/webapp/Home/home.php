<div class="container-fluid">
  <div class="page-header">
    <h1 class="text-center">Home</h1>
  </div>
  <?php if($this->session->roletitle == "Campus Case Coordinator") { ?>
  <h1>Connected Schools</h1>
  <div class="row">
      <div style="height:100px; width:100%; border: 1px solid #D23C3C">
        <?php
          if($schools) {
          foreach($schools as $school)  {
        ?>
          <div class="thumbnail" style="width:250px; display:inline-block; margin:15px; border-color:#D23C3C">
              <div class="caption">
                  <h4><?php echo $school['schoolName']; ?></h4>
              </div>
          </div>

      <?php } } ?>
      </div>
  </div>
  <?php } ?>
  <?php if($this->session->roletitle == "Region Administrator") { ?>
  <h1>Connected Schools</h1>
  <div class="row">
      <div style="height:100px; width:100%; border: 1px solid #D23C3C">
        <?php
          if($schools) {
          foreach($schools as $school)  {
        ?>
          <div class="thumbnail" style="width:250px; display:inline-block; margin:15px; border-color:#D23C3C">
              <div class="caption">
                  <h4><?php echo $school['schoolName']; ?></h4>
              </div>
          </div>

      <?php } } ?>
      </div>
  </div>
  <?php } ?>
  <?php if($this->session->roletitle != "Administrator") { ?>
  <h1>Participants
    <!-- CCC or RA Only -->
    <?php if(($this->session->roletitle == "Campus Case Coordinator") or ($this->session->roletitle == "Region Administrator")) { ?>
    <div class="btn-group">
      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Filter <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a onclick="filterSchool('All')">All</a></li>
        <?php
          if($schools) {
          foreach($schools as $school)  {
        ?>
          <li><a onclick="filterSchool('<?=$school['schoolName'];?>')"><?php echo $school['schoolName']; ?></a></li>
        <?php } } ?>
      </ul>
    </div>
  <?php } ?>
  </h1>

  <div class="row">
      <div style="height:425px; width:100%; border: 1px solid #D23C3C; overflow:auto">
          <?php
            if($participants) {
            foreach($participants as $participant) {
          ?>
          <div class="thumbnail" style="width:300px; display:inline-block; margin:15px; border-color:#D23C3C">
              <div class="caption">
                  <h4><?php echo $participant['firstname']." ".$participant['lastname']; ?></h4>
                  <p class="linkedSchool"><?php echo $participant['school'];?></p>
                  <div class="dropdown" style="display:inline;">
                      <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Info
                          <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="<?php echo site_url("Participants/intakeForm");?>?participantId=<?php echo $participant['id']; ?>">Intake Form</a>
                          </li>
                          <li>
                              <a href="<?php echo site_url("Participants/positiveExperienceDesign");?>?participantId=<?php echo $participant['id']; ?>">Positive Experince Design</a>
                          </li>
                          <li>
                              <a href="<?php echo site_url("Participants/scoutingReport");?>?participantId=<?php echo $participant['id']; ?>">Scouting Report</a>
                          </li>
                          <?php if(($this->session->role == "1") OR ($this->session->role == "7") OR ($this->session->role == "8") OR ($this->session->role == "2")) { ?>
                              <li><a href="<?php echo site_url("Participants/fileStore");?>?participantId=<?php echo $participant['id']; ?>">Files</a></li>
                          <?php } ?>
                      </ul>
                  </div>
                  <a href="<?php echo site_url('Participants/participantsProfile');?>?participantId=<?php echo $participant['id']; ?>" class="btn btn-primary" role="button">Stats</a>
                  <p></p>
              </div>
          </div>
        <?php } }?>
      </div>
  </div>
  <?php }?>
    <div class="row">
        <h2>Current Week Time Sheet</h2>
        <table class="table table-striped table-bordered" id="weekTotals">
            <thead class="ywuHeader">
                <tr>
                    <th>Living Skills (Hours)</th>
                    <th>Employment Skills (Hours)</th>
                    <th>Social Inclusion/Leisure (Hours)</th>
                    <th>Social Skills (Hours)</th>
                    <th>Administrative (Hours)</th>
                    <th>Interval Total (Hours)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php if($currentWeek['adultLivingTotal']['adultLiving']){ echo $currentWeek['adultLivingTotal']['adultLiving']; } else { echo "0"; } ; ?></td>
                    <td><?php if($currentWeek['employmentTotal']['employment']){ echo $currentWeek['employmentTotal']['employment']; } else { echo "0"; } ; ?></td>
                    <td><?php if($currentWeek['socialInclusionTotal']['socialInclusion']){ echo $currentWeek['socialInclusionTotal']['socialInclusion']; } else { echo "0"; } ; ?></td>
                    <td><?php if($currentWeek['socialTotal']['social']){ echo $currentWeek['socialTotal']['social']; } else { echo "0"; } ; ?></td>
                    <td><?php if($currentWeek['adminTotal']['admin']){ echo $currentWeek['adminTotal']['admin']; } else { echo "0"; } ; ?></td>
                    <td><?php if($currentWeek['intervalTotal']['intervalTime']){ echo $currentWeek['intervalTotal']['intervalTime']; } else { echo "0"; } ; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script src="<?=base_url('assets/js/home.js');?>"></script>
