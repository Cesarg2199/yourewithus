<div class="container">
  <div class="row">
    <ol class="breadcrumb" style="margin-top:15px;">
      <li><a href="<?=site_url('Settings')?>">Settings</a></li>
      <li class="active">Password</li>
    </ol>
  </div>
  <div class="page-header text-center">
      <h1><span class="glyphicon glyphicon-lock" style="position: inherit; vertical-align:middle"></span>&nbsp;Change Password</h1>
  </div>
    <?php echo form_open('Settings/changePassword', 'data-toggle="validator" '); ?>
        <div class="form-group">
            <label class="control-label" for="formInput3">New Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="New Password">
        </div>
        <div class="form-group">
            <label class="control-label" for="formInput4">Confirm New Password
                <br>
            </label>
            <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" data-match="#password" data-match-error="Whoops, these don't match" placeholder="Confirm New Password">
        </div>
        <p class="text-center"><i>*If this is your first time logging in, please change your password immediately</i></p>
        <button type="submit" id="btnUpdatePassword" class="btn btn-default pull-right btn-danger" disabled="true">Change Password</button>
      </form>
</div>
<script src="<?=base_url();?>assets/js/password.js"></script>
