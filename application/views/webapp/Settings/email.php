<div class="container">
  <div class="row">
    <ol class="breadcrumb" style="margin-top:15px;">
      <li><a href="<?=site_url('Settings')?>">Settings</a></li>
      <li class="active">Email</li>
    </ol>
  </div>
  <div class="page-header text-center">
            <h1><span class="glyphicon glyphicon-envelope" style="position: inherit; vertical-align:middle"></span>&nbsp;Change Login Email</h1>
        </div>
    <?php echo form_open('Settings/changeEmail', 'data-toggle="validator" '); ?>
        <div class="form-group">
            <label class="control-label" for="formInput1">New Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="New Email">
        </div>
        <div class="form-group">
            <label class="control-label" for="formInput2">Confirm New Email</label>
            <input type="email" class="form-control" id="confirmemail" name="confirmemail" data-match="#email" data-match-error="Whoops, these don't match" placeholder="Confirm New Email">
        </div>
        <button type="submit" id="btnUpdateEmail" class="btn btn-default pull-right btn-danger" disabled="true">Change Email</button>
    </form>
</div>
<script src="<?=base_url();?>assets/js/email.js"></script>
