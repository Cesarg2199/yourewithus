<div class="container" style="height:97.5%;">
  <div class="row">
    <ol class="breadcrumb" style="margin-top:15px;">
      <li><a href="<?=site_url('Settings')?>" class="active">Settings</a></li>
    </ol>
  </div>
    <div class="row">
        <div class="page-header text-center">
            <h1><span class="glyphicon glyphicon-cog" style="position: inherit; vertical-align:middle"></span>&nbsp;Account Settings</h1>
        </div>
        <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
            <input type="text" class="form-control" id="filter" placeholder="Search Settings">
        </div>
    </div>
    <br>
    <div class="row searchable">
        <!--<div class="col-sm-12 col-md-6">
            <h4><a href="<?php echo site_url("Settings/password"); ?>">Account Type</a></h4>
        </div>-->
        <?php if($this->session->type == "p") { ?>
        <div class="col-sm-12 col-md-6">
            <h4><a href="<?php echo site_url("Settings/currentSchool"); ?>">Change Current School</a></h4>
        </div>
       <?php } ?>
        <div class="col-sm-12 col-md-6">
            <h4><a href="<?php echo site_url("Settings/password"); ?>">Change Password</a></h4>
        </div>
        <div class="col-sm-12 col-md-6">
            <h4><a href="<?php echo site_url("Settings/email"); ?>">Change Login Email</a></h4>
        </div>
    </div>
</div>
<script>
function search()
{
  var rex = new RegExp($("#filter").val(), 'i');
  $('.searchable div').hide();
  $('.searchable div').filter(function () {
      return rex.test($(this).text());
  }).show();
}

$('#filter').keyup(function () {
  search();
});
</script>
