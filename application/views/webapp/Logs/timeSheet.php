<div class="container-fluid">
    <div class="page-header text-center">
        <h1>Time Sheet</h1>
    </div>
    <?php echo form_open('Logs/saveTimeSheet', array('id'=>'timeSheetForm', 'class' => 'form-horizontal')); ?>
    <div class="form-group">
      <label class="col-md-4 control-label" for="date">Date</label>
      <div class="col-md-4">
      <input id="date" name="date" type="text" placeholder="" value="<?php echo date('Y-m-d'); ?>" autocomplete="off" class="form-control input-md">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="participant">Participant</label>
      <div class="col-md-4">
        <select id="participant" name="participantId" class="form-control">
        <option value="">-</option>
        <?php foreach($participants as $participant) { ?>
        <option value="<?php echo $participant['id']; ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
        <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="">Time In</label>
      <div class="col-md-4">
      <input type="text" class="form-control time" id="timeIn" name="timeIn" placeholder="Enter time in" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="">Time Out</label>
      <div class="col-md-4">
      <input type="text" class="form-control time" id="timeOut" name="timeOut" placeholder="Enter time out" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="">Activity</label>
      <div class="col-md-4">
        <select id="activity" name="activity" class="form-control" onchange="timeDiff()" required>
            <option value="">-</option>
            <option value="Social">Social Skills</option>
            <option value="Adult Living">Living Skills</option>
            <option value="Employment">Employment Skills</option>
            <option value="Social Inclusion">Social Inclusion/Leisure</option>
            <option value="Administrative">Administrative</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for=""></label>
      <div class="col-md-4">
        <input type="button" onclick="submitTimeEntry()" class="btn btn-success pull-right" id="btnSave" value="Save">
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="dailyLogMessage" tabindex="-1" role="dialog" aria-labelledby="dailyLogMessageLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header ywuHeader">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="dailyLogMessageLabel">Alert</h4>
          </div>
          <div class="modal-body text-center">
            <h4>You can not submit a time sheet without submitting at least one <a href="<?php echo site_url('Logs'); ?>">daily log</a> today.</h4>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </div>
    </div>

</div>
<input type="hidden" value="<?=$this->session->userId;?>" id="userId">
<script src="<?=base_url('assets/js/time.js');?>"></script>
