<div class="container-fluid">
<div class="page-header">
  <h1 class="text-center">Daily Log</h1>
</div>
  <?php echo form_open('Logs/newDailyLog', array('class' => 'form-horizontal')); ?>
<fieldset>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="date">Date</label>
  <div class="col-md-4">
  <input id="date" name="date" type="text" placeholder="" value="<?php echo date('Y-m-d'); ?>" autocomplete="off" class="form-control input-md">
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="participant">Participant</label>
  <div class="col-md-4">
    <select id="participant" name="participant" class="form-control">
    <?php foreach($participants as $participant) { ?>
    <option value="<?php echo $participant['id']; ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
    <?php } ?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="activity">Activity</label>
  <div class="col-md-4">
  <input id="activity" name="activity" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="notes">Notes</label>
  <div class="col-md-4">
    <textarea class="form-control" rows="8" id="notes" name="notes"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-success pull-right">Save</button>
  </div>
</div>

</fieldset>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $( "#date" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });
});
</script>
