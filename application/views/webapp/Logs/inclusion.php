<div class="container">
        <div class="row">
            <div class="page-header">
                <h1 class="text-center">Inclusion Assessment Tool</h1>
            </div>
        </div>
        <?php echo form_open('Logs/newIA'); ?>
        <div class="form-group">
            <label class="control-label">Date</label>
            <input type="text" name="date" class="form-control" value="<?php echo date('Y-m-d h:i'); ?>">
        </div>
        <!-- Select Basic -->
        <div class="form-group">
          <label class="control-label" for="participant">Participant</label>
            <select id="participant" name="participant" class="form-control">
            <?php foreach($participants as $participant) { ?>
            <option value="<?php echo $participant['id']; ?>"><?php echo $participant['firstname']." ".$participant['lastname']; ?></option>
            <?php } ?>
            </select>
        </div>

        <h2 class="text-center">Physical Presence</h2>
        <div class="row">
            <div class="col-md-4">
                <p>Participant has plan for travel and arrives on time</p>
                <p>
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="pq1">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Participant is actively engaging with team/group
                    <br>
                </p>
                <p>
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="pq2">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>All of participants needs are met during &nbsp;daily activities
                    <br>
                </p>
                <p>
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="pq3">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Participant is comfortable to participate for full weekly schedule
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="pq4">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Participant is comfortable in buildings and meeting rooms
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="pq5">
            </div>
        </div>
        <h2 class="text-center">Social and Community</h2>
        <div class="row">
            <div class="col-md-4">
                <p>Participant feels welcomed and greeted upon arrival</p>
                <p>
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="sq1">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Team/group build rapport directly with participant</p>
                <p>
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="sq2">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Participant is confident to vocalize needs to team/group or campus coordinator</p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="sq3">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Team/group understand how participant best communicates and practice this understanding</p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="sq4">
            </div>
        </div>
        <h2 class="text-center">Administrative</h2>
        <div class="row">
            <div class="col-md-4">
                <p>Participant’s successes and obstacles are recorded and assessed regularly</p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="aq1">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Participant’s schedule is formalized and adjusted as needed based on participant’s weekly success</p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="aq2">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Participant can rely on team/group leader, one-on-one and ABA student to have their best interest in mind</p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="aq3">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Program uses confidentiality and protection when dealing with participant profiles
                    <br>
                </p>
            </div>
            <div class="col-md-8">
                <input type="number" class="form-control" name="aq4">
            </div>
        </div>
        <br/>
        <button type="submit" class="btn btn-default btn-success pull-right">Save</button>
        <div style="height:100px;"></div>
      </form>
    </div>
