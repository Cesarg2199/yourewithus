<div class="container-fluid">
  <div class="page-header">
    <h1 class="text-center">Expense Sheet</h1>
  </div>
  <?php echo form_open('Logs/newExpenseSheet', array('class' => 'form-horizontal')); ?>

  <div class="form-group">
    <label class="col-md-4 control-label" for="date">Date</label>
    <div class="col-md-4">
    <input id="date" name="date" type="text"  value="<?php echo date('Y-m-d h:i'); ?>" class="form-control" readonly>
    </div>
  </div>

  <div class="form-group">
    <label for="item" class="control-label col-md-4">Item</label>
    <div class="col-md-4">
      <input id="item" name="item" placeholder="Title" type="text" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label for="amount" class="control-label col-md-4">Amount</label>
    <div class="col-md-4">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="glyphicon glyphicon-usd"></i>
        </div>
      <input id="amount" name="amount" placeholder="$0.00" type="text" class="form-control">
    </div>
      <span class="help-block"><i>The amount is automatically formatted.</i></span>
    </div>
  </div>

  <div class="form-group">
    <label for="receipt" class="control-label col-md-4">Receipt</label>
    <div class="col-md-4">
      <label class="radio-inline">
        <input type="radio" name="receipt" value="y">
              Yes
      </label>
      <label class="radio-inline">
        <input type="radio" name="receipt" value="n" checked>
              No
      </label>
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label" for="notes">Notes</label>
    <div class="col-md-4">
      <textarea class="form-control" rows="8" id="notes" name="notes"></textarea>
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label" for=""></label>
    <div class="col-md-4">
      <button id="" name="" class="btn btn-success pull-right">Send</button>
    </div>
  </div>

  </form>
</div>

<script src="<?=base_url('assets/js/mask.js');?>"></script>
<script src="<?=base_url('assets/js/expenseSheet.js');?>"></script>
