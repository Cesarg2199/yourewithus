<div class="container">
    <div class="page-header text-center">
        <h1>Edit Task</h1>
    </div>
    <div class="row">
        <?php echo form_open('Tasks/reassignTask'); ?>
        <?php foreach($getTask as $task) { ?>
        <input type="hidden" name="taskId" value="<?=$task['id'];?>">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
              <label class="control-label" for="">Users</label>
              <select id="user" name="assignTo" class="form-control">
                <option value="<?=$task['assignedTo'];?>"><?=$task['assignedToName'];?></option>
                <option>None</option>
                <?php foreach($users as $user) { ?>
                <option value="<?php echo $user['id']; ?>"><?php echo $user['firstname']." ".$user['lastname']; ?></option>
                <?php } ?>
              </select>
          </div>
          <div class="form-group">
              <label class="control-label">Due Date</label>
              <input type="text" class="form-control" id="assignDue" name="assignDue" placeholder="Due Date" value="<?=$task['dateDue'];?>" required autocomplete="off">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label">Description</label>
                <textarea class="form-control" id="description" name="description" rows="5"><?=$task['description'];?></textarea>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="row">
      <button type="submit" class="btn pull-right btn-danger">Assign</button>
    </div>
      <?php echo form_close(); ?>
</div>
<script src="<?=base_url('assets/js/tasks.js');?>"></script>
