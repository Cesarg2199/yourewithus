<div class="container">
    <div class="page-header text-center">
        <h1>New Task</h1>
    </div>
    <?php echo form_open('Tasks/assignTask'); ?>
    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
              <label class="control-label" for="">Users</label>
              <select id="user" name="assignTo" class="form-control">
              <option>None</option>
              <?php foreach($users as $user) { ?>
              <option value="<?php echo $user['id']; ?>"><?php echo $user['firstname']." ".$user['lastname']; ?></option>
              <?php } ?>
              </select>
          </div>
          <div class="form-group">
              <label class="control-label">Due Date</label>
              <input type="text" class="form-control" id="assignDue" name="assignDue" placeholder="Due Date" required autocomplete="off">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label class="control-label">Description</label>
                <textarea class="form-control" id="description" name="description" rows="5"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
      <button type="submit" class="btn pull-right btn-danger">Assign</button>
    </div>
    <?php echo form_close(); ?>

    <!-- Notes Modal -->
	<div class="modal" id="notesModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header" style="color:white;">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-center">Notes</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="taskId" />
					<table class="table table-striped table-bordered table-condensed notesTable" >
						<thead style="background-color: #5cb85c; color:white;">
							<tr>
								<th class="col-md-2">Employee</th>
								<th class="col-md-7">Message</th>
								<th class="col-md-3">Date</th>
								<th class="col-md-1"></th>
							</tr>
						</thead>
						<tbody id="notesView">

						</tbody>
					</table>
					<br>
					<div role="form" style="width:100%;">
						<div class="form-group" style="width:100%">
							<div>
								<div>
									<textarea rows="4" cols="50" type="text" class="form-control" id="newNote" placeholder="Type Message" style="width:100%; resize:none;" maxlength="1000"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group" style="float:right;">
							<div>
								<div>
									<button type="button" class="btn btn-success" onclick="addNoteToTask()">Add</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- -->

</div>
<script src="<?=base_url('assets/js/tasks.js');?>"></script>
