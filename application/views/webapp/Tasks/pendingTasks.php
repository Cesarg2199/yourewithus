<div class="container-fluid">
    <div class="page-header text-center">
        <h1>Pending Tasks</h1>
    </div>
    <table class="table table-condensed table-responsive table-striped table-bordered datatable">
        <thead class="ywuHeader">
            <tr>
                <th>Task ID</th>
                <th>Assigned By</th>
                <th>Description</th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach($pendingTasks as $task) {?>
            <tr class="removeable-<?=$task['id'];?>">
                <td><?=$task['id'];?></td>
                <td><?=$task['assignedBy'];?></td>
                <td><?=$task['description'];?></td>
                <td><?=$task['dateDue'];?></td>
                <td><?=$task['status'];?></td>
                <td>
                    <button type="button" class="btn btn-warning" onclick="viewNotes('<?=$task['id'];?>')">Notes</button>
                    &nbsp;
                    <button type="button" class="btn btn-success" onmouseup="completedTask('<?=$task['id'];?>')">Complete</button>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<!-- Notes Modal -->
<div class="modal" id="notesModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #D23C3C; color:white;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title text-center">Notes</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" id="taskId" />
				<table class="table table-striped table-bordered table-condensed notesTable" >
					<thead style="background-color: #D23C3C; color:white;">
						<tr>
							<th class="col-md-2">User</th>
							<th class="col-md-7">Message</th>
							<th class="col-md-3">Date</th>
							<th class="col-md-1"></th>
						</tr>
					</thead>
					<tbody id="notesView">

					</tbody>
				</table>
				<br>
				<div role="form" style="width:100%;">
					<div class="form-group" style="width:100%">
						<div>
							<div>
								<textarea rows="4" cols="50" type="text" class="form-control" id="newNote" placeholder="Type Message" style="width:100%; resize:none;" maxlength="1000"></textarea>
							</div>
						</div>
					</div>
					<div class="form-group" style="text-align:right;">
						<button type="button" class="btn btn-danger" onclick="addNoteToTask()">Add</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- -->

<script src="<?=base_url('assets/js/tasks.js');?>"></script>
