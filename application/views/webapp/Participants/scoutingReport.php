<div class="container">
    <div class="page-header text-center">
        <h1>Scouting Report</h1>
    </div>
    <div class="row">
        <div class="dropdown pull-right">
            <button class="btn btn-info dropdown-toggle" type="button" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Options
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#" onclick="printScoutingReport('<?php echo $participantId; ?>')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp; Print</a></li>
            </ul>
        </div>
        <div class="dropdown pull-right">
          <button class="btn btn-success dropdown-toggle" type="button" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="margin-right:5px;">
          Forms
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="<?php echo site_url("Participants/intakeForm");?>?participantId=<?php echo $participantId; ?>">Intake Form</a></li>
            <li><a href="<?php echo site_url("Participants/positiveExperienceDesign");?>?participantId=<?php echo $participantId; ?>">Positive Experince Design</a></li>
            <li><a href="<?php echo site_url("Participants/scoutingReport");?>?participantId=<?php echo $participantId; ?>">Scouting Report</a></li>
          </ul>
        </div>
    </div>
    <?php echo form_open("Participants/saveScoutingReport"); ?>
    <input type="hidden" value="<?php echo $participantId; ?>" name="participantId" />
    <div class="row">
        <div class="col-md-3">
            <h3>Date</h3>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label class="control-label" for="formInput1">&nbsp;</label>
                <input type="text" class="form-control" id="dateM" name="dateM" placeholder="This will hold the last date modified" value="<?=$sr['date'];?>" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h3>Player</h3>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label class="control-label" for="formInput1">&nbsp;</label>
                <input type="text" class="form-control" name="name" placeholder="Player" value="<?=$participantInfo['firstname']." ".$participantInfo['lastname'];?>" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h3>Contact's Info</h3>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label class="control-label" for="">&nbsp;</label>
                <input type="text" class="form-control" name="contactInfo" value="<?=$sr['contactInfo'];?>" placeholder="Contact Info">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h3>Guardian's Cell Phone</h3>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label class="control-label" for="">&nbsp;</label>
                <input type="text" class="form-control" name="guardian" value="<?=$sr['guardian'];?>" placeholder="Guardian's Cell Phone">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label" for="">Personal</label>
            <textarea class="form-control" rows="8" name="personal"><?=$sr['personal']?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label" for="">School</label>
            <textarea class="form-control" rows="8" name="school"><?=$sr['school'];?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label" for="">Offense (Positive)</label>
            <textarea class="form-control" rows="8" name="offense"><?=$sr['offense'];?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="control-label" for="">Defense (Challenges we should be aware of)</label>
            <textarea class="form-control" rows="8" name="defense"><?=$sr['defense'];?></textarea>
        </div>

    </div>
    <div class="row">
        <h3>Keys to Success</h3>
    </div>
    <br>
    <div class="row">
        <input type="text" class="form-control" name="key1" value="<?=$sr['key1'];?>" placeholder="Key 1">
    </div>
    <br>
    <div class="row">
        <input type="text" class="form-control" name="key2" value="<?=$sr['key2'];?>" placeholder="Key 2">
    </div>
    <br>
    <div class="row">
        <input type="text" class="form-control" name="key3" value="<?=$sr['key3'];?>" placeholder="Key 3">
    </div>
    <div class="row text-center">
        <h3><b>You're With Us! Core Reminders</b></h3>
    </div>
    <div class="row">
        <p>1. Make time at the right time. Make sure someone is ready to meet <?=$participantInfo['firstname'];?> when he/she arrives and spend some time after activity is over.</p>
    </div>
    <div class="row">
        <p>2. Knowing his/her story. Always call <?=$participantInfo['firstname'];?> by name and be curious about his/her go. Put yourself in his/her shoes.</p>
    </div>
    <div class="row">
        <p>3. Believe <?=$participantInfo['firstname'];?> can succeed.</p>
    </div>
    <div class="row">
        <p>4. Praise <?=$participantInfo['firstname'];?>’s successes and strengths.</p>
    </div>
    <div class="row">
        <input type="submit" class="btn btn-success pull-right" style="margin-bottom:25px;" value="Save" />
    </form>
    </div>
</div>
