<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>You're With Us</title>
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
      <style>
        .row {
            margin: 10px 0px 0px 0px !important;
            padding: 0px !important;
          }
      </style>
    </head>
    <body>
      <div class="container">
        <img src="<?php echo base_url('assets/images/printheader.png');?>" width="200" height="75" style="padding:0px; margin-left:32%; margin-top:-25px;" class="center-block" />
        <div style="margin-top:-10px">
            <h4 class="text-center">Scouting Report</h4>
        </div>
         <div class="row">
             <div class="col-xs-3 col-sm-3">
                 <h5><b>Date</b></h5>
             </div>
             <div class="col-sm-9">
                 <h5><?=$sr['date'];?></h5>
             </div>
             <div class="col-xs-3 col-sm-3">
                 <h5><b>Player</b></h5>
             </div>
             <div class="col-sm-9">
                 <h5><?=$participantInfo['firstname']." ".$participantInfo['lastname'];?></h5>
             </div>
             <div class="col-xs-3 col-sm-3">
                 <h5><b>Contact's Info</b></h5>
             </div>
             <div class="col-sm-9">
                 <h5><?=$sr['contactInfo'];?></h5>
             </div>
             <div class="col-xs-3 col-sm-3">
                 <h5><b>Guardian's Cell Phone</b></h5>
             </div>
             <div class="col-sm-9">
                 <h5><?=$sr['guardian'];?></h5>
             </div>
         </div>
         <div class="row">
             <div class="col-xs-12 col-sm-12">
                 <h5><b>Personal</b></h5>
                 <h5><?php echo nl2br($sr['personal']);?></h5>
             </div>
         </div>
         <div class="row">
             <div class="col-xs-12 col-sm-12">
                 <h5><b>School</b></h5>
                 <h5><?php echo nl2br($sr['school']);?></h5>
             </div>
         </div>
         <div class="row">
             <div class="col-xs-12 col-sm-12">
                 <h5><b>Offense (Positive)</b></h5>
                 <h5><?php echo nl2br($sr['offense']);?></h5>
             </div>
         </div>
         <div class="row">
             <div class="col-xs-12 col-sm-12">
                 <h5><b>Defense (Challenges we should be aware of)</b></h5>
                 <h5><?php echo nl2br($sr['defense']);?></h5>
             </div>
         </div>
         <div class="row">
             <div class="col-xs-12 col-sm-12">
                 <h5><b>Keys To Success</b></h5>
             </div>
             <div class="col-xs-12 col-sm-12">
                 <h5>1. <?=$sr['key1'];?></h5>
             </div>
             <div class="col-xs-12 col-sm-12">
                 <h5>2. <?=$sr['key2'];?></h5>
             </div>
             <div class="col-xs-12 col-sm-12">
                 <h5>3. <?=$sr['key3'];?></h5>
             </div>
         </div>
         <div class="row text-center">
             <h3><b>You're With Us! Core Reminders</b></h3>
         </div>
         <div class="row">
             <p>1. Make time at the right time. Make sure someone is ready to meet <?=$participantInfo->firstname;?> when he/she arrives and spend some time after activity is over.</p>
         </div>
         <div class="row">
             <p>2. Knowing his/her story. Always call <?=$participantInfo->firstname;?> by name and be curious about his/her go. Put yourself in his/her shoes.</p>
         </div>
         <div class="row">
             <p>3. Believe <?=$participantInfo->firstname;?> can succeed.</p>
         </div>
         <div class="row">
             <p>4. Praise <?=$participantInfo->firstname;?>’s successes and strengths.</p>
         </div>
      </div>
    </body>
</html>
