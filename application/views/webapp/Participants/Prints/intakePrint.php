<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>You're With Us</title>
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
      <style>
        .row {
            margin: 10px 0px 0px 0px !important;
            padding: 0px !important;
          }
      </style>
    </head>
    <body>
      <div class="container">
          <img src="<?php echo base_url('assets/images/printheader.png');?>" width="200" height="75" style="padding:0px; margin-left:32%; margin-top:-25px;" class="center-block" />
          <div style="margin-top:-10px">
              <h4 class="text-center">Intake Form</h4>
          </div>

          <div class="row text-center">
              <h5><b>Screening Objectives &amp; Questions</b></h5>
          </div>

          <div class="row">
              <h5><b>Objectives:</b></h5>
              <ul>
                  <li>
                      <p>Determine feasibility of inclusion in YWU program; goal is to define if potential client is the right fit</p>
                  </li>
                  <li>
                      <p>Allow client to provide user-friendly feedback; goal is to provide data for best possible customized program, with ongoing improvement</p>
                  </li>
                  <li>
                      <p>Measure two critically important areas: depression and isolation, at both the start and during their participation; goal is to provide benchmark for insight and data</p>
                  </li>
                  <li>
                      <p>Define positive quality of life metrics that include (but are not limited to):</p>
                      <ul>
                          <li>Number and quality of friends</li>
                          <li>General happiness</li>
                          <li>Community participation</li>
                          <li>Feeling of self-worth</li>
                          <li>Sense of control/independence</li>
                      </ul>
                  </li>
              </ul>
          </div>

          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <h5><b>Name:</b> <?=$participantInfo['firstname']." ".$participantInfo['lastname'];?></h5>
              </div>
              <div class="col-sm-6">
                  <h5><b>Date:</b> <?=$intake['date'];?></h5>
              </div>
          </div>

          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>Questions:</b></h5>
              </div>
          </div>

          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>1. How would you best categorize your percentage of time spent alone:</b></h5>
                  <p><?=$intake['q1'];?></p>
              </div>
          </div>

          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>2. Name family members that you interact with on a regular basis and their relationship:</b></h5>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>a) <?=$intake['q2s1p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q2s1p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>b) <?=$intake['q2s2p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q2s2p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>c) <?=$intake['q2s3p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q2s3p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>d) <?=$intake['q2s4p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q2s4p2'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>3. Name friends that you interact with and how you met them:</b></h5>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>a) <?=$intake['q3s1p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q3s1p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>b) <?=$intake['q3s2p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q3s2p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>c) <?=$intake['q3s3p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q3s3p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>d) <?=$intake['q3s4p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q3s4p2'];?></p>
              </div>
          </div>
          <br><br><br>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>4. Name paid service providers you interact with and the services that they provide:</b></h5>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>a) <?=$intake['q4s1p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q4s1p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>b) <?=$intake['q4s2p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q4s2p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>c) <?=$intake['q4s3p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q4s3p2'];?></p>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <p>d) <?=$intake['q4s4p1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$intake['q4s4p1'];?></p>
              </div>
          </div>

          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>5. How would you best categorize the percentage of time spent with family/friends compared to
      service providers:</b></h5>
                  <p><?=$intake['q5'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>6. List how you most frequently spend your free time:</b></h5>
              </div>
              <div class="col-md-12">
                  <p>a) <?=$intake['q6p1'];?></p>
              </div>
              <div class="col-md-12">
                  <p>b) <?=$intake['q6p2'];?></p>
              </div>
              <div class="col-md-12">
                  <p>c) <?=$intake['q6p3'];?></p>
              </div>
              <div class="col-md-12">
                  <p>d) <?=$intake['q6p4'];?></p>
              </div>
              <div class="col-md-12">
                  <p>e) <?=$intake['q6p5'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>7. List activities would you most like to add to your free time:</b></h5>
              </div>
              <div class="col-md-12">
                  <p>a) <?=$intake['q7p1'];?></p>
              </div>
              <div class="col-md-12">
                  <p>b) <?=$intake['q7p2'];?></p>
              </div>
              <div class="col-md-12">
                  <p>c) <?=$intake['q7p3'];?></p>
              </div>
              <div class="col-md-12">
                  <p>d) <?=$intake['q7p4'];?></p>
              </div>
              <div class="col-md-12">
                  <p>e) <?=$intake['q7p5'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>8. List how you interact when meeting new people and large groups of people:</b></h5>
              </div>
              <div class="col-md-12">
                  <p>a) <?=$intake['q13p1'];?></p>
              </div>
              <div class="col-md-12">
                  <p>b) <?=$intake['q13p2'];?></p>
              </div>
              <div class="col-md-12">
                  <p>c) <?=$intake['q13p3'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>9. List activities from current list (# 6) that you would like to eliminate during your free time (if
      any):</b></h5>
              </div>
              <div class="col-md-12">
                  <p>a) <?=$intake['q8p1'];?></p>
              </div>
              <div class="col-md-12">
                  <p>b) <?=$intake['q8p2'];?></p>
              </div>
              <div class="col-md-12">
                  <p>c) <?=$intake['q8p3'];?></p>
              </div>
          </div>
          <br><br><br>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>10. List your main personality characteristics, and/or your limitations (physical/social):</b></h5>
              </div>
              <div class="col-md-12">
                  <p>a) <?=$intake['q14p1'];?></p>
              </div>
              <div class="col-md-12">
                  <p>b) <?=$intake['q14p2'];?></p>
              </div>
              <div class="col-md-12">
                  <p>c) <?=$intake['q14p3'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>11. List anything you would love to learn:</b></h5>
              </div>
              <div class="col-md-12">
                  <p>a) <?=$intake['q9p1'];?></p>
              </div>
              <div class="col-md-12">
                  <p>b) <?=$intake['q9p2'];?></p>
              </div>
              <div class="col-md-12">
                  <p>c) <?=$intake['q9p3'];?></p>
              </div>
              <div class="col-md-12">
                  <p>d) <?=$intake['q9p4'];?></p>
              </div>
              <div class="col-md-12">
                  <p>e) <?=$intake['q9p5'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>12. How often do you feel depressed:</b></h5>
                  <p><?=$intake['q10'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>13. How often do you feel isolated:</b></h5>
                  <p><?=$intake['q11'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>14. In the past 6 months have you:</b></h5>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt suicidal:</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s1'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt despondent</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s2'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt isolated</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s3'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt very anxious</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s4'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt overlooked</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s5'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt like you didn’t matter</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s6'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt joyful</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s7'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <p>Felt like part of a community</p>
              </div>
              <div class="col-sm-8">
                  <p><?php echo ($intake['q12s8'] == 'y') ? 'Yes' : 'No'; ?></p>
              </div>
          </div>
      </div>
    </body>
</html>
