<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>You're With Us</title>
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
      <style>
        .row {
            margin: 10px 0px 0px 0px !important;
            padding: 0px !important;
          }
      </style>
    </head>
    <body>
      <div class="container">
          <img src="<?php echo base_url('assets/images/printheader.png');?>" width="200" height="75" style="padding:0px; margin-left:32%; margin-top:-25px;" class="center-block" />
          <div style="margin-top:-10px">
              <h4 class="text-center">Positive Experience Design</h4>
          </div>
          <div class="row">
              <div class="col-xs-3 col-sm-3">
                  <h5><b>Name:</b> <?=$participantInfo['firstname']." ".$participantInfo['lastname'];?></h5>
              </div>
              <div class="col-xs-4 col-sm-4">
                  <h5><b>Date:</b> <?=$ped['date'];?></h5>
              </div>
              <div class="col-sm-5">
                  <h5><b>Age:</b> <?=$ped['age'];?></h5>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>Adult Goal(s)</b></h5>
              </div>
              <div class="col-xs-12 col-sm-12">
                  <p>1) <?=$ped['ag1'];?></p>
                  <p>2) <?=$ped['ag2'];?></p>
                  <p>3) <?=$ped['ag3'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <h5><b>Experience needed for each goal</b></h5>
              </div>
              <div class="col-sm-6">
                  <h5><b>Resources/Network</b></h5>
              </div>
          </div>
          <div class="row" style="margin-top:-5px;">
              <div class="col-xs-6 col-sm-6">
                  <p>1) <?=$ped['en1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$ped['enrn1'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <p>2) <?=$ped['en2'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$ped['enrn2'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <p>3) <?=$ped['en3'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$ped['enrn3'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <h5><b>Skills needed to reach each goal</b></h5>
              </div>
              <div class="col-sm-6">
                  <h5><b>Resources/Network</b></h5>
              </div>
          </div>
          <div class="row" style="margin-top:-5px;">
              <div class="col-xs-6 col-sm-6">
                  <p>1) <?=$ped['sn1'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$ped['snrn1'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <p>2) <?=$ped['sn2'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$ped['snrn2'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-6 col-sm-6">
                  <p>3) <?=$ped['sn3'];?></p>
              </div>
              <div class="col-sm-6">
                  <p><?=$ped['snrn3'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>Short Term Objectives (4 month chunks)</b></h5>
              </div>
              <div class="col-xs-12 col-sm-12">
                  <p>1) <?=$ped['sto1'];?></p>
                  <p>2) <?=$ped['sto2'];?></p>
                  <p>3) <?=$ped['sto3'];?></p>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12">
                  <h5><b>Long Term Objectives (1-5 years)</b></h5>
              </div>
              <div class="col-xs-12 col-sm-12">
                  <p>1) <?=$ped['lto1'];?></p>
                  <p>2) <?=$ped['lto2'];?></p>
                  <p>3) <?=$ped['lto3'];?></p>
              </div>
          </div>
        </div>
    </body>
</html>
