<div class="container-fluid" style="height:100%;">
            <?php if(($this->session->role == "1") or ($this->session->role == "4") or ($this->session->role == "7")) { ?>
            <a class="btn btn-success pull-right" style="margin-top: 15px;" href="<?php echo site_url('Participants/addParticipant'); ?>">+ Add Participant</a>
            <?php } ?>
            <div class="page-header">
                <h1 class="text-center" style="margin-top: 10px;">Participants</h1>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover datatable" style="min-height: 225px;">
                    <thead class="ywuHeader">
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>School</th>
                            <th>Group<br></th>
                            <th class="col-xs-3">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($participants as $participant) { ?>
                        <tr id="<?php echo $participant['id']; ?>">
                            <td><?php echo $participant['id']; ?></td>
                            <td><?php echo $participant['firstname']; ?></td>
                            <td><?php echo $participant['lastname']; ?></td>
                            <td><?php echo $participant['school']; ?></td>
                            <td><?php echo $participant['group']; ?></td>
                            <td>
                              <div class="dropdown" style="display:inline;">
                                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Info
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo site_url('Participants/participantsProfile');?>?participantId=<?php echo $participant['id']; ?>">Stats</a></li>
                                  <li><a href="<?php echo site_url("Participants/intakeForm");?>?participantId=<?php echo $participant['id']; ?>">Intake Form</a></li>
                                  <li><a href="<?php echo site_url("Participants/positiveExperienceDesign");?>?participantId=<?php echo $participant['id']; ?>">Positive Experince Design</a></li>
                                  <li><a href="<?php echo site_url("Participants/scoutingReport");?>?participantId=<?php echo $participant['id']; ?>">Scouting Report</a></li>
                                  <!-- Admin and mentor only view here -->
                                  <?php if(($this->session->role == "1") OR ($this->session->role == "7") OR ($this->session->role == "8") OR ($this->session->role == "2")) { ?>
                                  <div class="divider"></div>
                                  <li><a href="<?php echo site_url("Participants/fileStore");?>?participantId=<?php echo $participant['id']; ?>">Files</a></li>
                                  <?php } ?>
                                </ul>
                              </div>
                              <?php if(($this->session->role == "1") or ($this->session->role == "4")) { ?>
                                <div class="dropdown" style="display:inline;">
                                  <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Options
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('Participants/editParticipant');?>?participantId=<?php echo $participant['id']; ?>">Edit</a></li>
                                  </ul>
                                </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
