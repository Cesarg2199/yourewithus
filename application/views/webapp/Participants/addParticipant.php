<div class="container">
  <h1 class="text-center">Add Participant</h1>
  <div class="row">
    <?php echo form_open('Participants/insertParticipant', 'data-toggle="validator" '); ?>
    <div class="form-group ">
      <label class="control-label requiredField" for="firstname">
       First Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="firstname" name="firstname" placeholder="First name" type="text" required="requried" />
    </div>
    <div class="form-group ">
      <label class="control-label requiredField" for="lastname">
       Last Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="lastname" name="lastname" placeholder="Last name" type="text" required="requried" />
    </div>
    <div class="form-group ">
      <label class="control-label" for="school">School</label>
      <select id="school" class="form-control" name="school">
          <option value="none">None</option>
          <?php if($this->session->roletitle == "Administrator") { ?>
          <?php foreach($schools as $school) {?>
            <option value="<?php echo $school['name'];?>"><?php echo $school['name'];?></option>
          <?php
            } } else {
            foreach($schools as $school) {
          ?>
            <option value="<?php echo $school['schoolName'];?>"><?php echo $school['schoolName'];?></option>
          <?php } }?>
      </select>
    </div>
    <div class="form-group ">
      <label class="control-label requiredField" for="password">
       Group
      </label>
      <input class="form-control" id="group" name="group" placeholder="Group" type="text" />
    </div>
    <div class="form-group">
      <div>
        <button class="btn btn-success pull-right" name="submit" type="submit" style="margin-left:5px;">
        Add Participant
       </button>
      </div>
    </div>
    <?php if($this->session->role == 1) { ?>
    <div class="form-group">
      <div>
        <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#schoolAddModal">
         Quick Add School
        </button>
      </div>
    </div>
    <?php } ?>
    </form>
  </div>

  <div class="modal fade" id="schoolAddModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header" style="background-color:#D23C3C !important; color:white">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title text-center">Add School</h4>
              </div>
              <div class="modal-body">
                  <form role="form">
                      <div class="form-group">
                          <label class="control-label" for="schoolName">Name
                              <br>
                          </label>
                          <input type="text" class="form-control" id="schoolNameAdd" placeholder="Enter School Name">
                      </div>

                  </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-success" onclick="quickAddSchool()">Save</button>
              </div>
          </div>
      </div>
  </div>
</div>

<script src="<?=base_url('assets/js/participants.js');?>"></script>
