<div class="container">
    <div class="row">
        <div class="page-header text-center">
            <h1>Intake Form</h1>
        </div>
    </div>
    <div class="row">
      <div class="dropdown pull-right">
        <button class="btn btn-info dropdown-toggle" type="button" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Options
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="#" onclick="printIntake('<?php echo $participantId; ?>')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp; Print</a></li>
        </ul>
      </div>
      <div class="dropdown pull-right">
        <button class="btn btn-success dropdown-toggle" type="button" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="margin-right:5px;">
        Forms
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="<?php echo site_url("Participants/intakeForm");?>?participantId=<?php echo $participantId; ?>">Intake Form</a></li>
          <li><a href="<?php echo site_url("Participants/positiveExperienceDesign");?>?participantId=<?php echo $participantId; ?>">Positive Experince Design</a></li>
          <li><a href="<?php echo site_url("Participants/scoutingReport");?>?participantId=<?php echo $participantId; ?>">Scouting Report</a></li>
        </ul>
      </div>
    </div>
    <div class="row text-center">
      <h4><b>Screening Objectives &amp; Questions</b></h4>
    </div>
    <div class="row">
        <h2>Objectives:</h2>
        <ul>
            <li>
                <p>Determine feasibility of inclusion in YWU program; goal is to define if potential client is the right fit</p>
            </li>
            <li>
                <p>Allow client to provide user-friendly feedback; goal is to provide data for best possible customized program, with ongoing improvement</p>
            </li>
            <li>
                <p>Measure two critically important areas: depression and isolation, at both the start and during their participation; goal is to provide benchmark for insight and data</p>
            </li>
            <li>
                <p>Define positive quality of life metrics that include (but are not limited to):</p>
                <ul>
                    <li>Number and quality of friends</li>
                    <li>General happiness</li>
                    <li>Community participation</li>
                    <li>Feeling of self-worth</li>
                    <li>Sense of control/independence</li>
                </ul>
            </li>
        </ul>
    </div>
    <?php echo form_open('Participants/saveIntakeForm'); ?>
    <input type="hidden" value="<?php echo $participantId; ?>" name="participantId" />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="<?=$participantInfo['firstname']." ".$participantInfo['lastname'];?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="group">Last Date Modified</label>
                <input type="text" class="form-control" id="dateM" name="dateM" placeholder="This will hold the last date modified" value="<?=$intake['date'];?>" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <h2>Questions</h2>
    </div>
    <div class="row">
        <h4>1. How would you best categorize your percentage of time spent alone:</h4>
        <!-- Question  1-->
        <select class="form-control" name="q1">
            <?php if(!empty($intake['q1'])) { ?> <option value="<?=$intake['q1'];?>"><?=$intake['q1'];?></option> <?php } ?>
            <option value="">-</option>
            <option value="Almost all of the time 95%">Almost all of the time 95%</option>
            <option value="Most of the time 75%">Most of the time 75%</option>
            <option value="Half of the time 50%">Half of the time 50%</option>
            <option value="Not often 25%">Not often 25%</option>
            <option value="Almost never 5%">Almost never 5%</option>
        </select>
    </div>
    <br>
    <div class="row">
        <h4>2. Name family members that you interact with on a regular basis and their relationship:</h4>
        <!-- Question 2 -->
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Name</label>
                    <input type="text" class="form-control" name="q2s1p1" value="<?=$intake['q2s1p1'];?>" placeholder="Enter Family Member Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Relationship</label>
                    <input type="text" class="form-control" name="q2s1p2" value="<?=$intake['q2s1p2'];?>" placeholder="Enter Family Member Relationship">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Name</label>
                    <input type="text" class="form-control" name="q2s2p1" value="<?=$intake['q2s2p1'];?>" placeholder="Enter Family Member Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Relationship</label>
                    <input type="text" class="form-control" name="q2s2p2" value="<?=$intake['q2s2p2'];?>" placeholder="Enter Family Member Relationship">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Name</label>
                    <input type="text" class="form-control" name="q2s3p1" value="<?=$intake['q2s3p1'];?>" placeholder="Enter Family Member Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Relationship</label>
                    <input type="text" class="form-control" name="q2s3p2" value="<?=$intake['q2s3p2'];?>" placeholder="Enter Family Member Relationship">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Name</label>
                    <input type="text" class="form-control" name="q2s4p1" value="<?=$intake['q2s4p1'];?>" placeholder="Enter Family Member Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Family Member Relationship</label>
                    <input type="text" class="form-control" name="q2s4p2" value="<?=$intake['q2s4p2'];?>" placeholder="Enter Family Member Relationship">
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <h4>3. Name friends that you interact with and how you met them:</h4>
        <!-- Question 3 -->
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Friend's Name</label>
                    <input type="text" class="form-control" name="q3s1p1" value="<?=$intake['q3s1p1'];?>" placeholder="Enter Friend's Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">How They Met</label>
                    <input type="text" class="form-control" name="q3s1p2" value="<?=$intake['q3s1p2'];?>" placeholder="Enter How They Met">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Friend's Name</label>
                    <input type="text" class="form-control" name="q3s2p1" value="<?=$intake['q3s2p1'];?>" placeholder="Enter Friend's Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">How They Met</label>
                    <input type="text" class="form-control" name="q3s2p2" value="<?=$intake['q3s2p2'];?>" placeholder="Enter How They Met">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Friend's Name</label>
                    <input type="text" class="form-control" name="q3s3p1" value="<?=$intake['q3s3p1'];?>" placeholder="Enter Friend's Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">How They Met</label>
                    <input type="text" class="form-control" name="q3s3p2" value="<?=$intake['q3s3p2'];?>" placeholder="Enter How They Met">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Friend's Name</label>
                    <input type="text" class="form-control" name="q3s4p1" value="<?=$intake['q3s4p1'];?>" placeholder="Enter Friend's Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">How They Met</label>
                    <input type="text" class="form-control" name="q3s4p2" value="<?=$intake['q3s4p2'];?>" placeholder="Enter How They Met">
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <h4>4. Name paid service providers you interact with and the services that they provide:</h4>
        <!-- Question  4-->
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Paid Service Provider</label>
                    <input type="text" class="form-control" name="q4s1p1" value="<?=$intake['q4s1p1'];?>" placeholder="Enter Paid Service Provider">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Service They Provide</label>
                    <input type="text" class="form-control" name="q4s1p2" value="<?=$intake['q4s1p2'];?>" placeholder="Enter Service They Provide">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Paid Service Provider</label>
                    <input type="text" class="form-control" name="q4s2p1" value="<?=$intake['q4s2p1'];?>" placeholder="Enter Paid Service Provider">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Service They Provide</label>
                    <input type="text" class="form-control" name="q4s2p2" value="<?=$intake['q4s2p2'];?>" placeholder="Enter Service They Provide">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Paid Service Provider</label>
                    <input type="text" class="form-control" name="q4s3p1" value="<?=$intake['q4s3p1'];?>" placeholder="Enter Paid Service Provider">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Service They Provide</label>
                    <input type="text" class="form-control" name="q4s3p2" value="<?=$intake['q4s3p2'];?>" placeholder="Enter Service They Provide">
                </div>
            </div>
        </div>
        <div class="familyRow">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Paid Service Provider</label>
                    <input type="text" class="form-control" name="q4s4p1" value="<?=$intake['q4s4p1'];?>" placeholder="Enter Paid Service Provider">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="name">Service They Provide</label>
                    <input type="text" class="form-control" name="q4s4p2" value="<?=$intake['q4s4p2'];?>" placeholder="Enter Service They Provide">
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <h4>5. How would you best categorize the percentage of time spent with family/friends compared to
service providers:</h4>
        <!-- Question  5-->
        <select class="form-control" name="q5">
            <?php if(!empty($intake['q5'])) { ?> <option value="<?=$intake['q5'];?>"><?=$intake['q5'];?></option> <?php } ?>
            <option value="-">-</option>
            <option value="Much more with family/friends than service providers 90%/10% (fam f/svc pr)">Much more with family/friends than service providers 90%/10% (fam f/svc pr)</option>
            <option value="More with family/friends than with service providers 75%/25%">More with family/friends than with service providers 75%/25%</option>
            <option value="Somewhat equal 50%/50%">Somewhat equal 50%/50%</option>
            <option value="More with service providers than family/friends 25%/75%">More with service providers than family/friends 25%/75%</option>
            <option value="Much more with service providers than family/friends 10%/90%">Much more with service providers than family/friends 10%/90%</option>
        </select>
    </div>
    <br>
    <div class="row">
        <h4>6. List how you most frequently spend your free time:</h4>
        <!-- Question 6 -->
        <input type="text" class="form-control" name="q6p1" value="<?=$intake['q6p1'];?>" placeholder="A">
        <br>
        <input type="text" class="form-control" name="q6p2" value="<?=$intake['q6p2'];?>" placeholder="B">
        <br>
        <input type="text" class="form-control" name="q6p3" value="<?=$intake['q6p3'];?>" placeholder="C">
        <br>
        <input type="text" class="form-control" name="q6p4" value="<?=$intake['q6p4'];?>" placeholder="D">
        <br>
        <input type="text" class="form-control" name="q6p5" value="<?=$intake['q6p5'];?>" placeholder="E">

    </div>
    <br>
    <div class="row">
        <h4>7. List activities would you most like to add to your free time:</h4>
        <!-- Question 7 -->
        <input type="text" class="form-control" name="q7p1" value="<?=$intake['q7p1'];?>" placeholder="A">
        <br>
        <input type="text" class="form-control" name="q7p2" value="<?=$intake['q7p2'];?>" placeholder="B">
        <br>
        <input type="text" class="form-control" name="q7p3" value="<?=$intake['q7p3'];?>" placeholder="C">
        <br>
        <input type="text" class="form-control" name="q7p4" value="<?=$intake['q7p4'];?>" placeholder="D">
        <br>
        <input type="text" class="form-control" name="q7p5" value="<?=$intake['q7p5'];?>" placeholder="E">
    </div>
    <br>
    <div class="row">
        <h4>8. List how you interact when meeting new people and large groups of people:</h4>
        <!-- Question 13 -->
        <input type="text" class="form-control" name="q13p1" value="<?=$intake['q13p1'];?>" placeholder="A">
        <br>
        <input type="text" class="form-control" name="q13p2" value="<?=$intake['q13p2'];?>" placeholder="B">
        <br>
        <input type="text" class="form-control" name="q13p3" value="<?=$intake['q13p3'];?>" placeholder="C">
    </div>
    <br>
    <div class="row">
        <h4>9. List activities from current list (# 6) that you would like to eliminate during your free time (if any):</h4>
        <!-- Question 8 -->
        <input type="text" class="form-control" name="q8p1" value="<?=$intake['q8p1'];?>" placeholder="A">
        <br>
        <input type="text" class="form-control" name="q8p2" value="<?=$intake['q8p2'];?>" placeholder="B">
        <br>
        <input type="text" class="form-control" name="q8p3" value="<?=$intake['q8p3'];?>" placeholder="C">
    </div>
    <br>
    <div class="row">
        <h4>10. List your main personality characteristics, and/or your limitations (physical/social):</h4>
        <!-- Question 14 -->
        <input type="text" class="form-control" name="q14p1" value="<?=$intake['q14p1'];?>" placeholder="A">
        <br>
        <input type="text" class="form-control" name="q14p2" value="<?=$intake['q14p2'];?>" placeholder="B">
        <br>
        <input type="text" class="form-control" name="q14p3" value="<?=$intake['q14p3'];?>" placeholder="C">
    </div>
    <br>
    <div class="row">
        <h4>11. List anything you would love to learn:</h4>
        <!-- Question 9 -->
        <input type="text" class="form-control" name="q9p1" value="<?=$intake['q9p1'];?>" placeholder="A">
        <br>
        <input type="text" class="form-control" name="q9p2" value="<?=$intake['q9p2'];?>" placeholder="B">
        <br>
        <input type="text" class="form-control" name="q9p3" value="<?=$intake['q9p3'];?>" placeholder="C">
        <br>
        <input type="text" class="form-control" name="q9p4" value="<?=$intake['q9p4'];?>" placeholder="D">
        <br>
        <input type="text" class="form-control" name="q9p5" value="<?=$intake['q9p5'];?>" placeholder="E">
    </div>
    <br>
    <div class="row">
        <h4>12. How often do you feel depressed:</h4>
        <!-- Question 10 -->
        <select class="form-control" name="q10">
            <?php if(!empty($intake['q10'])) { ?> <option value="<?=$intake['q10'];?>"><?=$intake['q10'];?></option> <?php } ?>
            <option value="">-</option>
            <option value="Extremely often">Extremely often</option>
            <option value="Very often">Very often</option>
            <option value="Sometimes">Sometimes</option>
            <option value="Once in a while">Once in a while</option>
            <option value="Never">Never</option>
        </select>
    </div>
    <br>
    <div class="row">
        <h4>13. How often do you feel isolated:</h4>
        <!-- Question 11 -->
        <select class="form-control" name="q11">
            <?php if(!empty($intake['q11'])) { ?> <option value="<?=$intake['q11'];?>"><?=$intake['q11'];?></option> <?php } ?>
            <option value="">-</option>
            <option value="Extremely often">Extremely often</option>
            <option value="Very often">Very often</option>
            <option value="Sometimes">Sometimes</option>
            <option value="Once in a while">Once in a while</option>
            <option value="Never">Never</option>
        </select>
    </div>
    <br>
    <div class="row">
        <h4>14. In the past 6 months have you:</h4>
        <!-- Question 12 -->
        <div class="row">
            <div class="col-md-6">
                <h5>Felt suicidal</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s1" value="y" <?php if($intake['q12s1'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s1" value="n" <?php if($intake['q12s1'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt despondent</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s2" value="y" <?php if($intake['q12s2'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s2" value="n" <?php if($intake['q12s2'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt isolated</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s3" value="y" <?php if($intake['q12s3'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s3" value="n" <?php if($intake['q12s3'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt very anxious</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s4" value="y" <?php if($intake['q12s4'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s4" value="n" <?php if($intake['q12s4'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt overlooked</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s5" value="y" <?php if($intake['q12s5'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s5" value="n" <?php if($intake['q12s5'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt like you didn’t matter</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s6" value="y" <?php if($intake['q12s6'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s6" value="n" <?php if($intake['q12s6'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt joyful</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s7" value="y" <?php if($intake['q12s7'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s7" value="n" <?php if($intake['q12s7'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Felt like part of a community</h5>
            </div>
            <div class="col-md-6">
                <label class="control-label">
                    <input type="radio" name="q12s8" value="y" <?php if($intake['q12s8'] == 'y') { ?> checked="" <?php } ?>>Yes</label>
                &nbsp;
                <label class="control-label">
                    <input type="radio" name="q12s8" value="n" <?php if($intake['q12s8'] == 'n') { ?> checked="" <?php } ?>>No</label>
            </div>
        </div>
    </div>
    <br>
    <input type="submit" class="btn btn-success pull-right" style="margin-bottom:25px;" value="Save" />
  </form>
    <br>
</div>
