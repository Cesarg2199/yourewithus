<div class="container-fluid">
  <div class="page-header">
      <h1 class="text-center"><?=$participant['firstname'].' '.$participant['lastname'];?>'s Files</h1>
        </div>
  <div class="pull-right">
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#uploadModal">Upload File&nbsp;&nbsp;<span class="glyphicon glyphicon-upload" aria-hidden="true"></span></button>
  </div>
  <h2>Total Files: <span id="fileAmount"><?=$amount;?></span></h2>
  <br>
  <div class="input-group">
   <input type="text" class="form-control" placeholder="Search Files..." id="filter">
   <span class="input-group-btn">
        <button class="btn btn-warning" type="button" onclick="search()"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;&nbsp;Search</button>
   </span>
</div>
  <br>
  <div class="row searchable">
    <?php foreach($files as $file) {   ?>
    <div class="col-md-2" id="file<?=$count;?>" style="width:21%;">
        <div class="thumbnail">
            <div class="caption" style="height:125px;">
                <p class="text-center" style="overflow-wrap:break-word;"><?=$file;?></p>
                <p class="text-center">
                  <?php if (in_array(substr($file, -4), $allowed)) {  ?> <a href="<?php echo site_url('participantFiles');?><?php echo "/".$participant['id']. "/".$file ; ?>" class="btn btn-primary" target="_blank">View</a>  <?php } ?>
                  <a href="<?php echo site_url('participantFiles');?><?php echo "/".$participant['id']. "/".$file ; ?>" class="btn btn-warning" download>Download</a>
                  <button class="btn btn-default btn-danger" onclick="deleteFile('<?=$file;?>', '<?=$count;?>')">Delete</button>
                </p>
            </div>
        </div>
    </div>
    <?php $count++;  } ?>

    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center ywuHeader">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-toggle="modal">×</button>
                    <h4 class="modal-title">Quick Upload Files</h4>
                </div>
                <div class="modal-body">
                  <h2 class="text-center">Drop Files to Upload</h2>
                  <div>
                      <?php echo form_open_multipart('Participants/saveFile', array('class' => 'dropzone')); ?>

                      </form>
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

  </div>
</div>

<input type="hidden" id="participantId" value="<?=$participant['id'];?>">
<script src='<?php echo base_url();?>assets/js/files.js'></script>
