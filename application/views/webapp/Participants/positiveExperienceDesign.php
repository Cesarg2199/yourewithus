<div class="container">
    <div class="page-header text-center">
        <h1>Positive Experience Design</h1>
    </div>
    <div class="row">
        <div class="dropdown pull-right">
            <button class="btn btn-info dropdown-toggle" type="button" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Options
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#" onclick="printPED('<?php echo $participantId; ?>')"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp; Print</a></li>
            </ul>
        </div>
        <div class="dropdown pull-right">
          <button class="btn btn-success dropdown-toggle" type="button" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="margin-right:5px;">
          Forms
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="<?php echo site_url("Participants/intakeForm");?>?participantId=<?php echo $participantId; ?>">Intake Form</a></li>
            <li><a href="<?php echo site_url("Participants/positiveExperienceDesign");?>?participantId=<?php echo $participantId; ?>">Positive Experince Design</a></li>
            <li><a href="<?php echo site_url("Participants/scoutingReport");?>?participantId=<?php echo $participantId; ?>">Scouting Report</a></li>
          </ul>
        </div>
    </div>
    <?php echo form_open("Participants/savePositiveExperienceDesign"); ?>
    <input type="hidden" value="<?php echo $participantId; ?>" name="participantId" />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="<?=$participantInfo['firstname']." ".$participantInfo['lastname'];?>" readonly="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="group">Date</label>
                <input type="text" class="form-control" id="dateM" name="dateM" placeholder="This will hold the last date modified" value="<?=$ped['date'];?>" readonly>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
          <div class="form-group">
              <label class="control-label" for="group">Age</label>
              <input type="text" class="form-control" name="age" placeholder="Age" value="<?=$ped['age'];?>">
          </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Adult Goals</h5>
            <input type="text" class="form-control" name="ag1" value="<?=$ped['ag1'];?>" placeholder="Goal 1">
            <br>
            <input type="text" class="form-control" name="ag2" value="<?=$ped['ag2'];?>" placeholder="Goal 2">
            <br>
            <input type="text" class="form-control" name="ag3" value="<?=$ped['ag3'];?>" placeholder="Goal 3">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <h5>Experience needed for each goal</h5>
            <input type="text" class="form-control" name="en1" value="<?=$ped['en1'];?>" placeholder="1.">
            <br>
            <input type="text" class="form-control" name="en2" value="<?=$ped['en2'];?>" placeholder="2.">
            <br>
            <input type="text" class="form-control" name="en3" value="<?=$ped['en3'];?>" placeholder="3.">

        </div>
        <div class="col-md-6">
            <h5>Resources/Network</h5>
            <input type="text" class="form-control" name="enrn1" value="<?=$ped['enrn1'];?>" placeholder="1.">
            <br>
            <input type="text" class="form-control" name="enrn2" value="<?=$ped['enrn2'];?>" placeholder="2.">
            <br>
            <input type="text" class="form-control" name="enrn3" value="<?=$ped['enrn3'];?>" placeholder="3.">

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <h5>Skills needed to reach each goal</h5>
            <input type="text" class="form-control" name="sn1" value="<?=$ped['sn1'];?>" placeholder="1.">
            <br>
            <input type="text" class="form-control" name="sn2" value="<?=$ped['sn2'];?>" placeholder="2.">
            <br>
            <input type="text" class="form-control" name="sn3" value="<?=$ped['sn3'];?>" placeholder="3.">

        </div>

        <div class="col-md-6">
            <h5>Resources/Network</h5>
            <input type="text" class="form-control" name="snrn1" value="<?=$ped['snrn1'];?>" placeholder="1.">
            <br>
            <input type="text" class="form-control" name="snrn2" value="<?=$ped['snrn2'];?>" placeholder="2.">
            <br>
            <input type="text" class="form-control" name="snrn3" value="<?=$ped['snrn3'];?>" placeholder="3.">

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <h5>Short Term Objectives (4 month chunks)</h5>
            <input type="text" class="form-control" name="sto1" value="<?=$ped['sto1'];?>" placeholder="Goal 1">
            <br>
            <input type="text" class="form-control" name="sto2" value="<?=$ped['sto2'];?>" placeholder="Goal 2">
            <br>
            <input type="text" class="form-control" name="sto3" value="<?=$ped['sto3'];?>" placeholder="Goal 3">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <h5>Long Term Objectives (1-5 years)</h5>
            <input type="text" class="form-control" name="lto1" value="<?=$ped['lto1'];?>" placeholder="Goal 1">
            <br>
            <input type="text" class="form-control" name="lto2" value="<?=$ped['lto2'];?>" placeholder="Goal 2">
            <br>
            <input type="text" class="form-control" name="lto3" value="<?=$ped['lto3'];?>" placeholder="Goal 3">
        </div>
    </div>
    <br>
    <input type="submit" class="btn btn-success pull-right" style="margin-bottom:25px;" value="Save">
  </form>
</div>
