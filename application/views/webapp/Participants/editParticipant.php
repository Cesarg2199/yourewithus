<div class="container">
<h1 class="text-center">Edit Participant</h1>
  <div class="row">
    <?php echo form_open('Participants/updateParticipant', 'data-toggle="validator" '); ?>
    <?php foreach($participant as $info) ?>
    <input type="hidden" name="id" value="<?php echo $info['id']; ?>" />
     <div class="form-group ">
      <label class="control-label requiredField" for="firstname">
       First Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="firstname" name="firstname" placeholder="First name" type="text" value="<?php echo $info['firstname']; ?>" required="requried"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="lastname">
       Last Name
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="lastname" name="lastname" placeholder="Last name" type="text" value="<?php echo $info['lastname']; ?>" required="requried"/>
     </div>
     <div class="form-group ">
       <label class="control-label" for="school">School</label>
       <select id="school" class="form-control" name="school">
           <option value="<?php echo $info['school']; ?>"><?php echo $info['school'];?></option>
           <option disabled>-</option>
           <option value="none">None</option>
           <?php foreach($schools as $school) {?>
             <option value="<?php echo $school['name'];?>"><?php echo $school['name'];?></option>
           <?php } ?>
       </select>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="group">
       Group
      </label>
      <input class="form-control" id="group" name="group" placeholder="Group" value="<?php echo $info['group']; ?>" type="text"/>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-warning pull-right" name="submit" type="submit" style="margin-left:5px;">
        Update Participant
       </button>
      </div>
     </div>
     <?php if($this->session->role == 1) { ?>
     <div class="form-group">
       <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#schoolAddModal">
        Quick Add School
       </button>
     </div>
     <?php } ?>
    </form>
  </div>
  <div class="modal fade" id="schoolAddModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header" style="background-color:#D23C3C !important; color:white">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title text-center">Add School</h4>
              </div>
              <div class="modal-body">
                  <form role="form">
                      <div class="form-group">
                          <label class="control-label" for="schoolName">Name
                              <br>
                          </label>
                          <input type="text" class="form-control" id="schoolNameAdd" placeholder="Enter School Name">
                      </div>

                  </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-success" onclick="quickAddSchool()">Save</button>
              </div>
          </div>
      </div>
  </div>
 </div>
<script src="<?=base_url('assets/js/participants.js');?>"></script>
