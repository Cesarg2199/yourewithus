<div class="container">
          <div class="page-header text-center" data-pg-collapsed>
              <h1>Mailing List</h1>
          </div>
    <div class="row">
      <div class="col-md-3">
        <h4 id="total" style="color:#D23C3C">Total Subscribers: <?php echo $subsList; ?></h4>
      </div>
      <div class="col-md-3">
        <h4 id="totalNew" style="color:#D23C3C">Total New Subscribers: <?php echo $newSubsList; ?></h4>
      </div>
      <!--<div class="col-md-3">
        <div class="dropdown">
          <button class="btn btn-warning dropdown-toggle col-xs-12" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Filter
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a onclick="viewAll();">View All</a></li>
            <li><a onclick="hideExported();">View New Only</a></li>
          </ul>
          </div>
      </div>-->
      <div class="col-md-3">
        <div class="dropdown">
          <button class="btn btn-success dropdown-toggle col-xs-12" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Options
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a id="exportNew" href="<?php echo site_url('Subscribers/exportSubs'); ?>" <?php if($subsList == 0){ echo 'disabled="true"'; }?>>Export New</a></li>
            <li><a id="exportAll" href="<?php echo site_url('Subscribers/exportAllSubs'); ?>">Export All</a></li>
          </ul>
          </div>
      </div>
    </div>
    <br />
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover datatable">
                <thead class="ywuHeader">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th class="col-xs-8">Email</th>
                        <th>Exported</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach($subs as $sub) { ?>
                    <tr class="<?php echo $sub['exported']; ?>">
                        <td><?php echo $sub['firstname']; ?></td>
                        <td><?php echo $sub['lastname']; ?></td>
                        <td><?php echo $sub['email']; ?></td>
                        <td><?php echo $sub['exported']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#exportNew").click(function(){
  makeZero();
});

$("#exportAll").click(function(){
  makeZero();
});

function makeZero()
{
  $("#totalNew").html("Total New Subscribers: 0");
}

function hideExported()
{
  $('.y').hide();
}

function viewAll()
{
  $('.y').show();
}

</script>
