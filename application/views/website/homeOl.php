<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>You're With Us!</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/website.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" integrity="sha256-HAaDW5o2+LelybUhfuk0Zh2Vdk8Y2W2UeKmbaXhalfA=" crossorigin="anonymous"/>
    </head>
    <body>
        <nav id="headliner" class="navbar navbar-expand bg-ywu navbar-dark fixed-top justify-content-center">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="https://www.facebook.com/urwus" class="fa fa-logo fa-facebook" target="_blank"></a>
                    <a href="https://twitter.com/youre_with_us" class="fa fa-logo fa-twitter" target="_blank"></a>
                    <a href="https://www.instagram.com/youre_with_us/" class="fa fa-logo fa-instagram" target="_blank"></a>
                    <a href="https://www.linkedin.com/company/you-re-with-us/" class="fa fa-logo fa-linkedin" target="_blank"></a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" href="#">|</span>
                </li>
                <li class="nav-item">
                    <a class="btn btn-warning" href="https://yourewithus.networkforgood.com/projects/21691-inclusion-is-not-coming-and-knocking-on-your-door" target="_blank">Donate</a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" href="#">|</span>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="https://docs.google.com/forms/d/e/1FAIpQLSfoUWiTE_YDWqDYKpb7EcqG6lNIVkXVw4M_UPV66L3f7CA_4w/viewform" target="_blank">Register</a>
                </li>
            </ul>
        </nav>
        <div class="justify-content-center d-flex cBanner bg-dark">
            <a href="https://www.cummingsfoundation.org/oneworldboston/grant_recipients.htm" target="_blank">
            <img src="assets/images/misc/100k.jpg" class="img-fluid"></a>
        </div>
        <nav class="navbar navbar-expand-sm navbar-light bg-white headlinerAdjust">
            <ul class="nav navbar-nav abs-center-x">
                <a class="navbar-brand" href="#">
                    <img id="navbarLogo" src="assets/images/misc/logo.jpg" alt="">
                </a>
            </ul>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler3" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon ywuHeader"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler3">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#testimonials">Testimonials</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#board">Board</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('Login'); ?>">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron d-flex justify-content-center align-items-center">
            <div id="mainBannerOverlay">
                <h1 id="mainBanner" class="display-3">Don't worry, <i class="text-ywu">You're With Us!</i></h1>
                <a id="mainBtn" class="btn btn-ywu btn-lg" href="https://youtu.be/GfCHQhKi2Yo" data-toggle="lightbox">Play Video&nbsp;<i class="fa fa-play-circle"></i></a>
            </div>
        </div>
        <div class="container">
            <div class="row sectionContent">
                <h1 class="font-weight-bolder ywuHeader">Our Mission Statement</h1>
                <p class="lead">
                    You’re With Us! is a non-profit 501(c)(3) organization and a Department of Developmental Services (DDS) service provider that creates inclusion opportunities for young adults with disabilities. The program identifies and trains college clubs, groups, and teams to welcome individuals with disabilities into their groups as they are. You’re With Us! believes that a meaningful life includes a home, a job, family, friends and social opportunities with their peers - able and otherwise. </p>
                <div id="rmMissionStatement" style="display:none;">
                    <p class="lead">People with disabilities often feel different and excluded from normal social opportunities. You’re With Us! aids these young adults in belonging to a group and receiving the benefits young people would typically receive from inclusive opportunities. Benefits of this inclusion flow in both directions. People with disabilities have the potential to influence those around them -- a special ability to open people up to compassion, empathy, and leadership. Further, You’re With Us! seeks to change the perceptions of disabled young people who believe they are separate and excluded by helping them become part of the communities centered on the activities that they enjoy. In turn, their new peers in these communities, who may have thought of children with disabilities as different, now find them to be friends, teammates, and even family.</p>
                    <p class="lead">All people are healthiest when they feel safe, supported, and connected to others in their neighborhoods and communities. The inclusion of people with disabilities in these communities provides the essential social skills and leadership tools to allow them to lead a full life.</p>
                </div>
                <div class="col-12 text-right">
                    <button type="button" id="btnMissionStatement" class="btn btn-light btn-outline-danger rounded-lg">Read
                        <span class="more">More +</span>
                        <span class="less" style="display:none;">Less -</span>
                    </button>
                </div>
            </div>
            <div class="row rowAlt sectionContent">
                <div class="sectionRight">
                    <h1 class="d-block w-100 font-weight-bolder">Testimonials</h1>
                </div>
                <div id="testimonials" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#testimonials" data-slide-to="0" class="active"></li>
                        <li data-target="#testimonials" data-slide-to="1"></li>
                        <li data-target="#testimonials" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="d-block w-100" alt="First slide">
                                <h5>"Most recently, through Pete’s (Nick's mentor) enthusiasm and advocacy, Nick has been able to audit the Perspective on a Coaching course at SSU! Of course, Nick is thrilled to be in a typical college class, learning about a topic he is passionate about with some of his peers from the baseball team! Pete’s instinct that Coach Passinari would be a good instructional match for Nick was spot on. Not only is Nick learning new things about coaching, he is a truly motivated learner with this motivation spilling over into other areas."</h5>
                                <p>- Jodi Jarvis, YWU Participant Parent</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="d-block w-100" alt="Second slide">
                                <h5>“YWU! is an easy way to change lives through inclusion. It blows my mind how separated people who have disabilities are from the general population. Sports have always introduced me to some of my favorite and closest friends – why not open that door to someone else whom would really benefit from it?”</h5>
                                <p>- Cat S., Northeastern University Girls Soccer</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="d-block w-100" alt="Third slide">
                                <h5>“I think the YWU! program is a great learning experience for both my mentee Nick and I. I think it’s expanding both of our worlds for the better. I plan on keeping in touch with Nick and the program out of season. We have a routine of getting lunch every Tuesday.”</h5>
                                <p>- Peter G., Salem State University</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sectionContent">
                <div class="col-12">
                    <h1 class="font-weight-bolder ywuHeader">Schools</h1>
                </div>
                <div id="schoolsWall">
                    <img src="assets/images/schools/bc.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/bentley.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/bran.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/cu.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/ec.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/gc.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/harvard.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/lhs.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/mc.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/mit.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/neu.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/shc.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/sjp.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/ssu.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/uml.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/uri.png" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/wsc.jpg" class="img-fluid" alt="Responsive image">
                    <img src="assets/images/schools/ww.jpg" class="img-fluid" alt="Responsive image">
                </div>
            </div>
            <div class="row rowAlt sectionContent">
                <div class="sectionCenter">
                    <h1 class="d-block w-100 font-weight-bolder">Featured Supporters</h1>
                </div>
                <br/>
                <br/>
                <br/>
                <div class="col-md-6">
                    <div class="card">
                        <a target="_blank" href="https://www.cummingsfoundation.org/index.htm">
                            <div class="card-body d-flex align-items-center justify-content-around supporter">
                                <img src="assets/images/supporters/cfLogo.png" class="img-fluid full">
                            </div>
                            <a/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <a target="_blank" href="http://www.flutiefoundation.org/">
                            <div class="card-body d-flex align-items-center justify-content-center supporter">
                                <img src="assets/images/supporters/ffLogo.jpg">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row sectionContent">
                <div class="sectionRight">
                    <h1 class="d-block w-100 font-weight-bolder ywuHeader">Our Vision</h1>
                </div>
                <p class="lead">
                    You’re With Us! creates inclusion opportunities for young adults with disabilities. The program identifies and trains college clubs, groups, and teams to be welcoming and supportive peers to the differently abled. The college group then creates a Positive Experience Design (PED) customized for the young adult to help him or her transition from school to an adult life in the community. You’re With Us! believes that a meaningful life includes a home, a job, family, friends and social opportunities with their peers - able and otherwise.</p>
            </div>
            <div class="row rowAlt sectionContent">
                <h1 class="d-block w-100 font-weight-bolder">Our History</h1>
                <br/>
                <p class="lead">
                    You’re With Us! creates inclusion opportunities for young adults with disabilities. The program identifies and trains college clubs, groups, and teams to be welcoming and supportive peers to the differently abled. The college group then creates a Positive Experience Design (PED) customized for the young adult to help him or her transition from school to an adult life in the community. You’re With Us! believes that a meaningful life includes a home, a job, family, friends and social opportunities with their peers - able and otherwise.</p>
                <div id="rmHistory" style="display:none">
                    <p class="lead">January 3, 2000 A few months later, as a first-time head men’s basketball coach at Salve Regina University, I was readying my team for practice. Minutes before we were to start, I found out my son Max had been diagnosed with Cerebral Palsy. Max was nine months old and I was five months into my head coaching career. Standing before a group of fit and able-bodied young men, I started my usual practice warm-up address. But that day was different. As I began, I informed my team of Max’s diagnosis. And I began to cry. Many players began to tear up with me. After a couple of minutes, I told them we needed to have a good practice because Max would never have a chance to have a good practice. Max became an inspiration to my teams for the next six years. The players viewed Max as their teammate. He is present in every championship photo because the players insisted he be. When I left coaching – and for the eight years that followed –Max had no opportunities to be included in groups of able-bodied people. He withdrew and his physical condition worsened. That changed in 2013 when Max was welcomed onto the Northeastern University’s men’s basketball team. A new group of players welcomed him as their teammate. I had my son back. His life changed – and so did theirs.</p>
                </div>
                <div class="col-12 text-right">
                    <button type="button" id="btnHistory" class="btn rounded-lg btn-outline-light">Read
                        <span class="more">More +</span>
                        <span class="less" style="display:none;">Less -</span>
                    </button>
                </div>
            </div>
            <div class="row sectionContent">
                <div class="sectionRight">
                    <h1 class="d-block w-100 font-weight-bolder ywuHeader">The Benefits</h1>
                </div>
                <p class="lead">
                    It has been said a person's quality of life is directly linked to the quality of the relationships in their life. You’re With Us! provides adolescents/young adults with the unique opportunity to belong to a group--just like any person would or could--- and derive the typical benefits people receive from out-of-school activities. In addition, our participants have the unique ability to profoundly affect those around them. By being included in their groups, our participants open up college students---and college campuses---to compassion and empathy, and provide them with leadership opportunities that would otherwise not be available. In essence, our framework enables our participants and peer mentors to “practice together” in the non-disabled community, which is the setting where they will be expected to function and thrive for the balance of their lives. </p>
            </div>
            <div class="row rowAlt sectionContent">
                <h1 class="d-block w-100 font-weight-bolder">The Program</h1>
                <br/>
                <p class="lead">
                    We locate possible opportunities to join college groups, teams, and clubs; coordinate recruitment into the group; act as the liaison between the family, school, and college/university; train mentors of the college groups to be responsible for making sure participants are included and connected with the group, facilitate communication between all parties; facilitate group education and development of Positive Experience Design; gather data on impact and quality of life; create and coordinate social connections across campus for participants and families; facilitate possible job/career opportunities through college career resources; offer support services as needed (ABA, BCBA, Monitor); create systems and manuals for future schools; and generally problem solve. </p>
            </div>
            <div class="row sectionContent">
                <div class="sectionCenter">
                    <h1 class="d-block w-100 font-weight-bolder ywuHeader">Board of Directors</h1>
                    <p>Our board is committed to helping people with disabilities have a life in the community which includes a sense of belonging to a group. We have leaders in the youth development and special needs transition space helping coach our participants, families
    and college students to successful outcomes.</p>
                </div>
                <div class="row text-center full">
                    <div class="col-12 founderBoard">
                        <img class="img-fluid rounded boardAdjust" src="assets/images/board/michael.jpg">
                        <h3>Michael Plansky</h3>
                        <p>Founder/President You’re With Us!</p>
                    </div>
                </div>
                <div class="row text-center full">
                    <div class="col-md-3">
                        <img src="assets/images/board/davidPliner.jpg" class="img-fluid rounded boardAdjust">
                        <h3>David Pliner</h3>
                        <p>President Infusion Ventures</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/board/sunny.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Sunny Stich</h3>
                        <p>Non-profit development professional</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/board/morgan.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Morgan Krush</h3>
                        <p>Clinical research coordinator at Dana-Farber Cancer Institute</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/board/sean.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Sean Stellato</h3>
                        <p>NFL Player Agent, Author of No Backing Down</p>
                    </div>
                </div>
                <div id="boardSplit"></div>
                <div class="row text-center full">
                    <div class="col-md-3 text-center">
                        <img src="assets/images/board/peter.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Peter Doliber</h3>
                        <p>Executive Director at Alliance of Massachusetts YMCAs.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/board/dan.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Dan Lebowitz</h3>
                        <p>Executive Director The Center for the Study of Sport in Society at Northeastern University</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/board/donald.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Donald Cretella</h3>
                        <p>Attorney Zingaro and Cretella</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/board/Edward.jpg" class="img-fluid rounded boardAdjust">
                        <h3>Edward Nazarro</h3>
                        <p>Former Director of Pupil Services Town of Winthrop, Member Massachusetts Executive Council of Special Education</p>
                    </div>
                </div>
            </div>
            <div class="row rowAlt sectionContent">
                <div class="sectionCenter">
                    <i><h1 class="d-block w-100 font-weight-bolder">You're With Us! In Action</h1></i>
                </div>
                <br/>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div id="galleryTopRow" class="row">
                            <a href="assets/images/wall/1.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/1.jpg" class="img-fluid">
                            </a>
                            <a href="assets/images/wall/5.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/5.jpg" class="img-fluid">
                            </a>
                            <a href="assets/images/wall/3.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/3.jpg" class="img-fluid">
                            </a>
                            <a href="assets/images/wall/8.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/8.jpg" class="img-fluid">
                            </a>
                        </div>
                        <div class="row ">
                            <a href="assets/images/wall/2.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/2.jpg" class="img-fluid">
                            </a>
                            <a href="assets/images/wall/7.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/7.jpg" class="img-fluid">
                            </a>
                            <a href="assets/images/wall/6.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/6.jpg" class="img-fluid">
                            </a>
                            <a href="assets/images/wall/4.jpg" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-3 col-6">
                                <img src="assets/images/wall/4.jpg" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <section id="footer">
            <div class="container">
                <div class="row text-center text-xs-center text-sm-left text-md-left">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h4>Stay in Touch!</h4>
                        <div class="form-group">
                            <label for="Subscribe">Sign up to recieve the latest news about You're With Us!</label>
                            <input type="text" class="form-control" id="emailSignUp" placeholder="Email Address">
                        </div>
                        <!-- Mini Form Goes Here -->
                        <button type="button" class="btn float-right btn-success">Subscribe</button>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h4>Contact Us</h4>
                        <ul class="list-unstyled">
                            <br>
                            <li>
                                <p class="lead">Phone: <a class="footerWhite" href="tel:+9785876663">(978) 587-6663</a></p>
                            </li>
                            <li>
                                <p class="lead">Email: <a class="footerWhite" href="mailto:michael@yourewithus.org">michael@yourewithus.org</a></p>
                            </li>
                            <br>
                            <li>
                                <p class="lead">Mailing Address <br>PO Box 938 <br> Methuen, MA 01844</p>
                            </li>
                        </ul>
                        <!-- address, phone, email -->
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h4>Navigate</h4>
                        <ul class="list-unstyled quick-links">
                            <li>
                                <a href="#home"><i class="fa fa-angle-double-right"></i>Home</a>
                            </li>
                            <li>
                                <a href="#about"><i class="fa fa-angle-double-right"></i>About Us</a>
                            </li>
                            <li>
                                <a href="#testimonials"><i class="fa fa-angle-double-right"></i>Testimonials</a>
                            </li>
                            <li>
                                <a href="#board" title="Design and developed by"><i class="fa fa-angle-double-right"></i>Board</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('Login'); ?>"><i class="fa fa-angle-double-right"></i>Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <a class="btn btn-lg btn-warning" href="https://yourewithus.networkforgood.com/projects/21691-inclusion-is-not-coming-and-knocking-on-your-door" target="_blank">Donate Today</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                        <ul class="list-unstyled list-inline social text-center">
                            <li class="list-inline-item">
                                <a href="https://www.facebook.com/urwus" target="_blank"><i class="fa fa-logo fa-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://twitter.com/youre_with_us" target="_blank"><i class="fa fa-logo fa-twitter"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.instagram.com/youre_with_us/" target="_blank"><i class="fa fa-logo fa-instagram"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.linkedin.com/company/you-re-with-us/" target="_blank"><i class="fa fa-logo fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                        <p class="h6">© Copyright 2019<a class="text-green ml-2 footerWhite" href="https://www.yourewithus.org/" target="_blank">You’re With Us! is a Non-Profit 501(c)(3) Organization</a></p>
                    </div>
                    <hr>
                </div>
            </div>
        </section>
        <script src="assets/external/js/jquery.min.js"></script>
        <script src="assets/external/js/popper.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/website.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js" integrity="sha256-Y1rRlwTzT5K5hhCBfAFWABD4cU13QGuRN6P5apfWzVs=" crossorigin="anonymous"></script>
    </body>
</html>
