<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>You're With Us</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"> -->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="<?php echo base_url('assets/external/css/fullcalendar.min.css');?>" rel='stylesheet' />
    <link href="<?php echo base_url('assets/external/css/fullcalendar.print.css');?>" rel='stylesheet' media='print' />
    <link href="<?php echo base_url('assets/external/css/bootstrapValidator.min.css');?>" rel='stylesheet' rel="stylesheet" />
    <link href="<?php echo base_url('assets/external/css/select2.min.css');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/external/css/jquery-ui.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/external/css/dataTables.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/wickedpicker.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets/external/css/dropzone.css');?>" rel="stylesheet">


    </head>
    <body>
      <?php date_default_timezone_set('America/New_York'); ?>
        <nav class="navbar-fixed-top navbar navbar-inverse">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url('Home/index');?>">You're With Us!</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                      <li>
                          <a href="<?php echo site_url('Home/index');?>">Home</a>
                      </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Tools &nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url('Logs/index');?>">Daily Activity Log</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Logs/inclusionAssessment');?>">Inclusion Assesment Tool</a>
                                </li>
                                <?php if($this->session->role != "6") { ?>
                                <li>
                                    <a href="<?php echo site_url('Message/index');?>">Message Board</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Logs/timeSheet');?>">Time Sheet</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Logs/expenseSheet');?>">Expense Sheet</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                          <li>
                              <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Reports&nbsp;<b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                  <li>
                                      <a href="<?php echo site_url('Reports/dailyLogEntreies');?>">Daily Log Entries</a>
                                  </li>
                                  <li>
                                      <a href="<?php echo site_url('Reports/inclusionAssessmentReport');?>">Inclusion Assessment Entries</a>
                                  </li>
                                  <?php if($this->session->role != "6") { ?>
                                  <li>
                                      <a href="<?php echo site_url('Reports/timeSheetReport');?>">Time Sheet Entries</a>
                                  </li>
                                  <li>
                                      <a href="<?php echo site_url('Reports/expenseSheetReport');?>">Expense Sheet Entries</a>
                                  </li>
                                  <?php } ?>
                                  <?php if($this->session->role == "1") { ?>
                                  <li>
                                      <a href="<?php echo site_url('Subscribers/index');?>">Mailing List</a>
                                  </li>
                                  <?php } ?>
                              </ul>
                          </li>
                          <li>
                              <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">List&nbsp;<b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                  <?php if(($this->session->role == "1") or ($this->session->role == "4") or ($this->session->role == "7")) { ?>
                                  <li>
                                      <a href="<?php echo site_url('Users/index');?>">Users</a>
                                  </li>
                                  <?php } ?>
                                  <li>
                                      <a href="<?php echo site_url('Participants/index');?>">Participants</a>
                                  </li>
                                  <?php if($this->session->role == "1") { ?>
                                  <li>
                                      <a href="<?php echo site_url('Schools/index');?>">Schools</a>
                                  </li>
                                  <?php } ?>
                                  <!-- Admin only view here -->
                                  <?php if($this->session->role == "1") { ?>
                                  <div class="divider"></div>
                                  <li class="dropdown-header">Deactivated</li>
                                  <li>
                                      <a href="<?php echo site_url('Users/deactiveUsers');?>">Deactivated Users</a>
                                  </li>
                                  <!-- <li>
                                      <a href="<?php echo site_url('Users/deactiveUsers');?>">Deactivated Partipants</a>
                                  </li> -->
                                  <?php } ?>
                              </ul>
                          </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Surveys&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="https://surveyplanet.com/5720fd4b340263736337da6b" target="_blank">Group Survey</a>
                                </li>
                                <li>
                                    <a href="https://surveyplanet.com/5720f8d7a7ede18e32199801" target="_blank">Particant Survey</a>
                                </li>
                                <li>
                                    <a href="https://surveyplanet.com/571fd245a7ede18e32199437" target="_blank">Parent/Guardian Survey</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Support&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url('Support/theWhy');?>">The Why</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Support/orientation');?>">Orientation</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Support');?>">Individual Supports</a>
                                </li>
                                  <li>
                                    <a href="<?php echo site_url('Support/transitions');?>">Transition Best Practices</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Support/mentorThoughts');?>">Mentor Thoughts</a>
                                </li>
                                <?php if($this->session->role != "6") { ?>
                                <li>
                                    <a href="<?php echo site_url('Support/resources');?>">Employee Resources</a>
                                </li>
                                <?php } ?>
                                <div class="divider"></div>
                                <li class="dropdown-header">External Links</li>
                                <li>
                                    <a href="http://www.bu.edu/sed/research-action/community-programs/power-of-good/" target="_blank">Webcast</a>
                                </li>
                                <li>
                                    <a href="https://ma.exceptionallives.org/how-to-support-the-transition-to-adulthood/" target="_blank">Transition Guide</a>
                                </li>
                                 <li>
                                    <a href="http://51a.middlesexcac.org/" target="_blank">51A Mandated Reported Training</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="<?php echo site_url('Calendar/home');?>">Schedule</a>
                        </li>
                        <!-- <li>
                            <a href="<?php echo site_url('Blog/index');?>">Blog</a>
                        </li> -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Tasks&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url('Tasks/newTask');?>">New Task</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Tasks/pendingTasks');?>">Pending Tasks</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Tasks/completedTasks');?>">Completed Tasks</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Tasks/sentTasks');?>">Sent Tasks</a>
                                </li>
                                <?php if($this->session->role == "1") { ?>
                                <li>
                                    <a href="<?php echo site_url('Tasks/allTasks');?>">All Tasks</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"><?php echo $this->session->firstname." ".$this->session->lastname;?>&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-header"><?php echo "Role: ".$this->session->roletitle;?></li>
                                <li>
                                    <a href="<?php echo site_url('Settings');?>">Account Settings</a>
                                </li>
                                  <li>
                                    <a href="<?php echo site_url('Bugs');?>">Report Bug</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('Login/logout');?>">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
        </nav>

        <input type="hidden" value="<?php echo site_url(); ?>" id="base_url">

        <script src='<?php echo base_url();?>assets/external/js/jquery.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/moment.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/bootstrap.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/bootstrapValidator.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/fullcalendar.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/select2.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/validator.min.js'></script>
        <script src='<?php echo base_url();?>assets/js/calendar.js'></script>
        <script src='<?php echo base_url();?>assets/js/app.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/jquery-ui.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/jquery.dataTables.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/dataTables.bootstrap.min.js'></script>
        <script src='<?php echo base_url();?>assets/external/js/dropzone.js'></script>
        <script src='<?php echo base_url();?>assets/js/wickedpicker.js'></script>
        <script src='<?php echo base_url();?>assets/js/print.js'></script>
